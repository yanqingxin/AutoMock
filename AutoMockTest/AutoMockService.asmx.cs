﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using AutoMockTest.CommonLib;
using System.Threading.Tasks;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections;

namespace AutoMockTest
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class AutoMockService : System.Web.Services.WebService
    {

        [WebMethod]
        public Response PullLog(string searchFilter, string count,string useMobileLog)
        {
            Response response = new Response();
            response.ResultCode = "FAIL";

            int logCount = 100;
            if (count != null && count.Trim() != string.Empty)
            {
                if (!int.TryParse(count, out logCount))
                {
                    response.ErrorMsg = "输入参数count格式错误";
                    return response;
                }
            }

            if (searchFilter != null)
            {
                searchFilter = searchFilter.Trim();
            }

            ESLogHelper esLoger = new ESLogHelper();
            string batchNum = esLoger.initialBatch(searchFilter,logCount);
            response.batchNum = batchNum;

            Task task = Task.Factory.StartNew(() =>
                {
                    esLoger.PullEsLog(searchFilter, count, batchNum,useMobileLog);
                });

            if (response.batchNum != "0")
                response.ResultCode = "PASS";
            else
                response.ErrorMsg = "无法新增批次";
            return response;

        }

        [WebMethod]
        public ProgressResponse QueryProgress(string batchNum, string executeBatchNum)
        {
            ProgressResponse pRsp = new ProgressResponse();
            pRsp.ResultCode = "FAIL";

            if (batchNum.Trim() == string.Empty)
            {
                pRsp.ErrorMsg = "BatchNum参数错误";
                return pRsp;
            }

            string sql = string.Format("select * from FlightBookingAutomation..ESLogBatch where BatchNum = '{0}'", batchNum);
            DataTable BatchNumTB = SQLHelper.ExecuteSql(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));
            if (BatchNumTB.Rows.Count == 0)
            {
                pRsp.ErrorMsg = string.Format("BatchNum:{0}不存在", batchNum);
                return pRsp;
            }
            pRsp.ResultCode = "PASS";            

            if (executeBatchNum.Trim() != string.Empty)
            {
                sql = string.Format("select * from FlightBookingAutomation..ESLogExecuteBatch where BatchNum = '{0}' and ExecuteBatchNum = '{1}'", batchNum, executeBatchNum);
                DataTable ExeBatchNumTB = SQLHelper.ExecuteSql(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));
                if (ExeBatchNumTB.Rows.Count == 0)
                {
                    pRsp.ErrorMsg = string.Format("BatchNum:{0} ExecuteBatchNum:{1}不存在", batchNum, executeBatchNum);
                    return pRsp;
                }

                pRsp.Status = ExeBatchNumTB.Rows[0]["Status"].ToString();

                string totalCount = SQLHelper.ExecuteSqlByScalar(string.Format("SELECT count(1) from FlightBookingAutomation..ESLogMobileWS where BatchNum = '{0}'", batchNum)).ToString();
                string executeCount = SQLHelper.ExecuteSqlByScalar(string.Format("SELECT count(1) from FlightBookingAutomation..ESLogExecuteData where BatchNum = '{0}' and ExecuteBatchNum = '{1}'", batchNum, executeBatchNum)).ToString();

                if (totalCount.Trim() == "" || int.Parse(totalCount) == 0 || executeCount.Trim() == "")
                {
                    pRsp.Progress = "0";
                    pRsp.ResultCode = "FAIL";
                }
                else
                {
                    pRsp.Progress = (int.Parse(executeCount) * 100 / int.Parse(totalCount)).ToString();
                }
            }
            else
            {
                pRsp.Status = BatchNumTB.Rows[0]["Status"].ToString();

                string totalCount = BatchNumTB.Rows[0]["ExpectCount"].ToString();
                string executeCount = SQLHelper.ExecuteSqlByScalar(string.Format("SELECT count(1) from FlightBookingAutomation..ESLogMobileWS where BatchNum = '{0}'", batchNum)).ToString();

                if (totalCount.Trim() == "" || int.Parse(totalCount) == 0 || executeCount.Trim() == "")
                {
                    pRsp.Progress = "0";
                    pRsp.ResultCode = "FAIL";
                }
                else
                {
                    pRsp.Progress = (int.Parse(executeCount) * 100 / int.Parse(totalCount)).ToString();
                }
            }

            
            return pRsp;
        }

        [WebMethod]
        public Response Excute(string batchNum)
        {            
            Response response = new Response();
            response.ResultCode = "FAIL";
            response.batchNum = batchNum;

            try
            {
                DataTable MockInfo = MockHelper.getMobileWSFromDB(batchNum);

                if (MockInfo.Rows.Count == 0)
                {
                    response.ErrorMsg = "根据批次号未检查到可执行批次，请检查参数";
                    return response;
                }
                response.ResultCode = "PASS";

                MockHelper mh = new MockHelper(Scope.AfterServiceOrderChange);

                int ExecuteBatchNum = mh.getExecuteNoByBatchNum(batchNum);
                response.executeBatchNum = ExecuteBatchNum.ToString();
                Task task = Task.Factory.StartNew(() =>
                {
                    mh.beginMockByBatchNumNew(batchNum, ExecuteBatchNum).ToString();
                });
                    
            }
            catch(Exception e)
            {
                response.ErrorMsg = e.Message;
                response.StackTrace = e.StackTrace;
            }
            return response;
        }

        [WebMethod]
        public Response ExcuteByGuid(string batchNum,int ExecuteBatchNum,string Guid)
        {
            Response response = new Response();
            response.ResultCode = "FAIL";
            response.batchNum = batchNum;

            try
            {
                MockHelper mh = new MockHelper(Scope.AfterServiceOrderChange);
                mh.beginMockByBatchNumNew(batchNum, ExecuteBatchNum, Guid);
                response.ResultCode = "PASS";
            }
            catch (Exception e)
            {
                response.ErrorMsg = e.Message;
                response.StackTrace = e.StackTrace;
            }
            return response;
        }

        [WebMethod]
        public Response Compare(string batchNum, string executeNum)
        {
            Response response = new Response();
            response.ResultCode = "FAIL";
            response.batchNum = batchNum;
            try
            {
                CompareHelper ch = new CompareHelper();
                ch.process(batchNum,  executeNum);
                response.ResultCode = "PASS";
            }
            catch(Exception e)
            {
                response.ErrorMsg = e.Message;
                response.StackTrace = e.StackTrace;
            }

            return response;
        }

        [WebMethod]
        public SearchResponse Query(string batchNum)
        {
            SearchResponse rsp = new SearchResponse();
            rsp.ResultCode = "FAIL";

            DataTable resultTB = null;
            DataTable _resultTB = null;
            string sql = string.Empty;
            
            if (batchNum.Trim() == "")
                sql = "SELECT top 100 * from FlightBookingAutomation..ESLogBatch ORDER BY CreateTime DESC";
            else
                sql = string.Format("SELECT top 100 * from FlightBookingAutomation..ESLogBatch WHERE BatchNum = '{0}' ORDER BY CreateTime DESC", batchNum);

            try
            {
                resultTB = SQLHelper.ExecuteSql(sql);

                foreach (DataRow row in resultTB.Rows)
                {
                    BatchInfo batch = new BatchInfo();
                    batch.BatchNum = row["BatchNum"].ToString();
                    batch.CreateTime = row["CreateTime"].ToString();
                    batch.Status = row["Status"].ToString();
                    batch.SearchCondition = row["SearchFilter"].ToString();
                    batch.Count = row["ExpectCount"].ToString();

                    string _sql = string.Format("SELECT * from FlightBookingAutomation..ESLogExecuteBatch where BatchNum = '{0}' ORDER BY ExecuteBatchNum", row["BatchNum"].ToString());
                    _resultTB = SQLHelper.ExecuteSql(_sql);

                    foreach (DataRow _row in _resultTB.Rows)
                    {
                        ExecuteInfo _executeInfo = new ExecuteInfo();
                        _executeInfo.ExecuteBatchNum = _row["ExecuteBatchNum"].ToString();
                        _executeInfo.CreateTime = _row["CreateTime"].ToString();
                        _executeInfo.Status = _row["Status"].ToString();
                        batch.appendExecuteInfo(_executeInfo);
                    }

                    rsp.appendBatchInfo(batch);
                }

                rsp.ResultCode = "PASS";
            }
            catch (Exception e)
            {
                rsp.ErrorMsg = e.Message;
                rsp.StackTrace = e.StackTrace;
            }


            return rsp;
        }

        [WebMethod]
        public string queryRequest(string BatchNum, string Guid)
        {
            string sql = string.Format("SELECT RequestData  FROM [FlightBookingAutomation]..ESLogMobileWS where BatchNum = '{0}' and GUID = '{1}'", BatchNum, Guid);
            DataTable result = SQLHelper.ExecuteSql(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));

            if (result.Rows.Count == 0)
                return "未查询到报文";
            else
                return result.Rows[0]["RequestData"].ToString();
        }

        [WebMethod]
        public CompareResultResponse queryCompareResult(string BatchNum, string ExecuteBatchNum, string compareType, string xmlPath = "",string guid = "")
        {
            CompareResultResponse rsp = new CompareResultResponse();
            rsp.ResultCode = "FAIL";
            Dictionary<string,string> dict = new Dictionary<string,string>();
            Hashtable hashtable = new Hashtable();
            List<DifferInfo> differList = new List<DifferInfo>();

            try
            {
                string sql = string.Empty;
                sql = string.Format("SELECT CompareStatus from FlightBookingAutomation..ESLogExecuteBatch where BatchNum = {0} and ExecuteBatchNum = {1}", BatchNum, ExecuteBatchNum);
                DataTable exeBatchTable = SQLHelper.ExecuteSql(sql);
                rsp.Status = exeBatchTable.Rows.Count > 0 ? exeBatchTable.Rows[0]["CompareStatus"].ToString() : "Unknow";

                if(compareType == "1")
                    sql = string.Format("SELECT GUID,ESCompare as BenchCompare,TestCompare,DifferSet from FlightBookingAutomation..ESLogCompareData where BatchNum = '{0}' and ExecuteBatchNum = '{1}'", BatchNum, ExecuteBatchNum);
                else
                    sql = string.Format("SELECT GUID,LastTestCompare as BenchCompare,TestCompare,DifferSet from FlightBookingAutomation..ESLogCompareTestData where BatchNum = '{0}' and ExecuteBatchNum = '{1}'", BatchNum, ExecuteBatchNum);
                DataTable compareDataTable = SQLHelper.ExecuteSql(sql);
                rsp.totalCount = compareDataTable.Rows.Count.ToString();
                foreach (DataRow row in compareDataTable.Rows)
                {
                    if (guid != "")
                    {
                        if (row["GUID"].ToString().ToLower() == guid.ToLower())
                        {
                            DifferDetail differDetail = new DifferDetail();
                            differDetail.GUID = row["GUID"].ToString();
                            differDetail.ESCompare = Utility.filerXpath(row["BenchCompare"].ToString(), xmlPath);
                            differDetail.TestCompare = Utility.filerXpath(row["TestCompare"].ToString(), xmlPath);
                            rsp.differDetails.Add(differDetail);
                        }
                    }
                    else
                    {
                        string[] differs = Regex.Split(row["DifferSet"].ToString(), @"\|\|\|");
                        foreach (string differ in differs)
                        {
                            if (xmlPath == string.Empty)
                            {
                                if (differ.Trim() != string.Empty)
                                {
                                    if (differList.Find(e => e.xmlPath == differ) != null)
                                    {
                                        differList.Find(e => e.xmlPath == differ).count++;
                                    }
                                    else
                                    {
                                        DifferInfo differInfo = new DifferInfo();
                                        differInfo.xmlPath = differ;
                                        differInfo.count = 1;
                                        differList.Add(differInfo);
                                    }
                                }
                            }
                            else
                            {
                                if (differ == xmlPath)
                                {
                                    DifferDetail differDetail = new DifferDetail();
                                    differDetail.GUID = row["GUID"].ToString();
                                    rsp.differDetails.Add(differDetail);
                                    break;
                                }
                            }
                        }



                        if (xmlPath == "")
                        {
                            differList.Sort();
                            rsp.differs = differList;
                        }
                    }
                }

                rsp.ResultCode = "PASS";
            }
            catch (Exception e)
            {
                rsp.ErrorMsg = e.Message;
                rsp.StackTrace = e.StackTrace;
            }

            return rsp;
        }

    }


}