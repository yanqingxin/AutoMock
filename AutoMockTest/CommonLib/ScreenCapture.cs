﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.IO;

namespace AutoMockTest.CommonLib
{
    public class ScreenCapture
    {
        private string CmdPath = @"C:\Windows\System32\cmd.exe";
        public string Capture(string url)
        {
            string imageName = DateTime.Now.ToString("yyyyMMddHHmmssfff")+".jpg";
            string currentPath = AppDomain.CurrentDomain.BaseDirectory;
            string cmd = string.Empty;
            cmd = string.Format(@"phantomjs.exe {0}Resource\render.js ""{1}"" {0}Resource\{2} &exit", currentPath, url, imageName);
            Process p = new Process();
            p.StartInfo.FileName = CmdPath;
            p.StartInfo.UseShellExecute = false;        //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;   //接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;  //由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;   //重定向标准错误输出
            p.StartInfo.CreateNoWindow = true;          //不显示程序窗口
            p.Start();
            p.StandardInput.WriteLine(cmd);

            p.WaitForExit();//等待程序执行完退出进程
            p.Close();//关闭进程
            return string.Format(@"{0}Resource\{1}", currentPath, imageName);
        }
    }
}