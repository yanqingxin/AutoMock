﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;

namespace AutoMockTest.CommonLib
{
    public class Email
    {
        static string serviceUrl = "http://ws.email.chnl.fat412.qa.nt.ctripcorp.com/emailservice/json/SendEmail";

        static string emailRequest = @"{{
  ""OrderID"": 0,
  ""UID"": ""String"",
  ""Eid"": ""String"",
  ""SourceID"": 0,
  ""SendCode"": ""19090014"",
  ""AppID"": 100004705,
  ""Sender"": ""ji_cy@ctrip.com"",
  ""Recipient"": [
    {0}
  ],
  ""Cc"": [
  ],
  ""Bcc"": [
  ],
  ""SenderName"": ""自动化Job"",
  ""RecipientName"": """",
  ""Subject"": ""{1}"",
  ""BodyTemplateID"": 19090014,
  ""BodyContent"": ""<entry><content><![CDATA[{2}]]></content></entry>"",
  ""IsBodyHtml"": true,
  ""Charset"": ""GB2312"",
  ""ExpiredTime"": ""{3:yyyy-MM-dd HH:00:00}"",
  ""AttachmentList"": [
  ]
}}";

        string BuildRequest(string receiverList, string subject, string url, string ImageData)
        {
            string receivers= string.Empty;
            foreach(string receiver in receiverList.Split(','))
            {
                receivers += "\""+receiver+"\",";
            }
            receivers = receivers.TrimEnd(',');
            string emailContent = BuildContent(url,ImageData);

            return string.Format(emailRequest, receivers, subject, emailContent, DateTime.Now.AddHours(1));
        }

        string BuildContent(string url,string ImageData)
        {
            string imageContent = ImageData.Trim() == "" ? "" : string.Format(@"<DIV><IMG src=""data:image/jpg;base64,{0}> </IMG></DIV>", ImageData);
            string urlContent = string.Format("<DIV>{0}</DIV>", url);

            //return string.Format("<BODY style=\"MARGIN: 10px\">{0}{1}</BODY>", urlContent, imageContent);
            return url;
        }

        public void SendMail(string receiverList, string subject, string url, string ImageData)
        {
            string request = BuildRequest(receiverList, subject, url, ImageData);
            string response = Utility.HttpPost(serviceUrl, request, "application/json","utf-8");
        }

        public string convertData(string Imagefilename)
        {
            Bitmap bmp = new Bitmap(Imagefilename);

            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] arr = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(arr, 0, (int)ms.Length);
            ms.Close();

            String strbaser64 = Convert.ToBase64String(arr);
            return strbaser64;
        }
    }
}