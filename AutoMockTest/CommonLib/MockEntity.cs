﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoMockTest.CommonLib
{
    public class MockEntity
    {
        public string ResponseData { get; set; }
        public string CompressType { get; set; }
    }
}
