﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Data;
using System.Threading.Tasks;
using System.Threading;

namespace AutoMockTest.CommonLib
{
    public class CompareHelper
    {

        /// <summary>
        /// 对比json，填充到list中，遍历json，递归调用
        /// </summary>
        /// <param name="jsonAStr"></param>
        /// <param name="jsonBStr"></param>
        /// <param name="listA"></param>
        /// <param name="listB"></param>
        /// <param name="indent">缩进</param>
        public void compare(string jsonAStr, string jsonBStr, List<string> listA, List<string> listB, List<string> diffList, string parentPath, string indent = "")
        {
            JObject jsonA = null;
            JObject jsonB = null;
            bool AisJson = true;
            bool BisJson = true;

            try
            {
                // convert JSON to object
                jsonA = JObject.Parse(jsonAStr);

            }
            catch (JsonReaderException)
            {
                AisJson = false;
            }

            try
            {
                jsonB = JObject.Parse(jsonBStr);
            }
            catch(JsonReaderException)
            {
                BisJson = false;
            }

            if (!BisJson&&!AisJson)
            {
                if (jsonAStr == jsonBStr)
                {
                    if (listA.Count != 0 && listA[listA.Count - 1].EndsWith(":"))//前一个节点是：结尾，说明需要显示为一行
                    {
                        listA[listA.Count - 1] += jsonAStr;
                        listB[listB.Count - 1] += jsonBStr;
                    }
                    else
                    {
                        listA.Add(indent + jsonAStr);
                        listB.Add(indent + jsonBStr);
                    }
                }
                else
                {
                    bool isEqual = false;
                    DateTime TimeA = new DateTime();
                    DateTime TimeB = new DateTime();

                    if (DateTime.TryParse(jsonAStr, out TimeA) && DateTime.TryParse(jsonBStr, out TimeB))
                    {
                        TimeSpan ts = TimeA.Subtract(TimeB);
                        if (ts <= new TimeSpan(0, 1, 0) && ts >= new TimeSpan(0, -1, 0))//时间相差1分钟以内算作正常
                        {
                            diffList.RemoveAt(diffList.Count - 1);//时间判断相同的情况下，删除之前记录的标记
                            isEqual = true;
                        }
                    }
                    if (isEqual)
                    {
                        if (listA.Count != 0 && listA[listA.Count - 1].EndsWith(":"))//前一个节点是：结尾，说明需要显示为一行
                        {
                            listA[listA.Count - 1] += jsonAStr;
                            listB[listB.Count - 1] += jsonBStr;
                        }
                        else
                        {
                            listA.Add(indent + jsonAStr);
                            listB.Add(indent + jsonBStr);
                        }
                    }
                    else
                    {
                        if (listA.Count != 0 && listA[listA.Count - 1].EndsWith(":") || listB.Count != 0 && listB[listB.Count - 1].EndsWith(":"))//前一个节点是：结尾，说明需要显示为一行
                        {
                            listA[listA.Count - 1] = (listA[listA.Count - 1].Contains("||") ? "" : (diffList[diffList.Count - 1] + "||")) + listA[listA.Count - 1].Replace("[HightLight]", "") + "[HightLight]" + jsonAStr;//没有Xpath的增加Xpath，去除已有的HightLight
                            listB[listB.Count - 1] = (listB[listB.Count - 1].Contains("||") ? "" : (diffList[diffList.Count - 1] + "||")) + listB[listB.Count - 1].Replace("[HightLight]", "") + "[HightLight]" + jsonBStr;//没有Xpath的增加Xpath，去除已有的HightLight
                        }
                        else
                        {
                            listA.Add(diffList[diffList.Count - 1] + "||" + indent + "[HightLight]" + jsonAStr);
                            listB.Add(diffList[diffList.Count - 1] + "||" + indent + "[HightLight]" + jsonBStr);
                        }
                    }
                }


                return;
            }

            if (jsonAStr == "null" || jsonAStr == "")
            {
                if (jsonAStr == "null" && jsonBStr != "")
                {
                    if (listA.Count != 0 && listA[listA.Count - 1].EndsWith(":") || listB.Count != 0 && listB[listB.Count - 1].EndsWith(":"))//前一个节点是：结尾，说明需要显示为一行
                    {
                        listA[listA.Count - 1] = (listA[listA.Count - 1].Contains("||") ? "" : (diffList[diffList.Count - 1] + "||")) + listA[listA.Count - 1].Replace("[HightLight]", "") + "[HightLight]" + jsonAStr;//没有Xpath的增加Xpath，去除已有的HightLight
                        listB[listB.Count - 1] = (listB[listB.Count - 1].Contains("||") ? "" : (diffList[diffList.Count - 1] + "||")) + listB[listB.Count - 1].Replace("[HightLight]", "") + "[HightLight]";//没有Xpath的增加Xpath，去除已有的HightLight
                    }
                }
                JObject _jsonB = JObject.Parse(jsonBStr);

                foreach (JToken eachToken in _jsonB.Values())
                {
                    string xpath = diffList[diffList.Count - 1] + "||";
                    if (eachToken.Type == JTokenType.Object)
                    {
                        listA.Add(xpath + indent + "" + "[HightLight]");
                        listB.Add(xpath + indent + eachToken.Path + "[HightLight]");                        

                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight]{");

                        compare("", eachToken.ToString(), listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachToken.Path), indent + "   ");

                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight]}");
                    }
                    else if (eachToken.Type == JTokenType.Array)
                    {
                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight]" + eachToken.Path);

                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight][");

                        foreach (JToken eachItem in eachToken.ToList())
                        {
                            if (eachItem.Type == JTokenType.Object)
                            {
                                listA.Add(xpath + indent + "[HightLight]");
                                listB.Add(xpath + indent + "[HightLight]{");
                            }
                            compare("", eachItem.ToString(), listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, eachToken.Path, eachToken.ToList().IndexOf(eachItem)), indent + "   ");
                            if (eachItem.Type == JTokenType.Object)
                            {
                                listA.Add(xpath + indent + "[HightLight]");
                                listB.Add(xpath + indent + "[HightLight]}");
                            }
                        }

                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight]]");
                    }
                    else
                    {
                        listA.Add(xpath + indent + "[HightLight]" + "");
                        listB.Add(xpath + indent + eachToken.Path + ":" + "[HightLight]" + eachToken.ToString());
                    }
                }

                return;
            }

            if (jsonBStr == "null" || jsonBStr == "")
            {
                if (jsonBStr == "null" && jsonAStr != "")
                {
                    if (listA.Count != 0 && listA[listA.Count - 1].EndsWith(":") || listB.Count != 0 && listB[listB.Count - 1].EndsWith(":"))//前一个节点是：结尾，说明需要显示为一行
                    {
                        listA[listA.Count - 1] = (listA[listA.Count - 1].Contains("||") ? "" : (diffList[diffList.Count - 1] + "||")) + listA[listA.Count - 1].Replace("[HightLight]", "") + "[HightLight]";//没有Xpath的增加Xpath，去除已有的HightLight
                        listB[listB.Count - 1] = (listB[listB.Count - 1].Contains("||") ? "" : (diffList[diffList.Count - 1] + "||")) + listB[listB.Count - 1].Replace("[HightLight]", "") + "[HightLight]" + jsonBStr;//没有Xpath的增加Xpath，去除已有的HightLight
                    }
                }
                JObject _jsonA = JObject.Parse(jsonAStr);

                foreach (JToken eachToken in _jsonA.Values())
                {
                    //addToDiffList(diffList, parentPath, eachToken.Path);//发现不同的path进行标记
                    string xpath = diffList[diffList.Count - 1] + "||";
                    if (eachToken.Type == JTokenType.Object)
                    {
                        listA.Add(xpath + indent + eachToken.Path + "[HightLight]");
                        listB.Add(xpath + indent + "" + "[HightLight]");

                        listA.Add(xpath + indent + "[HightLight]{");
                        listB.Add(xpath + indent + "[HightLight]");
                        compare(eachToken.ToString(), "", listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachToken.Path), indent + "   ");
                        listA.Add(xpath + indent + "[HightLight]}");
                        listB.Add(xpath + indent + "[HightLight]");
                    }
                    else if (eachToken.Type == JTokenType.Array)
                    {
                        listA.Add(xpath + indent + "[HightLight]" + eachToken.Path);
                        listB.Add(xpath + indent + "[HightLight]");

                        listA.Add(xpath + indent + "[HightLight][");
                        listB.Add(xpath + indent + "[HightLight]");

                        foreach (JToken eachItem in eachToken.ToList())
                        {
                            if (eachItem.Type == JTokenType.Object)
                            {
                                listA.Add(xpath + indent + "[HightLight]{");
                                listB.Add(xpath + indent + "[HightLight]");
                            }
                            compare(eachItem.ToString(), "", listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, eachToken.Path, eachToken.ToList().IndexOf(eachItem)), indent + "   ");
                            if (eachItem.Type == JTokenType.Object)
                            {
                                listA.Add(xpath + indent + "[HightLight]}");
                                listB.Add(xpath + indent + "[HightLight]");
                            }
                        }

                        listA.Add(xpath + indent + "[HightLight]]");
                        listB.Add(xpath + indent + "[HightLight]");
                    }
                    else
                    {
                        listA.Add(xpath + indent + eachToken.Path + ":" + "[HightLight]" + eachToken.ToString());
                        listB.Add(xpath + indent + "[HightLight]" + "");
                    }
                }

                return;
            }            

            List<JToken> jsonAValues = jsonA.Values().ToList();
            List<JToken> jsonBValues = jsonB.Values().ToList();

            //找出最全集的path
            List<string> pathList = jsonAValues.Select(e => e.Path).ToList();
            List<string> pathListB = jsonBValues.Select(e => e.Path).ToList();

            for (int i = 0; i < pathListB.Count; i++)
            {
                if (!pathList.Exists(e=>e.ToLower() == pathListB[i].ToLower()))
                {
                    if (i == 0)
                        pathList.Insert(0, pathListB[i]);
                    else
                        pathList.Insert(pathList.LastIndexOf(pathListB[i - 1]) + 1, pathListB[i]);
                }
            }

            foreach (string eachPath in pathList)
            {
                List<JToken> tokensA = jsonAValues.Where(e => e.Path.ToLower() == eachPath.ToLower()).ToList();
                List<JToken> tokensB = jsonBValues.Where(e => e.Path.ToLower() == eachPath.ToLower()).ToList();
                
                if (tokensA.Count > 0 && tokensB.Count > 0)
                {
                    listA.Add(indent + eachPath);
                    listB.Add(indent + eachPath);
                    if (tokensA[0].ToString() != tokensB[0].ToString())
                    {
                        if (tokensA[0].Type == tokensB[0].Type)
                        {
                            if (tokensA[0].Type != JTokenType.Object && tokensA[0].Type != JTokenType.Array)
                            {
                                listA.RemoveAt(listA.Count - 1);
                                listB.RemoveAt(listB.Count - 1);

                                addToDiffList(diffList, parentPath, eachPath);//发现不同的path进行标记

                                DateTime TimeA = new DateTime();
                                DateTime TimeB = new DateTime();
                                bool isEqual = false;
                                if (DateTime.TryParse(tokensA[0].ToString(), out TimeA) && DateTime.TryParse(tokensB[0].ToString(), out TimeB))
                                {
                                    TimeSpan ts = TimeA.Subtract(TimeB);
                                    if (ts <= new TimeSpan(0, 1, 0) && ts >= new TimeSpan(0, -1, 0))//时间相差1分钟以内算作正常
                                    {
                                        diffList.RemoveAt(diffList.Count - 1);
                                        isEqual = true;                                        
                                    }
                                }
                                if (isEqual)
                                {
                                    listA.Add( indent + eachPath + ":" + tokensA[0].ToString());
                                    listB.Add( indent + eachPath + ":" + tokensB[0].ToString());
                                }
                                else
                                {
                                    listA.Add(diffList[diffList.Count - 1] + "||" + indent + eachPath + ":" + "[HightLight]" + tokensA[0].ToString());
                                    listB.Add(diffList[diffList.Count - 1] + "||" + indent + eachPath + ":" + "[HightLight]" + tokensB[0].ToString());
                                }

                            }
                            else if (tokensA[0].Type == JTokenType.Array)
                            {
                                listA.Add(indent + "[");
                                listB.Add(indent + "[");                                

                                List<JToken> arrayListA = tokensA[0].ToList();
                                List<JToken> arrayListB = tokensB[0].ToList();
                                List<JToken> _arrayListA = tokensA[0].ToList();

                                foreach (JToken eachitemA in arrayListA)
                                {
                                    decimal Similarity = 0;
                                    decimal _Similarity = 0;
                                    JToken _item = null;
                                    JToken __item = null;
                                    foreach (JToken eachitemB in arrayListB)
                                    {
                                        //_Similarity = Utility.LevenshteinDistancePercent(eachitemA.ToString(), eachitemB.ToString());
                                        _Similarity = calcSimilarity(eachitemA.ToString(), eachitemB.ToString());
                                        if (_Similarity > Similarity)
                                        {
                                            Similarity = _Similarity;
                                            _item = eachitemB;
                                        }
                                    }

                                    if (_item != null)
                                    {
                                        Similarity = 0;
                                        foreach (JToken _eachitemA in _arrayListA)
                                        {
                                            //_Similarity = Utility.LevenshteinDistancePercent(_item.ToString(), _eachitemA.ToString());
                                            _Similarity = calcSimilarity(_item.ToString(), _eachitemA.ToString());
                                            if (_Similarity > Similarity)
                                            {
                                                Similarity = _Similarity;
                                                __item = _eachitemA;
                                            }
                                        }
                                    }

                                    if (__item == eachitemA)
                                    {
                                        listA.Add(indent + "{");
                                        listB.Add(indent + "{");
                                        compare(eachitemA.ToString(), _item.ToString(), listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, eachPath, arrayListA.IndexOf(eachitemA)), indent + "   ");
                                        listA.Add(indent + "}");
                                        listB.Add(indent + "}");
                                        arrayListB.Remove(_item);
                                    }
                                    else
                                    {
                                        addToDiffList(diffList, parentPath, string.Format("{0}[{1}]", eachPath, arrayListA.IndexOf(eachitemA)));//发现不同的path进行标记
                                        string xpath = diffList[diffList.Count - 1] + "||";
                                        listA.Add(xpath + indent + "[HightLight]{");
                                        listB.Add(xpath + indent + "[HightLight]");
                                        compare(eachitemA.ToString(), "", listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, eachPath, arrayListA.IndexOf(eachitemA)), indent + "   ");
                                        listA.Add(xpath + indent + "[HightLight]}");
                                        listB.Add(xpath + indent + "[HightLight]");
                                    }

                                    _arrayListA.Remove(eachitemA);
                                }

                                foreach (JToken eachitemB in arrayListB)
                                {
                                    addToDiffList(diffList, parentPath, string.Format("{0}[{1}]", eachPath, arrayListA.Count + arrayListB.IndexOf(eachitemB)));//发现不同的path进行标记
                                    string xpath = diffList[diffList.Count - 1] + "||";
                                    listA.Add(xpath + indent + "[HightLight]");
                                    listB.Add(xpath + indent + "[HightLight]{");
                                    compare("", eachitemB.ToString(), listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, eachPath, arrayListA.Count + arrayListB.IndexOf(eachitemB)), indent + "   ");
                                    listA.Add(xpath + indent + "[HightLight]");
                                    listB.Add(xpath + indent + "[HightLight]}");
                                }

                                listA.Add(indent + "]");
                                listB.Add(indent + "]");

                            }
                            else
                            {
                                listA.Add(indent + "{");
                                listB.Add(indent + "{");
                                compare(tokensA[0].ToString(), tokensB[0].ToString(), listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachPath), indent + "   ");
                                listA.Add(indent + "}");
                                listB.Add(indent + "}");
                            }
                        }
                        else
                        {
                            //节点类型不是json对象时，认为需要增加：，后续增加的内容要跟其保持一行
                            if (tokensA[0].Type != JTokenType.Object && tokensA[0].Type != JTokenType.Array && tokensB[0].Type != JTokenType.Object && tokensB[0].Type != JTokenType.Array)
                            {
                                listA[listA.Count - 1] += ":";
                                listB[listB.Count - 1] += ":";
                            }
                            addToDiffList(diffList, parentPath, eachPath);//发现不同的path进行标记

                            if (tokensA[0].Type == JTokenType.Array)//某个节点是数组的情况，要展开
                            {
                                string xpath = diffList[diffList.Count - 1] + "||";
                                listA.Add(xpath + indent + "[HightLight][");
                                listB.Add(xpath + indent + "[HightLight]");

                                foreach (JToken eachItem in tokensA[0].ToList())
                                {
                                    if (eachItem.Type == JTokenType.Object)
                                    {
                                        listA.Add(xpath + indent + "[HightLight]{");
                                        listB.Add(xpath + indent + "[HightLight]");
                                    }
                                    compare(eachItem.ToString(), tokensB[0].Type == JTokenType.Null ? "null" : tokensB[0].ToString(), listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, tokensA[0].Path, tokensA[0].ToList().IndexOf(eachItem)), indent + "   ");
                                    if (eachItem.Type == JTokenType.Object)
                                    {
                                        listA.Add(xpath + indent + "[HightLight]}");
                                        listB.Add(xpath + indent + "[HightLight]");
                                    }
                                }

                                listA.Add(xpath + indent + "[HightLight]]");
                                listB.Add(xpath + indent + "[HightLight]");
                            }
                            else if (tokensB[0].Type == JTokenType.Array)
                            {
                                string xpath = diffList[diffList.Count - 1] + "||";
                                listA.Add(xpath + indent + "[HightLight][");
                                listB.Add(xpath + indent + "[HightLight]");

                                foreach (JToken eachItem in tokensB[0].ToList())
                                {
                                    if (eachItem.Type == JTokenType.Object)
                                    {
                                        listA.Add(xpath + indent + "[HightLight]{");
                                        listB.Add(xpath + indent + "[HightLight]");
                                    }
                                    compare(tokensA[0].Type == JTokenType.Null ? "null" : tokensA[0].ToString(), eachItem.ToString(), listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, tokensA[0].Path, tokensA[0].ToList().IndexOf(eachItem)), indent + "   ");
                                    if (eachItem.Type == JTokenType.Object)
                                    {
                                        listA.Add(xpath + indent + "[HightLight]}");
                                        listB.Add(xpath + indent + "[HightLight]");
                                    }
                                }

                                listA.Add(xpath + indent + "[HightLight]]");
                                listB.Add(xpath + indent + "[HightLight]");
                            }
                            else
                                compare(tokensA[0].Type == JTokenType.Null ? "null" : tokensA[0].ToString(), tokensB[0].Type == JTokenType.Null ? "null" : tokensB[0].ToString(), listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachPath), indent + "   ");
                        }
                    }
                    else
                    {
                        if (tokensA[0].Type != JTokenType.Object && tokensB[0].Type != JTokenType.Array)
                        {
                            listA.RemoveAt(listA.Count - 1);
                            listB.RemoveAt(listB.Count - 1);

                            listA.Add(indent + eachPath + ":" + tokensA[0].ToString());
                            listB.Add(indent + eachPath + ":" + tokensB[0].ToString());
                        }
                        else if (tokensA[0].Type == JTokenType.Array)
                        {
                            listA.Add(indent + "[");
                            listB.Add(indent + "[");

                            List<JToken> arrayListA = tokensA[0].ToList();
                            List<JToken> arrayListB = tokensB[0].ToList();

                            foreach (JToken eachitemA in arrayListA)
                            {
                                if (eachitemA.Type == JTokenType.Object)
                                {
                                    listA.Add(indent + "{");
                                    listB.Add(indent + "{");
                                }
                                compare(eachitemA.Type == JTokenType.Null ? "null" : eachitemA.ToString(), eachitemA.Type == JTokenType.Null ? "null" : eachitemA.ToString(), listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachPath), indent + "   ");
                                if (eachitemA.Type == JTokenType.Object)
                                {
                                    listA.Add(indent + "}");
                                    listB.Add(indent + "}");
                                }
                            }

                            listA.Add(indent + "]");
                            listB.Add(indent + "]");
                        }
                        else
                        {
                            listA.Add(indent + "{");
                            listB.Add(indent + "{");
                            compare(tokensA[0].Type == JTokenType.Null ? "null" : tokensA[0].ToString(), tokensB[0].Type == JTokenType.Null ? "null" : tokensB[0].ToString(), listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachPath), indent + "   ");
                            listA.Add(indent + "}");
                            listB.Add(indent + "}");
                        }
                    }
                }
                else if (tokensA.Count == 0 && tokensB[0].Type != JTokenType.Null)
                {
                    addToDiffList(diffList, parentPath, eachPath);//发现不同的path进行标记
                    string xpath = diffList[diffList.Count - 1] + "||";

                    listA.Add(xpath + indent + "" + "[HightLight]");
                    listB.Add(xpath + indent + eachPath + "[HightLight]");

                    if (tokensB[0].Type != JTokenType.Array && tokensB[0].Type != JTokenType.Object)//节点不是数组或者json对象，认为是具体属性节点，需要增加:
                        listB[listB.Count - 1] += ":";

                    if (tokensB[0].Type == JTokenType.Object)
                    {
                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight]{");

                        compare("", tokensB[0].Type == JTokenType.Null ? "null" : tokensB[0].ToString() == "" ? "EmptyString" : tokensB[0].ToString(), listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachPath), indent + "   ");
                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight]}");
                    }
                    else if (tokensB[0].Type == JTokenType.Array)
                    {
                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight][");

                        foreach (JToken eachItem in tokensB[0].ToList())
                        {
                            if (eachItem.Type == JTokenType.Object)
                            {
                                listA.Add(xpath + indent + "[HightLight]");
                                listB.Add(xpath + indent + "[HightLight]{");
                            }
                            compare("", eachItem.ToString(), listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, tokensB[0].Path, tokensB[0].ToList().IndexOf(eachItem)), indent + "   ");
                            if (eachItem.Type == JTokenType.Object)
                            {
                                listA.Add(xpath + indent + "[HightLight]");
                                listB.Add(xpath + indent + "[HightLight]}");
                            }
                        }

                        listA.Add(xpath + indent + "[HightLight]");
                        listB.Add(xpath + indent + "[HightLight]]");
                    }
                    else
                        compare("", tokensB[0].Type == JTokenType.Null ? "null" : tokensB[0].ToString() == "" ? "EmptyString" : tokensB[0].ToString(), listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachPath), indent + "   ");
                }
                else if (tokensB.Count == 0 && tokensA[0].Type != JTokenType.Null)
                {
                    addToDiffList(diffList, parentPath, eachPath);//发现不同的path进行标记
                    string xpath = diffList[diffList.Count - 1] + "||";

                    listA.Add(xpath + indent + eachPath + "[HightLight]");
                    listB.Add(xpath + indent + "" + "[HightLight]");

                    if (tokensA[0].Type != JTokenType.Array && tokensA[0].Type != JTokenType.Object)//节点不是数组或者json对象，认为是具体属性节点，需要增加:
                        listA[listA.Count - 1] += ":";

                    if (tokensA[0].Type == JTokenType.Object)
                    {
                        listA.Add(xpath + indent + "[HightLight]{");
                        listB.Add(xpath + indent + "[HightLight]");
                        compare(tokensA[0].Type == JTokenType.Null ? "null" : tokensA[0].ToString() == "" ? "EmptyString" : tokensA[0].ToString(), "", listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachPath), indent + "   ");
                        listA.Add(xpath + indent + "[HightLight]}");
                        listB.Add(xpath + indent + "[HightLight]");
                    }
                    else if (tokensA[0].Type == JTokenType.Array)
                    {
                        listA.Add(xpath + indent + "[HightLight][");
                        listB.Add(xpath + indent + "[HightLight]");

                        foreach (JToken eachItem in tokensA[0].ToList())
                        {
                            if (eachItem.Type == JTokenType.Object)
                            {
                                listA.Add(xpath + indent + "[HightLight]{");
                                listB.Add(xpath + indent + "[HightLight]");
                            }
                            compare(eachItem.ToString(), "", listA, listB, diffList, string.Format("{0}.{1}[{2}]", parentPath, tokensA[0].Path, tokensA[0].ToList().IndexOf(eachItem)), indent + "   ");
                            if (eachItem.Type == JTokenType.Object)
                            {
                                listA.Add(xpath + indent + "[HightLight]}");
                                listB.Add(xpath + indent + "[HightLight]");
                            }
                        }

                        listA.Add(xpath + indent + "[HightLight]]");
                        listB.Add(xpath + indent + "[HightLight]");
                    }
                    else
                        compare(tokensA[0].Type == JTokenType.Null ? "null" : tokensA[0].ToString() == "" ? "EmptyString" : tokensA[0].ToString(), "", listA, listB, diffList, string.Format("{0}.{1}", parentPath, eachPath), indent + "   ");
                }
            }
            
        }

        /// <summary>
        /// 计算每个元素的相似度，求平均值
        /// </summary>
        /// <param name="tokenA"></param>
        /// <param name="tokenB"></param>
        /// <returns></returns>
        private static decimal calcSimilarity(string tokenA, string tokenB)
        {
            JObject _tokenA = null;
            JObject _tokenB = null;
            try
            {
                _tokenA = JObject.Parse(tokenA);
            }
            catch
            {
            }

            try
            {
                _tokenB = JObject.Parse(tokenB);
            }
            catch
            { 
            }

            if (_tokenA == null && _tokenB == null)
                return Utility.LevenshteinDistancePercent(tokenA, tokenB);            
            else if (_tokenA == null || _tokenB == null)
                return 0;

            var tokenAValues = _tokenA.Values();
            var tokenBValues = _tokenB.Values();
            //找出最全集的path
            List<string> pathList = tokenAValues.Select(e => e.Path).ToList();
            List<string> pathListB = tokenBValues.Select(e => e.Path).ToList();

            for (int i = 0; i < pathListB.Count; i++)
            {
                if (!pathList.Exists(e => e.ToLower() == pathListB[i].ToLower()))
                {
                    if (i == 0)
                        pathList.Insert(0, pathListB[i]);
                    else
                        pathList.Insert(pathList.LastIndexOf(pathListB[i - 1]) + 1, pathListB[i]);
                }
            }

            decimal TotalSimilarity = 0;
            foreach (string eachPath in pathList)
            {
                List<JToken> tokensA = tokenAValues.Where(e => e.Path.ToLower() == eachPath.ToLower()).ToList();
                List<JToken> tokensB = tokenBValues.Where(e => e.Path.ToLower() == eachPath.ToLower()).ToList();

                if (tokensA.Count > 0 && tokensB.Count > 0)
                {
                    TotalSimilarity += calcSimilarity(tokensA[0].ToString(), tokensB[0].ToString());
                }
            }

            return pathList.Count == 0 ? 1 : TotalSimilarity / pathList.Count;
        }
        

        private void addToDiffList(List<string> diffList, string parentPath, string eachPath)
        {
            diffList.Add(parentPath == "" ? parentPath : parentPath + "." + eachPath);
        }

        /// <summary>
        /// 落地结果到db
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeNum"></param>
        /// <param name="listA"></param>
        /// <param name="listB"></param>
        public void process(string batchNum, string executeNum = "")
        {
            updateExeBatchCompareStatus(batchNum, executeNum, "P");

            List<Task> taskList = new List<Task>();
            Task task1 = null;
            task1 = Task.Factory.StartNew(() =>
            {
                processPair(batchNum, executeNum, 0);
            });
            taskList.Add(task1);

            Task task2 = null;
            task2 = Task.Factory.StartNew(() =>
            {
                processPair(batchNum, executeNum, 1);
            });
            taskList.Add(task2);
            Task.WaitAll(taskList.ToArray());

            updateExeBatchCompareStatus(batchNum, executeNum, "S");
        }

        /// <summary>
        /// 对比多个环境
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeNum"></param>
        /// <param name="compareType">0:生产和测试 1:测试和测试</param>
        public void processPair(string batchNum, string executeNum,int compareType)
        {
            DataTable jsonPairs = null;
            if(compareType == 0)
            {
                jsonPairs = jsonPair(batchNum, executeNum);
            }
            else if (compareType == 1)
            {
                jsonPairs = jsonPair_Test(batchNum, executeNum);
            }

            string _executeNum = "0";
            if (jsonPairs.Rows.Count == 0)
            {
                updateExeBatchCompareStatus(batchNum, executeNum, "F");
                throw new Exception(string.Format("批次：{0}，{1}不存在", batchNum, executeNum == "" ? executeNum : string.Format("执行批次：{0}，", executeNum)));
            }
            List<Task> taskList = new List<Task>();
            for (int i = 0; i < jsonPairs.Rows.Count; i++)
            {
                string _batchNum = jsonPairs.Rows[i]["BatchNum"].ToString();
                if (_executeNum != jsonPairs.Rows[i]["ExecuteBatchNum"].ToString())
                {
                    if(compareType == 0)
                        deleteExistResult(_batchNum, jsonPairs.Rows[i]["ExecuteBatchNum"].ToString());
                    else if (compareType == 1)
                        deleteExistResult_Test(_batchNum, jsonPairs.Rows[i]["ExecuteBatchNum"].ToString());
                }
                _executeNum = jsonPairs.Rows[i]["ExecuteBatchNum"].ToString();

                Task eachTask = null;
                if (compareType == 0)
                {
                    eachTask = Task.Factory.StartNew((row) =>
                    {
                        compareProAndTest(row as DataRow);
                    }, jsonPairs.Rows[i]);
                }
                else if (compareType == 1)
                {
                    eachTask = Task.Factory.StartNew((row) =>
                    {
                        compareTestAndTest(row as DataRow);
                    }, jsonPairs.Rows[i]);
                }
                taskList.Add(eachTask);

                if (taskList.Count(e => !e.IsCompleted && !e.IsCanceled && !e.IsFaulted) >= 20 || jsonPairs.Rows.Count == i + 1)//每次处理20个
                {
                    taskList.RemoveAt(Task.WaitAny(taskList.ToArray()));                    
                }
            }
            Task.WaitAll(taskList.ToArray());
        }

        public void compareProAndTest(DataRow eachRow)
        {
            string _batchNum = eachRow["BatchNum"].ToString();
            string _executeNum = eachRow["ExecuteBatchNum"].ToString();
            string _GUID = eachRow["GUID"].ToString();
            string _ESJson = eachRow["ESJson"].ToString();
            string _TestJson = eachRow["TestJson"].ToString();

            List<string> compareResultA = new List<string>();
            List<string> compareResultB = new List<string>();
            List<string> diffList = new List<string>();

            compare(_ESJson, _TestJson, compareResultA, compareResultB, diffList, "Root");
            diffList = diffList.Where(e => !e.ToLower().StartsWith("Root.ResponseStatus".ToLower())).ToList();
            string sql = string.Format("insert into FlightBookingAutomation..ESLogCompareData values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", _batchNum, _executeNum, _GUID.Replace("'", "''"), _ESJson.Replace("'", "''"), _TestJson.Replace("'", "''"), (string.Join("|||", compareResultA.Select(x => HttpUtility.HtmlEncode(x)).ToList())).Replace("'", "''"), (string.Join("|||", compareResultB.Select(x => HttpUtility.HtmlEncode(x)).ToList())).Replace("'", "''"), (string.Join("|||", diffList)).Replace("'", "''"));
            SQLHelper.ExecuteSqlByNonQuery(sql);
        }

        public void compareTestAndTest(DataRow eachRow)
        {
            string _batchNum = eachRow["BatchNum"].ToString();
            string _executeNum = eachRow["ExecuteBatchNum"].ToString();
            string _GUID = eachRow["GUID"].ToString();
            string _LastTestJson = eachRow["LastTestJson"].ToString();
            string _TestJson = eachRow["TestJson"].ToString();

            List<string> compareResultA = new List<string>();
            List<string> compareResultB = new List<string>();
            List<string> diffList = new List<string>();

            compare(_LastTestJson, _TestJson, compareResultA, compareResultB, diffList, "Root");
            diffList = diffList.Where(e => !e.ToLower().StartsWith("Root.ResponseStatus".ToLower())).ToList();
            string sql = string.Format("insert into FlightBookingAutomation..ESLogCompareTestData values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", _batchNum, _executeNum, _GUID, _LastTestJson.Replace("'", "''"), _TestJson.Replace("'", "''"), (string.Join("|||", compareResultA.Select(x => HttpUtility.HtmlEncode(x)).ToList())).Replace("'", "''"), (string.Join("|||", compareResultB.Select(x => HttpUtility.HtmlEncode(x)).ToList())).Replace("'", "''"), (string.Join("|||", diffList)).Replace("'", "''"));
            SQLHelper.ExecuteSqlByNonQuery(sql);
        }

        /// <summary>
        /// 落地结果到db
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeNum"></param>
        /// <param name="listA"></param>
        /// <param name="listB"></param>
        public void process(string batchNum, string executeNum ,string Guid)
        {
            DataTable jsonPairs = jsonPair(batchNum, executeNum, Guid);
            string _executeNum = "0";
            if (jsonPairs.Rows.Count == 0)
            {
                throw new Exception(string.Format("批次：{0}，{1}不存在", batchNum, executeNum == "" ? executeNum : string.Format("执行批次：{0}，", executeNum)));
            }
            
            string _batchNum = jsonPairs.Rows[0]["BatchNum"].ToString();
            _executeNum = jsonPairs.Rows[0]["ExecuteBatchNum"].ToString();
            string _GUID = jsonPairs.Rows[0]["GUID"].ToString();
            string _ESJson = jsonPairs.Rows[0]["ESJson"].ToString();
            string _TestJson = jsonPairs.Rows[0]["TestJson"].ToString();

            deleteExistResult(_batchNum, _executeNum, _GUID);

            List<string> compareResultA = new List<string>();
            List<string> compareResultB = new List<string>();
            List<string> diffList = new List<string>();

            compare(_ESJson, _TestJson, compareResultA, compareResultB, diffList, "Root");
            diffList = diffList.Where(e => !e.ToLower().StartsWith("Root.ResponseStatus".ToLower())).ToList();
            string sql = string.Format("insert into FlightBookingAutomation..ESLogCompareData values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", _batchNum, _executeNum, _GUID.Replace("'", "''"), _ESJson.Replace("'", "''"), _TestJson.Replace("'", "''"), (string.Join("|||", compareResultA)).Replace("'", "''"), (string.Join("|||", compareResultB)).Replace("'", "''"), (string.Join("|||", diffList)).Replace("'", "''"));
            SQLHelper.ExecuteSqlByNonQuery(sql);

            jsonPairs = jsonPair_Test(batchNum, executeNum, Guid);
            _executeNum = "0";
            if (jsonPairs.Rows.Count == 0)
            {
                throw new Exception(string.Format("批次：{0}，{1}不存在", batchNum, executeNum == "" ? executeNum : string.Format("执行批次：{0}，", executeNum)));
            }

            _batchNum = jsonPairs.Rows[0]["BatchNum"].ToString();
            _executeNum = jsonPairs.Rows[0]["ExecuteBatchNum"].ToString();
            _GUID = jsonPairs.Rows[0]["GUID"].ToString();
            string _LastTestJson = jsonPairs.Rows[0]["LastTestJson"].ToString();
            _TestJson = jsonPairs.Rows[0]["TestJson"].ToString();

            deleteExistResult_Test(_batchNum, _executeNum, _GUID);

            compareResultA = new List<string>();
            compareResultB = new List<string>();
            diffList = new List<string>();

            compare(_LastTestJson, _TestJson, compareResultA, compareResultB, diffList, "Root");
            diffList = diffList.Where(e => !e.ToLower().StartsWith("Root.ResponseStatus".ToLower())).ToList();
            sql = string.Format("insert into FlightBookingAutomation..ESLogCompareTestData values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", _batchNum, _executeNum, _GUID, _LastTestJson.Replace("'", "''"), _TestJson.Replace("'", "''"), (string.Join("|||", compareResultA)).Replace("'", "''"), (string.Join("|||", compareResultB)).Replace("'", "''"), (string.Join("|||", diffList)).Replace("'", "''"));
            SQLHelper.ExecuteSqlByNonQuery(sql);
        }

        /// <summary>
        /// 查询数据库返回要对比的两个字符串对象
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeNum"></param>
        /// <returns></returns>
        public DataTable jsonPair(string batchNum, string executeNum,string guid="")
        {
            if (executeNum != "")
            {
                executeNum = string.Format(" and b.ExecuteBatchNum = '{0}'", executeNum); 
            }
            if (guid != "")
            {
                guid = string.Format(" and a.GUID = '{0}'", guid);
            }
            string sql = string.Format("SELECT a.BatchNum,b.ExecuteBatchNum,a.GUID,a.ResponseData 'ESJson',b.ResponseData 'TestJson' from FlightBookingAutomation..ESLogMobileWS a,FlightBookingAutomation..[ESLogExecuteData] b where a.BatchNum = b.BatchNum and a.GUID = b.GUID and a.BatchNum = '{0}'{1}{2} order by a.BatchNum,b.ExecuteBatchNum", batchNum, executeNum,guid);

            return SQLHelper.ExecuteSql(sql);
        }

        /// <summary>
        /// 查询数据库返回要对比的两个字符串对象，和上周版本代码对比
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeNum"></param>
        /// <returns></returns>
        public DataTable jsonPair_Test(string batchNum, string executeNum,string guid="")
        {
            if (executeNum != "")
            {
                executeNum = string.Format(" and ExecuteBatchNum = '{0}'", executeNum);
            }
            if (guid != "")
            {
                guid = string.Format(" and GUID = '{0}'", guid);
            }
            string sql = string.Format("SELECT BatchNum,ExecuteBatchNum,GUID,LastResponseData 'LastTestJson',ResponseData 'TestJson' from FlightBookingAutomation..[ESLogExecuteData] where BatchNum = '{0}'{1}{2} order by BatchNum,ExecuteBatchNum", batchNum, executeNum,guid);

            return SQLHelper.ExecuteSql(sql);
        }

        /// <summary>
        /// 删除已经存在的对比结果数据
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeNum"></param>
        public void deleteExistResult(string batchNum,string executeNum,string guid="")
        {
            if (batchNum.Trim() == "" || executeNum.Trim() == "")
            {
                return;
            }
            if (guid != "")
            {
                guid = string.Format(" and GUID = '{0}'", guid);
            }

            string sql = string.Format("DELETE FROM FlightBookingAutomation..ESLogCompareData WHERE BatchNum = '{0}' AND ExecuteBatchNum = '{1}'{2}", batchNum, executeNum, guid);
            SQLHelper.ExecuteSqlByNonQuery(sql);
        }

        /// <summary>
        /// 删除已经存在的测试环境和测试环境对比结果数据
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeNum"></param>
        public void deleteExistResult_Test(string batchNum, string executeNum, string guid = "")
        {
            if (batchNum.Trim() == "" || executeNum.Trim() == "")
            {
                return;
            }
            if (guid != "")
            {
                guid = string.Format(" and GUID = '{0}'", guid);
            }

            string sql = string.Format("DELETE FROM FlightBookingAutomation..ESLogCompareTestData WHERE BatchNum = '{0}' AND ExecuteBatchNum = '{1}'{2}", batchNum, executeNum, guid);
            SQLHelper.ExecuteSqlByNonQuery(sql);
        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="ExecuteBatchNum"></param>
        /// <param name="Status">F：失败，S：成功，P：处理中</param>
        private void updateExeBatchCompareStatus(string batchNum, string ExecuteBatchNum, string Status)
        {
            string sql = string.Format("update FlightBookingAutomation..ESLogExecuteBatch set CompareStatus = '{0}' where BatchNum = '{1}'{2}", Status, batchNum, ExecuteBatchNum.Trim() == "" ? "" : string.Format(" AND ExecuteBatchNum = '{0}'",ExecuteBatchNum));
            SQLHelper.ExecuteSqlByNonQuery(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));
        }
    }
}