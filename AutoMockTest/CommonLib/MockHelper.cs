﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Data;
using System.Xml.Linq;
using System.Net;
using System.Threading;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using Ctrip.Automation.Framework.Lib;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace AutoMockTest.CommonLib
{
    public enum Scope
    {
        AfterServiceOrderChange = 1,
        AfterServiceCheckIn =2,
        AfterServiceFlightVariable = 4
    }
    public class MockHelper
    {
        public static string OpenSOAMockUrl = "http://mock.fws.qa.nt.ctripcorp.com/SuiteApi/xml/reply/SetCaseApiMockStatus";
        public static string CloseSOAMockUrl = "http://mock.fws.qa.nt.ctripcorp.com/SuiteApi/xml/reply/SetCaseApiMockStatus";
        public static string GetMockStatusUrl = "http://mock.fws.qa.nt.ctripcorp.com/SuiteApi/json/reply/GetCaseApiInfo";
        public static string SetApiResponseUrl = "http://mock.fws.qa.nt.ctripcorp.com/SuiteApi/xml/reply/SetCaseApiMessage";
        public static string ServerIp_Release = "http://10.2.74.57";
        public static string ServerIp_Pro = "http://10.2.75.32";
        public static string AddMockUrl = "http://mock.fws.qa.nt.ctripcorp.com/SoaCase/AddCaseInterface";
        public static object locker = new object();//读写开关表的锁
        public static object checklocker = new object();//更新mock平台的锁
        public static List<string> needAddList = new List<string>();
        List<string> mockBackUp = null;

        public Hashtable MockCfgTb = null;
        public string Auth = string.Empty;
        private DataTable serverCfg = new DataTable();
        
        //对应的六套suit基本配置
        private List<MockModel> modelList = new List<MockModel>();
        private static Dictionary<string, Hashtable> statusDic = getStatus();

        //抢占资源时，标识抢占哪个case【0~5 对应 1~6套case】
        private static int caseFlag = 0;
       
        public MockHelper(Scope scope)
        {
            MockCfgTb = getConfig((int)scope);
            //Auth = getAuth();
            modelList = getModelList();
            mockBackUp = new List<string>();
            serverCfg = getServerCfg();
            //检测开关表是否缺项
            CheckDicAndApi();
        }

        private string getAuth()
        {
            return Utility.getAuth();
        }

        private List<MockModel> getModelList()
        {
    
            return Utility.getMockModelList();
        }

        private static Dictionary<string, Hashtable> getStatus()
        {
            string reset = Utility.Reset();
            return Utility.getStatus();
        }

        /// <summary>
        /// 获取soa接口配置
        /// </summary>
        /// <param name="soaName">接口名</param>
        /// <returns></returns>
        public Hashtable getConfig(int scope)
        {
            string sql = string.Format("SELECT  ServiceName,ApiID FROM [FlightBookingAutomation].[dbo].MockIDConfig where Scope & {0} = {0}", scope, scope);
            
            DataTable MockConfig = SQLHelper.ExecuteSql(sql);
            Hashtable MockCfgTb = new Hashtable();

            foreach (DataRow eachRow in MockConfig.Rows)
            {
                MockCfgTb[eachRow["ServiceName"].ToString().ToLower()] = eachRow["ApiID"].ToString();
            }

            return MockCfgTb;
        }

        private DataTable getServerCfg()
        {
            string sql = "SELECT * FROM FlightBookingAutomation..ESLogServerConfig order by path DESC";
            return SQLHelper.ExecuteSql(sql);
        }

        /// <summary>
        /// 开启关闭mock
        /// </summary>
        /// <param name="soaName">soa名</param>
        /// <param name="open">true开启，false关闭</param>
        public void openMock(string soaName, bool open, bool IgnoreLock = false)
        {
            string url;

            if (open)
            {
                openMockNew(soaName, open, IgnoreLock);
            }
            else
                closeMockNew(soaName);
        }


        public void openMockNew(string soaName, bool open, bool IgnoreLock = false)
        {
            string url = OpenSOAMockUrl;

            string request = string.Format("<SetCaseApiMockStatus xmlns:i='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://schemas.datacontract.org/2004/07/ServiceMock.RestApi.Suite'>  <ApiID>{0}</ApiID>  <ApiType>0</ApiType>  <CaseID>1347</CaseID> <IsMock>true</IsMock>  <UserDomain>CN1</UserDomain>  <UserName>ji_cy</UserName></SetCaseApiMockStatus>", MockCfgTb[soaName.ToLower()].ToString());

            for (int i = 0; i < 6; i++)
            {
                if (getMockStatus(soaName)["MessageBody"]["IsMocking"].ToString().ToLower() == "false")
                    break;
                if (IgnoreLock)
                    break;
                if (!open)
                    break;

                Thread.Sleep(10000);
            }
            string response = HttpPost(url, request);

            CheckResponse(request, response);
        }

        public void closeMockNew(string soaName, string CaseId = "1347")
        {
            string url = CloseSOAMockUrl;

            string request = string.Format("<SetCaseApiMockStatus xmlns:i='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://schemas.datacontract.org/2004/07/ServiceMock.RestApi.Suite'>  <ApiID>{0}</ApiID>  <ApiType>0</ApiType>  <CaseID>{1}</CaseID> <IsMock>false</IsMock>  <UserDomain>CN1</UserDomain>  <UserName>ji_cy</UserName></SetCaseApiMockStatus>", MockCfgTb[soaName.ToLower()].ToString(), CaseId);

            string response = HttpPost(url, request);
            CheckResponse(request, response);
        }

        public JObject getMockStatus(string soaName,string caseid="1347")
        {
            string url = GetMockStatusUrl;


            string request = string.Format("{{\"CaseID\":\"{0}\",\"ApiID\":\"{1}\",\"ApiType\":\"0\",\"UserDomain\":\"cn1\",\"UserName\":\"ji_cy\"}}", caseid,MockCfgTb[soaName.ToLower()].ToString());

            return JObject.Parse(HttpPost(url, request, "application/json"));
        }


        /// <summary>
        /// 更新mock接口报文
        /// </summary>
        /// <param name="soaName"></param>
        /// <param name="orignalXml"></param>
        public void saveMockInfo(string soaName, string orignalXml, string CompressType, bool IgnoreLock = false)
        {
            if (!mockBackUp.Contains(soaName))
            {
                lock (mockBackUp)
                {
                    mockBackUp.Add(soaName);
                }
            }
            openMock(soaName, true, IgnoreLock);            

            string url;
            
            url = SetApiResponseUrl;


            string request = buildRequest(MockCfgTb[soaName.ToLower()].ToString(), orignalXml, CompressType);
            string response = HttpPost(url, request);

            CheckResponse(request,response);
        }

        private static void CheckResponse(string request, string response)
        {
            Xml rsp = new Xml(RemoveAllNamespaces(XElement.Parse(response)).ToString());
            if (rsp.GetElementValue("Message").ToLower() != "success")
            {
                Exception exception = new Exception(string.Format("[Response]:{0};[Request]:{1}", response, request));
                throw exception;
            }
        }

        /// <summary>
        /// 恢复Mock
        /// </summary>
        public void restoreMock()
        {
            foreach (string soaname in mockBackUp)
            {
                try
                {
                    openMock(soaname, false);
                }
                catch (NullReferenceException e)
                {
                    Utility.RecordLog(string.Format("接口名：{0}未配置对应mockid！", soaname), e.StackTrace == null ? "" : e.StackTrace);
                }
                catch (Exception e)
                {
                    Utility.RecordLog(e.Message, e.StackTrace == null ? "" : e.StackTrace);
                }
            }

            mockBackUp = new List<string>();
        }

        /// <summary>
        /// 拼接保存mock接口请求的报文
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="orignalXml"></param>
        /// <returns></returns>
        public static string buildRequest(string prefix, string orignalXml, string CompressType, string caseId = "1347")
        {
            string messageType = string.Empty;

            switch (CompressType.ToUpper())
            {
                case "XML":
                    messageType = "0";
                    break;
                case "JSON":
                    messageType = "1";
                    break;
                case "BJJSON":
                    messageType = "2";
                    break;
                default:
                    messageType = string.Empty;
                    break;
            }

            if (messageType == string.Empty)//补偿
            {
                try
                {
                    XmlDocument _Document = new XmlDocument();
                    _Document.LoadXml(orignalXml);
                    messageType = "0";
                }
                catch
                {
                    messageType = "1";
                }
            }
            

            string request = string.Format(@"<SetCaseApiMessage xmlns:i='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://schemas.datacontract.org/2004/07/ServiceMock.RestApi.Suite'>
  <ApiID>{0}</ApiID>
  <ApiType>0</ApiType>
  <CaseID>{3}</CaseID>
  <MessageContent>{1}</MessageContent>
  <MessageType>{2}</MessageType>
  <UserDomain>CN1</UserDomain>
  <UserName>ji_cy</UserName>
</SetCaseApiMessage>", prefix, HttpUtility.UrlEncode(orignalXml), messageType, caseId);
            return request;

        }

        /// <summary>
        /// http请求方法
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="postDataStr"></param>
        /// <returns></returns>
        public static string HttpPost(string Url, string postDataStr,string ContentType = "")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";
            request.ContentType = "application/xml";
            request.Timeout = 60000;
            if (ContentType != "")
                request.ContentType = ContentType;
            //request.Credentials = new NetworkCredential(@"l.cui", "Cuilin19820514", "cn1");
            //request.ContentLength = Encoding.UTF8.GetByteCount(postDataStr);
            //request.CookieContainer = cookie;
            Stream myRequestStream = request.GetRequestStream();
            StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding("utf-8"));
            myStreamWriter.Write(postDataStr);
            myStreamWriter.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //response.Cookies = cookie.GetCookies(response.ResponseUri);
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;
        }

        /// <summary>
        /// http请求方法
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="postDataStr"></param>
        /// <returns></returns>
        public static string HttpGet(string Url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";

            try
            {
                Thread.Sleep(1000);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //response.Cookies = cookie.GetCookies(response.ResponseUri);
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("gb2312"));
                string retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();

                return retString;

            }
            catch (WebException e)
            {
                return "false";
            }
        }

        #region XML相关
        //Implemented based on interface, not part of algorithm

        public static XElement RemoveAllNamespaces(XElement e)
        {
            return new XElement(e.Name.LocalName,
              (from n in e.Nodes()
               select ((n is XElement) ? RemoveAllNamespaces(n as XElement) : n)),
                  (e.HasAttributes) ?
                    (from a in e.Attributes()
                     where (!a.IsNamespaceDeclaration)
                     select new XAttribute(a.Name.LocalName, a.Value)) : null);
        }
        #endregion

        public static DataTable getMobileWSFromDB(string batchNum, string guid = "")
        {
            string sql = string.Format("SELECT GUID,Path,AppID,RequestData as 'MobileWSReq'  FROM [FlightBookingAutomation].[dbo].ESLogMobileWS where BatchNum = '{0}' ORDER BY GUID DESC", batchNum);
            if (guid != "")
                sql = string.Format("SELECT GUID,Path,AppID,RequestData as 'MobileWSReq' FROM [FlightBookingAutomation].[dbo].ESLogMobileWS  where BatchNum = '{0}' and GUID='{1}' ORDER BY GUID DESC", batchNum, guid);

            DataTable MockInfo = SQLHelper.ExecuteSql(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));
            return MockInfo;
        }

        public static DataTable getMockInfoFromDB(string batchNum,string guid="")
        {
            string sql = string.Format("SELECT a.GUID,a.Path,a.AppID,a.RequestData as 'MobileWSReq',ServiceName,b.ResponseData as 'MockResponse',b.CompressType FROM [FlightBookingAutomation].[dbo].ESLogMobileWS a, [FlightBookingAutomation].[dbo].[ESLogMockData] b where a.BatchNum = '{0}' and a.BatchNum = b.BatchNum and a.GUID = b.GUID ORDER BY GUID DESC", batchNum);
            if(guid != "")
                sql = string.Format("SELECT a.GUID,a.Path,a.AppID,a.RequestData as 'MobileWSReq',ServiceName,b.ResponseData as 'MockResponse',b.CompressType  FROM [FlightBookingAutomation].[dbo].ESLogMobileWS a, [FlightBookingAutomation].[dbo].[ESLogMockData] b where a.BatchNum = '{0}' and a.BatchNum = b.BatchNum and a.GUID = b.GUID and a.GUID='{1}' ORDER BY GUID DESC", batchNum, guid);
            
            DataTable MockInfo = SQLHelper.ExecuteSql(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr","FAT"));
            return MockInfo;
        }

        
        /// <summary>
        /// 通过轮询caseid进行mock
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="ExecuteBatchNum"></param>
        /// <returns></returns>

       public int beginMockByBatchNumNew(string batchNum, int ExecuteBatchNum)
        {
            List<Task> taskList = new List<Task>();
            DataTable MockInfo = getMockInfoFromDB(batchNum);


            Dictionary<string,MockEntity> mockList = new Dictionary<string,MockEntity>();
            string currentGuid = string.Empty;
            string filterGuid = string.Empty;
            string path = string.Empty;
            string appid = string.Empty;
            string mobileWSReq = string.Empty;
            string ServiceName = string.Empty;
            string ResponseData = string.Empty;
            string CompressType = string.Empty;
            string mobileWSRsp = string.Empty;
            string mobileWSRspPro = string.Empty;
            bool needMock = true;
            bool noError = true;

            if (MockInfo.Rows.Count != 0)
            {

                for (int i = 0; i < MockInfo.Rows.Count; i++)
                {
                    currentGuid = MockInfo.Rows[i]["GUID"].ToString();
                    path = MockInfo.Rows[i]["Path"].ToString();
                    appid = MockInfo.Rows[i]["AppID"].ToString();
                    mobileWSReq = MockInfo.Rows[i]["MobileWSReq"].ToString();
                    ServiceName = MockInfo.Rows[i]["ServiceName"].ToString();
                    ResponseData = MockInfo.Rows[i]["MockResponse"].ToString();
                    CompressType = MockInfo.Rows[i]["CompressType"].ToString();

                    needMock = true;

                    //暂时不支持相同接口多个mock返回，跳过
                    if (!mockList.ContainsKey(ServiceName) && currentGuid != filterGuid)
                    {
                        if (needMock)
                        {
                            MockEntity me = new MockEntity();
                            me.ResponseData = ResponseData;
                            me.CompressType = CompressType;
                            mockList.Add(ServiceName, me);
                        }
                    }
                    else
                    {
                        filterGuid = currentGuid;
                        mockList.Clear();
                        continue;
                    }


                    //扫描到下一个的guid是新的或者扫描到最后，开始处理当前guid
                    if (currentGuid != filterGuid && (i == MockInfo.Rows.Count - 1 || currentGuid != MockInfo.Rows[i + 1]["GUID"].ToString()))
                    {
                        string caseid = "";
                        try
                        {
                            caseid = getCaseID(mockList);

                            MockModel mockModel = modelList.Find(e => e.Caseid == caseid);

                            foreach (string eachKey in mockList.Keys)
                            {
                                Task eachTask = null;
                                string eachServiceName = eachKey;
                                string eachResponsetemp = mockList[eachKey].ResponseData.ToString();
                                string eachResponse = Utility.UpdateUid(eachResponsetemp, mockModel.Uid);
                                //eachResponse = Utility.ConvertDate(eachResponse);//json报文中的日期改成UNIX形式
                                
                                string eachCompressType = mockList[eachKey].CompressType.ToString();
                                
                                eachTask = Task.Factory.StartNew(() =>
                                {
                                    saveMockInfoByCaseID(eachServiceName, eachResponse, eachCompressType,caseid);
                                });
                                taskList.Add(eachTask);
                            }
                            Task.WaitAll(taskList.ToArray());//并行打开mock

                            string _ServerIP_Release = ServerIp_Release;
                            string _ServerIP_Pro = ServerIp_Pro;

                            foreach (DataRow eachRow in serverCfg.Rows)
                            {
                                if (path.Trim().ToLower() == eachRow["Path"].ToString().Trim().ToLower() || appid.Trim().ToLower() == eachRow["AppID"].ToString().Trim().ToLower())
                                {
                                    _ServerIP_Release = "http://" + eachRow["CurrentMarkIP"].ToString() + eachRow["CurrentMarkExt"].ToString();
                                    _ServerIP_Pro = "http://" + eachRow["BenchMarkIP"].ToString() + eachRow["BenchMarkExt"].ToString();
                                    break;
                                }
                            }

                            string LastTestlogToken = Guid.NewGuid().ToString("N");//关联测试日志表的token
                            mobileWSRspPro = BeginPost(_ServerIP_Pro, path, mobileWSReq, mockModel.Auth, cid: mockModel.Clientid, pvid: LastTestlogToken);
                            string TestlogToken = Guid.NewGuid().ToString("N");//关联测试日志表的token
                            mobileWSRsp = BeginPost(_ServerIP_Release, path, mobileWSReq, mockModel.Auth, cid: mockModel.Clientid, pvid: TestlogToken);
                            log2DB(batchNum, ExecuteBatchNum, currentGuid, path, mobileWSRsp, mobileWSRspPro);

                            CatLogHelper clh = new CatLogHelper();
                            clh.Process(batchNum, ExecuteBatchNum, currentGuid, LastTestlogToken, TestlogToken, _ServerIP_Pro, _ServerIP_Release, DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            noError = false;
                            if (e.InnerException != null)
                                Utility.RecordLog(e.InnerException.Message, e.InnerException.StackTrace == null ? "" : e.InnerException.StackTrace);
                            else
                                Utility.RecordLog(string.Format("Path:{0} Req:{1} Error:{2}", path, mobileWSReq, e.Message), e.StackTrace == null ? "" : e.StackTrace);
                        }
                        finally
                        {
                            taskList.Clear();
                            mockList.Clear();
                            restoreMockByCaseID(caseid);
                        }
                    }
                }
            }
            else
            {
                DataTable MobileWSInfo = MockHelper.getMobileWSFromDB(batchNum);

                for (int i = 0; i < MobileWSInfo.Rows.Count; i++)
                {
                    currentGuid = MobileWSInfo.Rows[i]["GUID"].ToString();
                    path = MobileWSInfo.Rows[i]["Path"].ToString();
                    appid = MobileWSInfo.Rows[i]["AppID"].ToString();
                    mobileWSReq = MobileWSInfo.Rows[i]["MobileWSReq"].ToString();

                    try
                    {
                        string _ServerIP_Release = ServerIp_Release;
                        string _ServerIP_Pro = ServerIp_Pro;

                        foreach (DataRow eachRow in serverCfg.Rows)
                        {
                            if (path.Trim().ToLower() == eachRow["Path"].ToString().Trim().ToLower() || appid.Trim().ToLower() == eachRow["AppID"].ToString().Trim().ToLower())
                            {
                                _ServerIP_Release = "http://" + eachRow["CurrentMarkIP"].ToString() + eachRow["CurrentMarkExt"].ToString();
                                _ServerIP_Pro = "http://" + eachRow["BenchMarkIP"].ToString() + eachRow["BenchMarkExt"].ToString();
                                break;
                            }
                        }
                        string TestlogToken = Guid.NewGuid().ToString("N");//关联测试日志表的token
                        mobileWSRsp = BeginPost(_ServerIP_Release, path, mobileWSReq, modelList[0].Auth, pvid: TestlogToken);
                        string LastTestlogToken = Guid.NewGuid().ToString("N");//关联测试日志表的token
                        mobileWSRspPro = BeginPost(_ServerIP_Pro, path, mobileWSReq, modelList[0].Auth, pvid: LastTestlogToken);
                        log2DB(batchNum, ExecuteBatchNum, currentGuid, path, mobileWSRsp, mobileWSRspPro);

                        CatLogHelper clh = new CatLogHelper();
                        clh.Process(batchNum, ExecuteBatchNum, currentGuid, LastTestlogToken, TestlogToken, _ServerIP_Pro, _ServerIP_Release, DateTime.Now);
                    }
                    catch (Exception e)
                    {
                        noError = false;
                        if (e.InnerException != null)
                            Utility.RecordLog(e.InnerException.Message, e.InnerException.StackTrace == null ? "" : e.InnerException.StackTrace);
                        else
                            Utility.RecordLog(string.Format("Path:{0} Req:{1} Error:{2}", path, mobileWSReq, e.Message), e.StackTrace == null ? "" : e.StackTrace);
                    }
                }
            }
            if (noError)
                updateExeBatchStatus(batchNum, ExecuteBatchNum.ToString(), "S");
            else
                updateExeBatchStatus(batchNum, ExecuteBatchNum.ToString(), "F");
            

           //调试时执行完成触发对比
           CompareHelper ch = new CompareHelper();
           ch.process(batchNum, ExecuteBatchNum.ToString());

            return ExecuteBatchNum;
        }


       /// <summary>
       /// 通过轮询caseid进行mock
       /// </summary>
       /// <param name="batchNum"></param>
       /// <param name="ExecuteBatchNum"></param>
       /// <returns></returns>
       public void  beginMockByBatchNumNew(string batchNum, int ExecuteBatchNum,string guid)
       {
           List<Task> taskList = new List<Task>();
           DataTable MockInfo = getMockInfoFromDB(batchNum,guid);


           Dictionary<string, MockEntity> mockList = new Dictionary<string, MockEntity>();
           string currentGuid = string.Empty;
           string filterGuid = string.Empty;
           string path = string.Empty;
           string appid = string.Empty;
           string mobileWSReq = string.Empty;
           string ServiceName = string.Empty;
           string ResponseData = string.Empty;
           string CompressType = string.Empty;
           string mobileWSRsp = string.Empty;
           string mobileWSRspPro = string.Empty;
           bool needMock = true;

           if (MockInfo.Rows.Count != 0)
           {

               for (int i = 0; i < MockInfo.Rows.Count; i++)
               {
                   currentGuid = MockInfo.Rows[i]["GUID"].ToString();
                   path = MockInfo.Rows[i]["Path"].ToString();
                   appid = MockInfo.Rows[i]["AppID"].ToString();
                   mobileWSReq = MockInfo.Rows[i]["MobileWSReq"].ToString();
                   ServiceName = MockInfo.Rows[i]["ServiceName"].ToString();
                   ResponseData = MockInfo.Rows[i]["MockResponse"].ToString();
                   CompressType = MockInfo.Rows[i]["CompressType"].ToString();

                   needMock = true;

                   //暂时不支持相同接口多个mock返回，跳过
                   if (!mockList.ContainsKey(ServiceName))
                   {
                       if (needMock)
                       {
                           MockEntity me = new MockEntity();
                           me.ResponseData = ResponseData;
                           me.CompressType = CompressType;
                           mockList.Add(ServiceName, me);
                       }
                   }
                   else
                   {
                       filterGuid = currentGuid;
                       mockList.Clear();
                       continue;
                   }

                   //扫描到下一个的guid是新的或者扫描到最后，开始处理当前guid
                   if (currentGuid != filterGuid && (i == MockInfo.Rows.Count - 1 || currentGuid != MockInfo.Rows[i + 1]["GUID"].ToString()))
                   {
                       string caseid = getCaseID(mockList);

                       try
                       {
                           MockModel mockModel = modelList.Find(e => e.Caseid == caseid);

                           foreach (string eachKey in mockList.Keys)
                           {
                               Task eachTask = null;
                               string eachServiceName = eachKey;
                               string eachResponsetemp = mockList[eachKey].ResponseData.ToString();
                               string eachResponse = Utility.UpdateUid(eachResponsetemp, mockModel.Uid);
                               //ToDo:发布时需要删除这段
                               if (eachServiceName.ToLower() == "orderdetailsearch.openorderdetailsearch".ToLower())
                               {
                                   string orderid = string.Empty;
                                   orderid = Regex.Match(Regex.Match(mobileWSReq, @"""oid"":\d+").Value, @"\d+").Value;
                                   if (orderid != "")
                                   {
                                       eachResponse = Utility.UpdateOrderId(eachResponse, orderid);
                                   }
                               }
                               string eachCompressType = mockList[eachKey].CompressType.ToString();
                               eachTask = Task.Factory.StartNew(() =>
                               {
                                   saveMockInfoByCaseID(eachServiceName, eachResponse, eachCompressType, caseid);
                               });
                               taskList.Add(eachTask);
                           }
                           Task.WaitAll(taskList.ToArray());//并行打开mock

                           string _ServerIP_Release = ServerIp_Release;
                           string _ServerIP_Pro = ServerIp_Pro;

                           foreach (DataRow eachRow in serverCfg.Rows)
                           {
                               if (path.Trim().ToLower() == eachRow["Path"].ToString().Trim().ToLower() || appid.Trim().ToLower() == eachRow["AppID"].ToString().Trim().ToLower())
                               {
                                   _ServerIP_Release = "http://" + eachRow["CurrentMarkIP"].ToString() + eachRow["CurrentMarkExt"].ToString();
                                   _ServerIP_Pro = "http://" + eachRow["BenchMarkIP"].ToString() + eachRow["BenchMarkExt"].ToString();
                                   break;
                               }
                           }
                           string TestlogToken = Guid.NewGuid().ToString("N");//关联测试日志表的token
                           mobileWSRsp = BeginPost(_ServerIP_Release, path, mobileWSReq, mockModel.Auth, cid: mockModel.Clientid, pvid: TestlogToken);
                           string LastTestlogToken = Guid.NewGuid().ToString("N");//关联测试日志表的token
                           mobileWSRspPro = BeginPost(_ServerIP_Pro, path, mobileWSReq, mockModel.Auth, cid: mockModel.Clientid, pvid: LastTestlogToken);
                           log2DB(batchNum, ExecuteBatchNum, currentGuid, path, mobileWSRsp, mobileWSRspPro);

                           CatLogHelper clh = new CatLogHelper();
                           clh.Process(batchNum, ExecuteBatchNum, currentGuid, LastTestlogToken, TestlogToken, _ServerIP_Pro, _ServerIP_Release, DateTime.Now);
                       }
                       catch (Exception e)
                       {

                           Utility.RecordLog(e.Message, e.StackTrace == null ? "" : e.StackTrace);

                       }
                       finally
                       {
                           taskList.Clear();
                           mockList.Clear();
                           restoreMockByCaseID(caseid);
                       }
                   }
               }
           }
           else
           {
               DataTable MobileWSInfo = MockHelper.getMobileWSFromDB(batchNum,guid);

               for (int i = 0; i < MobileWSInfo.Rows.Count; i++)
               {
                   currentGuid = MobileWSInfo.Rows[i]["GUID"].ToString();
                   path = MobileWSInfo.Rows[i]["Path"].ToString();
                   appid = MobileWSInfo.Rows[i]["AppID"].ToString();
                   mobileWSReq = MobileWSInfo.Rows[i]["MobileWSReq"].ToString();

                   try
                   {
                       string _ServerIP_Release = ServerIp_Release;
                       string _ServerIP_Pro = ServerIp_Pro;

                       foreach (DataRow eachRow in serverCfg.Rows)
                       {
                           if (path.Trim().ToLower() == eachRow["Path"].ToString().Trim().ToLower() || appid.Trim().ToLower() == eachRow["AppID"].ToString().Trim().ToLower())
                           {
                               _ServerIP_Release = "http://" + eachRow["CurrentMarkIP"].ToString() + eachRow["CurrentMarkExt"].ToString();
                               _ServerIP_Pro = "http://" + eachRow["BenchMarkIP"].ToString() + eachRow["BenchMarkExt"].ToString();
                               break;
                           }
                       }
                       string TestlogToken = Guid.NewGuid().ToString("N");//关联测试日志表的token
                       mobileWSRsp = BeginPost(_ServerIP_Release, path, mobileWSReq, modelList[0].Auth, pvid: TestlogToken);
                       string LastTestlogToken = Guid.NewGuid().ToString("N");//关联测试日志表的token
                       mobileWSRspPro = BeginPost(_ServerIP_Pro, path, mobileWSReq, modelList[0].Auth, pvid: LastTestlogToken);

                       log2DB(batchNum, ExecuteBatchNum, currentGuid, path, mobileWSRsp, mobileWSRspPro);

                       CatLogHelper clh = new CatLogHelper();
                       clh.Process(batchNum, ExecuteBatchNum, currentGuid, LastTestlogToken, TestlogToken, _ServerIP_Pro, _ServerIP_Release, DateTime.Now);
                   }
                   catch (Exception e)
                   {
                       Utility.RecordLog(e.Message, e.StackTrace == null ? "" : e.StackTrace);
                   }
               }
           }
           //执行完成触发对比
           CompareHelper ch = new CompareHelper();
           ch.process(batchNum, ExecuteBatchNum.ToString(), guid);
       }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="ExecuteBatchNum"></param>
        /// <param name="Status">F：失败，S：成功，P：处理中</param>
        private void updateExeBatchStatus(string batchNum, string ExecuteBatchNum, string Status)
        {
            string sql = string.Format("update FlightBookingAutomation..ESLogExecuteBatch set Status = '{0}' where BatchNum = '{1}' AND ExecuteBatchNum = '{2}'", Status, batchNum, ExecuteBatchNum);
            SQLHelper.ExecuteSqlByNonQuery(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));
        }

        

        /// <summary>
        /// 运行结果落地
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="ExecuteBatchNum"></param>
        /// <param name="currentGuid"></param>
        /// <param name="path"></param>
        /// <param name="mobileWSRsp"></param>
        private void log2DB(string batchNum, int ExecuteBatchNum, string currentGuid, string path, string mobileWSRsp,string mobileWSRspPro)
        {
            string sql = string.Format("delete from FlightBookingAutomation..ESLogExecuteData where BatchNum = '{0}' and ExecuteBatchNum = '{1}' and GUID = '{2}' insert into FlightBookingAutomation..ESLogExecuteData Values('{0}','{1}','{2}','{3}','{4}','{5}',getdate())", batchNum, ExecuteBatchNum, currentGuid, path, mobileWSRsp.Replace("'", "''"), mobileWSRspPro.Replace("'", "''"));
            SQLHelper.ExecuteSqlByNonQuery(sql);
        }

        /// <summary>
        /// 获取批次号新的运行批次号
        /// </summary>
        /// <param name="batchNum"></param>
        /// <returns></returns>
        public int getExecuteNoByBatchNum(string batchNum)
        {
            string sql = string.Format("SELECT max(ExecuteBatchNum) as 'ExecuteBatchNum'  FROM [FlightBookingAutomation].[dbo].[ESLogExecuteBatch] where BatchNum = '{0}'", batchNum);

            string currentNum = SQLHelper.ExecuteSqlByScalar(sql).ToString();
            int num = 0;            
            num = (currentNum == "" ? 0 : int.Parse(currentNum))+1;
            if (num != 0)
            {
                sql = string.Format("INSERT into FlightBookingAutomation..ESLogExecuteBatch VALUES ('{0}','{1}',GETDATE(),'P','U')", batchNum, num);
                SQLHelper.ExecuteSqlByNonQuery(sql);
            }
            return num;
        }

        /// <summary>
        /// 发起后服务的请求
        /// </summary>
        /// <param name="path"></param>
        /// <param name="mobileWSReq"></param>
        /// <param name="Auth"></param>
        /// <returns></returns>
        private string BeginPost(string serverip, string path, string mobileWSReq,string Auth,string cid = "32001078510035788787" ,string pvid = "")
        {
            //string url = "http://ws.afterservice.mobile.flight.fat389.qa.nt.ctripcorp.com" + path;
            string url = serverip + path;
            string req = string.Empty;
            if(Regex.IsMatch(mobileWSReq, @"""auth""( )*:( )*"".+?"","))
                req = Regex.Replace(mobileWSReq, @"""auth""( )*:( )*"".+?"",", string.Format(@"""auth"": ""{0}"",", Auth));//替换Auth
            else
            {
                JObject jsonReq = JObject.Parse(mobileWSReq);
                JObject jsonHead = jsonReq["head"] as JObject;
                jsonHead.Add("auth", Auth);
                req = jsonReq.ToString();
            }

            req = Regex.Replace(req, @"""cid""( )*:( )*"".+?"",", string.Format(@"""cid"": ""{0}"",",cid));//替换CID
            req = Regex.Replace(req, @"""extension""( )*:( )*\[.*?\]", string.Format(@"""extension"":[{0}]", pvid == "" ? "" : string.Format(@"{{""name"":""pvid"",""value"":""{0}""}}", pvid)));//去除extension

            string response = HttpPost(url, req, "application/json");
            return response;

        }



        public static void Log(string caseName, bool isDomestic, List<string> orderids, Exception e = null)
        {
            string result = e == null ? "PASS" : "FAIL";
            string loginfo = "";
            string orderidStr = "";
            foreach (string orderid in orderids)
                orderidStr += orderid + "|";
            loginfo = e == null ? "" : string.Format("Message:{0}     Source:{1}", e.Message.Replace("'", "''"), e.StackTrace.Replace("'", "''"));


            string type = "1";
            string ProjectModuleID = "1";

            string sql = string.Format("insert into FlightBookingAutomation..CreateOrderLog VALUES('{0}','{1}','{2}','{3}','{4}',GETDATE())", caseName, result, isDomestic ? 1 : 0, orderidStr, loginfo.Replace("'", "''"));
            string connectStr = SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT");


            SQLHelper.ExecuteSqlByScalar(sql, connectStr);
        }

        public static void Log(string caseName, bool isDomestic, string orderid, string ErrorMsg = "")
        {
            string result = ErrorMsg == string.Empty ? "PASS" : "FAIL";
            string loginfo = "";
            string orderidStr = orderid;

            loginfo = ErrorMsg;


            string type = "1";
            string ProjectModuleID = "1";

            string sql = string.Format("insert into FlightBookingAutomation..CreateOrderLog VALUES('{0}','{1}','{2}','{3}','{4}',GETDATE())", caseName, result, isDomestic ? 1 : 0, orderidStr, loginfo.Replace("'", "''"));
            string connectStr = SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT");


            SQLHelper.ExecuteSqlByScalar(sql, connectStr);
        }

        //得到可以用的caseID
        public string getCaseID(Dictionary<string, MockEntity> mockList) { 
        
           string caseid = "";

                 while(true){
                     lock (locker)
                     {
                         caseid = getCaseIDOnce(mockList);
                     }
                  if (!caseid.Equals(""))
                   {
                       break;
                   }
                   Thread.Sleep(10000);
                   Utility.RecordLog(string.Format("其他所有均被占用，涉及到的接口有：{0}", string.Join(",", mockList.ToArray())), "调试轮询mock专用提示");
               }

           return caseid;
        }


        //得到可用的caseid
        public string getCaseIDOnce(Dictionary<string, MockEntity> mockList)
        {
           
            string caseid = "";
            for (int i = 0; i < modelList.Count; i++)
            {
                string currentCaseid = modelList[i].Caseid;

                if (caseIsEffect(currentCaseid, mockList))
                {
                    caseid = currentCaseid;
                    break;
                }
            }

            if (!caseid.Equals(""))
            {
               //确定caseid后直接更新开关
               foreach (string eachKey in mockList.Keys)
               {
                 string apiID = MockCfgTb[eachKey.ToLower()].ToString();
                 statusDic[caseid][apiID] = true;
               }
            }

            return caseid;
        }

        public bool caseIsEffect(string caseid, Dictionary<string, MockEntity> mockList)
        {
            bool flag = true;
            bool noError = true;
            foreach (string eachKey in mockList.Keys) {
                
                //如果发现接口拿不到配置表，说明接口在数据库中没有配置
                if (MockCfgTb[eachKey.ToLower()]==null ||MockCfgTb[eachKey.ToLower()].Equals(null))
                {
                    noError = false;
                    Utility.RecordLog(string.Format("接口名：{0}未配置对应mockid！", eachKey.ToLower()));
                    continue;
                }
                string apiID = MockCfgTb[eachKey.ToLower()].ToString();
                    
                bool tempflag = Boolean.Parse(statusDic[caseid][apiID].ToString());
                
                    if (tempflag)
                    {
                    return false;
                    }
                    /*
                    if (!getMockStatusByCaseId(caseid, eachKey))
                    {
                    Utility.RecordLog(string.Format("开关检测可用，但是平台检测不可用tempflag为{0}", tempflag), "调试轮询mock专用提示");
                    return false;
                    }*/
            }

            if (!noError)
            {
                Exception e = new Exception("数据库存在未配置的接口");
                throw e;
            }

            return flag;
        }

        /// <summary>
        /// 检测下是否mock平台真的与caseid开关检测的一致，调试使用
        /// </summary>
        /// <param name="caseid"></param>
        /// <param name="mockList"></param>
        /// <returns>true为有效的情况</returns>
        public bool getMockStatusByCaseId(string caseid, string soaName) {
            bool flag = true;
            bool tempflag = getMockStatus(soaName, caseid)["MessageBody"]["IsMocking"].ToString().ToLower() == "true";
            if (tempflag)
            {
                return false;
            }
            return flag;
        }


        /// <summary>
        /// 开启关闭mock
        /// </summary>
        /// <param name="soaName">soa名</param>
        /// <param name="open">true开启，false关闭</param>
        public void openMockByCaseID(string soaName, bool open, string CaseId)
        {
   
                if (open)
                {
                    openMockNewByCaseID(soaName, open, CaseId);
                }
                else
                    closeMockNew(soaName, CaseId);
         }


        public void openMockNewByCaseID(string soaName, bool open,string CaseId)
        {
            string url = OpenSOAMockUrl;
            string apiId = MockCfgTb[soaName.ToLower()].ToString();
            string request = string.Format("<SetCaseApiMockStatus xmlns:i='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://schemas.datacontract.org/2004/07/ServiceMock.RestApi.Suite'>  <ApiID>{0}</ApiID>  <ApiType>0</ApiType>  <CaseID>{1}</CaseID> <IsMock>true</IsMock>  <UserDomain>CN1</UserDomain>  <UserName>ji_cy</UserName></SetCaseApiMockStatus>", apiId,CaseId);
            
            /*
            //todo:查错用的每次检测mock平台端口状态，应该每次都是一次过，不应该出现延迟的情况
            for (int i = 0; i < 6; i++)
            {
                if (getMockStatus(soaName,CaseId)["MessageBody"]["IsMocking"].ToString().ToLower() == "false")
                    break;
                Thread.Sleep(10000);
                Utility.RecordLog(string.Format("mock系统caseid为：{0}，openMock时出现系统允许mock，但mock平台被占用的情况，需要mock的接口名为{1}！", CaseId,soaName),"调试轮询mock专用提示");
            }
            */

            string response = HttpPost(url, request);

            CheckResponse(request, response);
        }

        /// <summary>
        /// 更新mock接口报文
        /// </summary>
        /// <param name="soaName"></param>
        /// <param name="orignalXml"></param>
        public void saveMockInfoByCaseID(string soaName, string orignalXml, string CompressType,string caseId)
        {
            if (!mockBackUp.Contains(soaName))
            {
                lock (mockBackUp)
                {
                    mockBackUp.Add(soaName);
                }
            }
            openMockByCaseID(soaName, true, caseId);

            string url;

            url = SetApiResponseUrl;


            string request = buildRequest(MockCfgTb[soaName.ToLower()].ToString(), orignalXml, CompressType, caseId);
            string response = HttpPost(url, request);

            CheckResponse(request, response);
        }

        /// <summary>
        /// 轮询版恢复Mock
        /// </summary>
        public void restoreMockByCaseID(string caseid)
        {

            foreach (string soaname in mockBackUp)
            {
                try
                {
                    lock (locker)
                    {
                    //关远端mock平台的
                    openMockByCaseID(soaname, false, caseid);
                    string api = MockCfgTb[soaname.ToLower()].ToString();
                    //更新本地开关
                    statusDic[caseid][api] = false;
                    }
                }
                catch (NullReferenceException e)
                {
                    Utility.RecordLog(string.Format("接口名：{0}未配置对应mockid！", soaname), e.StackTrace == null ? "" : e.StackTrace);
                }
                catch (Exception e)
                {
                    Utility.RecordLog(e.Message, e.StackTrace == null ? "" : e.StackTrace);
                }
            }

            mockBackUp = new List<string>();
        }

        /// <summary>
        ///  检查开关表，如果缺少配置，新需求：将平台不存在的接口也同步给平台
        /// </summary>
        /// <param name="dic"></param>
        public static void CheckDicAndApi()
        {
            string sql = "SELECT ApiID FROM [FlightBookingAutomation].[dbo].MockIDConfig order by id desc";
            DataTable checkTable = SQLHelper.ExecuteSql(sql);
            
            //数据库里的所有的apiid
            List<string> dataBaseApis = new List<string>();
            
            for (int i = 0; i < checkTable.Rows.Count; i++)
            {
                dataBaseApis.Add(checkTable.Rows[i][0].ToString());
            }


            //六套caseid对应的开关表检测
            foreach (var item in statusDic)
            {
                string caseid = item.Key;
               
                //需要新增的apiid列表
                List<string> needAddApis = new List<string>();
                
                //对应的开关hashTable
                Hashtable hashtable = item.Value;
                
                //将hashTable的key注入list,后续则为两个list的对比
                List<string> hashtableApis = new List<string>();
                
                foreach (string key in hashtable.Keys)
                {
                    hashtableApis.Add(key);
                }

                for (int i = 0; i < dataBaseApis.Count; i++)
                {
                    string apiidTemp = dataBaseApis[i];
                    //开关表该caseid下缺少这个apiid,新增这个APIID在开关表
                    if (!hashtableApis.Contains(apiidTemp))
                    {
                        statusDic[caseid][apiidTemp] = false;
                        //mock平台也应同步新增
                        needAddApis.Add(apiidTemp);
                    }
                }

                for (int i = 0; i < hashtableApis.Count; i++)
                {
                    string apiidTemp = hashtableApis[i];
                    //数据库里无这个apiid，在开关表里去掉
                    if (!dataBaseApis.Contains(apiidTemp))
                    {
                        statusDic[caseid].Remove(apiidTemp);
                    }
                }

                //mock平台同步检测，检测时可能抛出请求网络异常，需捕获，防止影响任务执行（执行失败的任务，需由小工具进行补偿）
                try{

                    if (needAddApis.Count != 0){
                        //平台与数据库同步
                        addApiIDs(caseid, needAddApis);
                        //在mock平台关闭新增的api
                        for (int i = 0; i < needAddApis.Count; i++)
                        {
                            closeMockByApiid(caseid, needAddApis[i]);
                        }
                    }
                
                }catch (Exception e){

                    Utility.RecordLog("同步mock平台时报错，报错信息：" + e.InnerException.Message, e.InnerException.StackTrace == null ? "" : e.InnerException.StackTrace);
                }
             }

        }

        //设置需要给mock平台的数据,全量比较版
        public static void setNeedAddList(List<string> dataBaseApis, string caseid)
        {
            List<Task> tasklist = new List<Task>();
            Task eachTask = null;
            //检查mock平台，如果有mock平台没有配置的apiid，就给mock平台配置上
            for (int i = 0; i < dataBaseApis.Count; i++)
            {
                string apiidTemp = dataBaseApis[i];
                eachTask = Task.Factory.StartNew(() =>
                {
                    getDetailForNeedList(caseid, apiidTemp);
                });
                tasklist.Add(eachTask);
            }
            Task.WaitAll(tasklist.ToArray());
        }


        //具体调用的函数,多线程版，更改needlist
        public static void getDetailForNeedList(string caseid,string apiid) {
            
            JObject jsonobj = getMockStatusByApiid(caseid, apiid);
            string result = jsonobj["ResponseStatus"]["Result"].ToString();

            if (result.Equals("-1"))
            {
                string ErrorMsg = jsonobj["ResponseStatus"]["ErrorMsg"].ToString();
                //说明这个接口在mock平台没有
                if (ErrorMsg.Contains("interface does not exist"))
                {
                    lock (checklocker)
                    {
                        needAddList.Add(apiid);
                    }
                }
            }
        }

        //取得指定caseID和apiID对应的api状态
        public static JObject getMockStatusByApiid(string caseid, string apiID)
        {
            string url = GetMockStatusUrl;

            string request = string.Format("{{\"CaseID\":\"{0}\",\"ApiID\":\"{1}\",\"ApiType\":\"0\",\"UserDomain\":\"cn1\",\"UserName\":\"yanqx\"}}", caseid, apiID);

            return JObject.Parse(HttpPost(url, request, "application/json"));
        }


        //批量增加需要新增的APIID
        public static void addApiIDs(string caseID, List<string> appidList)
        {
            StringBuilder apiIDs = new StringBuilder();

            //通过数据库添加apiID
            for (int i = 0; i < appidList.Count; i++)
            {
                apiIDs.Append("[");
                apiIDs.Append(appidList[i]);
                apiIDs.Append("]");
            }

            string apiIDStr = string.Format("caseID={0}&apiIds=", caseID) + apiIDs.ToString();
            AddCaseInterface(apiIDStr);
        }

        //批量增加某个caseid下的mock数据
        private static string AddCaseInterface(string requ)
        {
            string req = requ;
            string result = string.Empty;
            string url = AddMockUrl;
            var request = WebRequest.Create(url) as HttpWebRequest;
            try
            {
                if (request != null)
                {
                    request.Method = "POST";
                    request.Timeout = 30 * 1000;
                    request.ContentType = "application/x-www-form-urlencoded";
                    CookieContainer cookieContain = new CookieContainer();
                    cookieContain.Add(new Cookie("UserHistory", "UserInfo=H4sIAAAAAAAEAGVRy0rDQBT9lRKXGklS08Ss1NraQNuIibqwIqEd20AyaScJWkQQFcWKSEUXQkE3itBFV1If+Dcmaf/Cm74Q3cxwzzn33DtnDii5hLBruPW8biFKooqYLRTqOq7tUzPUsm3pBgZ0DGTtsoFHyonIcKqmPu6v1/b9r2f/46zfbEckqurEtWAEcEHrI3x6CR9v/etm/+QFaGV31yiiUSvLT7FMmhEFIFIw2BwPWUi6xKjOFm0LmCTsCsT3WyN47UK9WCoR5DgTqH/e7N889Lqd+YToX3dVJaOAat1BZIXYXhWEW6NFMnxMLRLPimlIj5zTcjZFb9JDMni97HXu6P8raywH58Zqng4aR0GrE7bvw+N3gP5I1QxgBNU85Lg7y0sTgX9xFbY7Q/vw8xTwTTkt0zw7uOS8llrLL2a1lKpFjzNNey8HUcj4d6V4bpTtkgbeiEShRAHF1IqOyxXdoLZHP6UZg2A5hhVoZo7m5jRWkHhB4uKzQkKMczw/zYgSw0SBE2KTnFOmJOyZ5uEP3KrxHBgCAAA=&AutoLogin=true") { Domain = "mock.fws.qa.nt.ctripcorp.com" });
                    request.CookieContainer = cookieContain;
                    Stream myRequestStream = request.GetRequestStream();
                    using (StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding("UTF-8")))
                        myStreamWriter.Write(req);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        if (response != null && response.StatusCode == HttpStatusCode.OK)
                        {
                            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                            {
                                result = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.RecordLog(string.Format("检测开关表时，向mock平台新增api时报错，需要新增的请求是：{0}", requ));
            }
            return result;
        }

        //关闭指定apiid的mock
        public static void closeMockByApiid(string caseid, string apiID)
        {
            string url = CloseSOAMockUrl;

            string request = string.Format("<SetCaseApiMockStatus xmlns:i='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://schemas.datacontract.org/2004/07/ServiceMock.RestApi.Suite'>  <ApiID>{0}</ApiID>  <ApiType>0</ApiType>  <CaseID>{1}</CaseID> <IsMock>false</IsMock>  <UserDomain>CN1</UserDomain>  <UserName>yanqx</UserName></SetCaseApiMockStatus>", apiID, caseid);

            string response = HttpPost(url, request);
            CheckResponse(request, response);
        }


    }
}