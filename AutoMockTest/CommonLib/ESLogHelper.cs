﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Data;
using System.Threading.Tasks;
using System.Text;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Xml;
using Newtonsoft.Json;
namespace AutoMockTest.CommonLib
{
    /// <summary>
    /// ES日志查询
    /// </summary>
    public class ESLogHelper
    {
        string esLogUrl = "http://osg.ops.ctripcorp.com/api/10900/flight-online-{0}/_search";
        string esLogMobileUrl = "http://osg.ops.ctripcorp.com/api/10900/flight-mobile-{0}/_search";
        DataTable resultDT = null;

        public string PullEsLog(string searchFilter, string count, string batchNum,string useMobileLog)
        {
            string queryStr = "";
            //格式如果是XXX:XXX或者是*，直接拼接到查询条件前，否则认为是Path条件
            if (Regex.IsMatch(searchFilter, @".+[^\\]:.+") || searchFilter.Trim() == "*")
            {
                queryStr = Utility.processConditions(searchFilter) + queryStr;
            }
            else
            {
                queryStr = "Path:" + Utility.processCondition(searchFilter) + queryStr;
            }

            List<JObject> result = new List<JObject>();
            resultDT = new DataTable();
            resultDT.Columns.Add("path", typeof(string));
            resultDT.Columns.Add("guid", typeof(string));
            resultDT.Columns.Add("requestData", typeof(string));
            resultDT.Columns.Add("responseData", typeof(string));
            resultDT.Columns.Add("requestHeader", typeof(string));

            int days = 7;

            DateTime today = DateTime.Today;
            string esQueryStr = BuildQuery(queryStr, count,useMobileLog);

            if (useMobileLog != null && useMobileLog.Trim().ToLower() == "true")
            {
                esLogUrl = esLogMobileUrl;
            }

            int _counter = 0;
            long timeStamp = long.MaxValue;
            bool noError = true;

            for (int i = 0; i <= days; i++)
            {
                if (timeStamp != long.MaxValue)
                {

                    esQueryStr = BuildQuery(queryStr, count, useMobileLog, timeStamp);
                }

                string _esURL = string.Format(esLogUrl, today.AddDays(-i).ToString("yyyy.MM.dd"));
                if (_counter >= int.Parse(count))
                    break;
                JObject esResult = JObject.Parse(HttpPost(_esURL, esQueryStr));

                if (esResult["aggregations"] == null || esResult["aggregations"]["terms"] == null || esResult["aggregations"]["terms"]["buckets"].Count()==0)
                {
                    if (timeStamp == long.MaxValue)//第一次搜索无结果则判断继续搜索前几天
                        continue;
                    else//非第一次搜索无结果则判断当前日期无结果，继续搜索前一天
                    {
                        esQueryStr = BuildQuery(queryStr, count, useMobileLog);
                        timeStamp = long.MaxValue;
                        continue;
                    }
                }

                for (int j = 0; j < esResult["aggregations"]["terms"]["buckets"].Count(); j++)
                {
                    DataRow row = resultDT.NewRow();

                    row["guid"] = esResult["aggregations"]["terms"]["buckets"][j]["key"] == null ? string.Empty : esResult["aggregations"]["terms"]["buckets"][j]["key"].ToString();

                    resultDT.Rows.Add(row);

                    //找到当前查询结果中最早的时间戳
                    long thisTimeStamp = useMobileLog != null && useMobileLog.Trim().ToLower() == "true" ? long.Parse(esResult["aggregations"]["terms"]["buckets"][j]["@timestamp"]["value"].ToString()) : long.Parse(esResult["aggregations"]["terms"]["buckets"][j]["@timestamp"]["value"].ToString());
                    if (timeStamp >= thisTimeStamp)
                        timeStamp = thisTimeStamp - 1;

                }

                //多线程处理
                List<Task> taskList = new List<Task>();
                for (int j = 0; j < resultDT.Rows.Count; j++)
                {
                    try
                    {

                        Task eachTask = null;
                        if (_counter >= int.Parse(count))
                        {
                            Task.WaitAll(taskList.ToArray());
                            taskList.Clear();
                            break;
                        }
                        eachTask = Task.Factory.StartNew((row) =>
                        {
                            ProcessData(row as DataRow, _esURL, batchNum, count, useMobileLog, ref _counter);
                        }, resultDT.Rows[j]);
                        taskList.Add(eachTask);

                        if ((j + 1) % 20 == 0 || resultDT.Rows.Count == j + 1)//每次处理20个
                        {

                            Task.WaitAll(taskList.ToArray());

                            taskList.Clear();
                        }
                    }
                    catch (Exception e)
                    {
                        noError = false;
                        Utility.RecordLog(e.InnerException.Message, e.InnerException.StackTrace == null ? "" : e.InnerException.StackTrace);
                    }
                }

                resultDT.Rows.Clear();
                if (useMobileLog != null && useMobileLog.Trim().ToLower() == "true")
                    break;
                i--;//搜索范围还是在当天
            }
            if (_counter == 0 || !noError)
            {
                updateBatchStatus(batchNum, "F");
                return "0";
            }

            updateBatchStatus(batchNum, "S");
            return batchNum;
        }

        /// <summary>
        /// 创建新的Batch，返回Batch号
        /// </summary>
        /// <returns></returns>
        public string initialBatch(string searchFilter,int expectCount)
        {
            string batchSql = string.Format("INSERT INTO FlightBookingAutomation..ESLogBatch VALUES(getdate(),'{0}','{1}','P') SELECT @@identity", searchFilter, expectCount);
            return SQLHelper.ExecuteSql(batchSql, SQLHelper.constr("FlightBookingAutomation_ConnStr","FAT")).Rows[0][0].ToString();
        }

        /// <summary>
        /// 更新BatchNum的执行结果
        /// </summary>
        /// <param name="batchNum">批次号</param>
        /// <param name="status">F：失败，S：成功，P：处理中</param>
        private void updateBatchStatus(string batchNum, string status)
        {
            string sql = string.Format("UPDATE FlightBookingAutomation..ESLogBatch SET Status = '{0}' WHERE BatchNum = '{1}'", status, batchNum);
            SQLHelper.ExecuteSqlByNonQuery(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));
        }

        /// <summary>
        /// 查询单个guid日志，落地数据
        /// </summary>
        /// <param name="dataRow"></param>
        /// <param name="esUrl"></param>
        /// <param name="batchNum"></param>
        public void ProcessData(DataRow dataRow, string esUrl, string batchNum, string targetCount, string useMobileLog, ref int counter)
        {
            if (counter >= int.Parse(targetCount))//数量达到则停止查询
                return;
            string esGuidQueryStr = BuildGUIDQuery(dataRow["guid"].ToString(), useMobileLog);
            JObject esResult = JObject.Parse(HttpPost(esUrl, esGuidQueryStr));
            string guid = dataRow["guid"].ToString();
            string ServiceName = string.Empty;
            string RequestData = string.Empty;
            string ResponseData = string.Empty;
            string CompressType = string.Empty;
            string MockDataSql = string.Empty;
            string ESLogMobileWSSql = string.Empty;
            List<string> soaNameList = new List<string>();

            //查询无调用1.0或者2.0接口日志则不落地
            if (esResult["hits"] == null || esResult["hits"]["hits"] == null || esResult["hits"]["hits"].Count() == 0)
                return;

            if (useMobileLog != null && useMobileLog.Trim().ToLower() == "true")
            {
                for (int i = 0; i < esResult["hits"]["hits"].Count(); i++)
                {
                    if (esResult["hits"]["hits"][i]["_source"]["ServiceType"].ToString() == "MobileService")
                    {
                        if (esResult["hits"]["hits"][i]["_source"]["ClientRequestBody"] != null && esResult["hits"]["hits"][i]["_source"]["ClientResponseBody"] != null)
                        {
                            string requestBody = Utility.DecompressData(esResult["hits"]["hits"][i]["_source"]["ClientRequestBody"].ToString());
                            //请求报文不是正确的json格式pass掉
                            try
                            {
                                // convert JSON to object
                                JObject.Parse(requestBody);
                            }
                            catch (JsonReaderException)
                            {
                                return;
                            }

                            ESLogMobileWSSql = string.Format("INSERT into FlightBookingAutomation..ESLogMobileWS VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}') ", 
                                batchNum, 
                                esResult["hits"]["hits"][i]["_source"]["Path"].ToString(),
                                esResult["hits"]["hits"][i]["_source"]["cat_client_appid"].ToString(), 
                                guid,
                                requestBody.Replace("'", "''"), 
                                Utility.DecompressData(esResult["hits"]["hits"][i]["_source"]["ClientResponseBody"].ToString()).Replace("'", "''"),
                                Utility.DecompressData(esResult["hits"]["hits"][i]["_source"]["ClientRequestHead"].ToString()).Replace("'", "''"));
                        }
                        
                        continue;
                    }
                    if (esResult["hits"]["hits"][i]["_source"]["ServiceType"].ToString() == "SOA2.0")
                    {
                        RequestData = esResult["hits"]["hits"][i]["_source"]["SOA20Request"] == null ? "" : esResult["hits"]["hits"][i]["_source"]["SOA20Request"].ToString();
                        ResponseData = esResult["hits"]["hits"][i]["_source"]["SOA20Response"] == null ? "" : esResult["hits"]["hits"][i]["_source"]["SOA20Response"].ToString();
                        CompressType = "JSON";
                    }
                    else if (esResult["hits"]["hits"][i]["_source"]["ServiceType"].ToString() == "SOA1.0")
                    {
                        RequestData = esResult["hits"]["hits"][i]["_source"]["SOA10Request"] == null ? "" : esResult["hits"]["hits"][i]["_source"]["SOA10Request"].ToString();
                        ResponseData = esResult["hits"]["hits"][i]["_source"]["SOA10Response"] == null ? "" : esResult["hits"]["hits"][i]["_source"]["SOA10Response"].ToString();
                        CompressType = "XML";
                    }
                    try
                    {
                        RequestData = Utility.DecompressData(RequestData);
                        ResponseData = Utility.DecompressData(ResponseData);

                        if (ResponseData.Trim() == "\"已关闭记录 Response Log\"")
                            return;
                    }
                    catch
                    {
                        return;
                    }

                    #region ServiceName：1.0接口取请求报文里面的RequestType，2.0接口拼接ServiceName.SubService
                    if (esResult["hits"]["hits"][i]["_source"]["ServiceType"].ToString() == "SOA1.0")
                    {
                        ServiceName = Regex.Match(RequestData, @"RequestType( )*=( )*"".+?""( )*?").Value;
                        ServiceName = Regex.Replace(ServiceName, @"RequestType( )*=( )*""", "");
                        ServiceName = Regex.Replace(ServiceName, @"""( )*?", "");
                    }
                    else
                    {
                        ServiceName = string.Format("{0}.{1}", esResult["hits"]["hits"][i]["_source"]["ServiceName"] == null ? string.Empty : esResult["hits"]["hits"][i]["_source"]["ServiceName"].ToString(), esResult["hits"]["hits"][i]["_source"]["SubService"] == null ? string.Empty : esResult["hits"]["hits"][i]["_source"]["SubService"].ToString());
                    }
                    //补偿找不到ServiceName的情况
                    if (ServiceName.Trim() == string.Empty)
                        ServiceName = esResult["hits"]["hits"][i]["_source"]["SubService"] == null ? string.Empty : esResult["hits"]["hits"][i]["_source"]["SubService"].ToString();
                    #endregion
                    
                    if (RequestData == null || ResponseData == null)
                        return;
                    
                    //存在相同接口的不支持，过滤
                    if (soaNameList.Contains(ServiceName))
                        return;
                    else
                        soaNameList.Add(ServiceName);

                    if (CompressType == "JSON" || CompressType == "BJJSON")
                    {
                        //Mock报文不是正确的json格式pass掉
                        try
                        {
                            // convert JSON to object
                            JObject.Parse(ResponseData);
                        }
                        catch (JsonReaderException)
                        {
                            return;
                        }
                    }

                    MockDataSql += string.Format("INSERT into FlightBookingAutomation..ESLogMockData VALUES ('{0}','{1}','{2}','{3}','{4}','{5}') ", batchNum, guid, ServiceName, RequestData.Replace("'", "''"), ResponseData.Replace("'", "''"),CompressType);
                }
            }
            else
            {
                for (int i = 0; i < esResult["hits"]["hits"].Count(); i++)
                {
                    if (esResult["hits"]["hits"][i]["_source"]["TagType"].ToString() == "Cache")//Java服务存在类型为Cache的tag，过滤
                        continue;

                    if (esResult["hits"]["hits"][i]["_source"]["TagType"].ToString() == "Page" || esResult["hits"]["hits"][i]["_source"]["TagType"].ToString() == "Service")
                    {
                        if (esResult["hits"]["hits"][i]["_source"]["resultCode"] != null && esResult["hits"]["hits"][i]["_source"]["resultCode"].ToString().Trim() != "0")
                            return;

                        string requestBody = Utility.DecompressData(esResult["hits"]["hits"][i]["_source"]["stored"]["ClientRequestBody"].ToString());
                        //请求报文不是正确的json格式pass掉
                        try
                        {
                            // convert JSON to object
                            JObject.Parse(requestBody);
                        }
                        catch (JsonReaderException)
                        {
                            return;
                        }

                        ESLogMobileWSSql = string.Format("INSERT into FlightBookingAutomation..ESLogMobileWS VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}') ",
                            batchNum, 
                            esResult["hits"]["hits"][i]["_source"]["Path"].ToString(), 
                            esResult["hits"]["hits"][i]["_source"]["cat_client_appid"].ToString(), 
                            guid, 
                            requestBody.Replace("'", "''"), 
                            Utility.DecompressData(esResult["hits"]["hits"][i]["_source"]["stored"]["ClientResponseBody"].ToString()).Replace("'", "''"), 
                            string.Empty);
                        continue;
                    }

                    RequestData = esResult["hits"]["hits"][i]["_source"]["stored"]["SOARequest20"] == null ? (esResult["hits"]["hits"][i]["_source"]["stored"]["SOARequest"] == null ? string.Empty : esResult["hits"]["hits"][i]["_source"]["stored"]["SOARequest"].ToString()) : esResult["hits"]["hits"][i]["_source"]["stored"]["SOARequest20"].ToString();
                    ResponseData = esResult["hits"]["hits"][i]["_source"]["stored"]["SOAResponse20"] == null ? (esResult["hits"]["hits"][i]["_source"]["stored"]["SOAResponse"] == null ? string.Empty : esResult["hits"]["hits"][i]["_source"]["stored"]["SOAResponse"].ToString()) : esResult["hits"]["hits"][i]["_source"]["stored"]["SOAResponse20"].ToString();

                    RequestData = Utility.DecompressData(RequestData);
                    ResponseData = Utility.DecompressData(ResponseData);

                    if (RequestData == null || ResponseData == null)
                        return;

                    if (esResult["hits"]["hits"][i]["_source"]["SOAVersion"] != null)
                    {
                        #region ServiceName：1.0接口取请求报文里面的RequestType，2.0接口拼接ServiceName.SubService
                        if (esResult["hits"]["hits"][i]["_source"]["SOAVersion"].ToString().ToUpper() == "1.0")
                        {
                            ServiceName = Regex.Match(RequestData, @"RequestType( )*=( )*"".+?""( )*?").Value;
                            ServiceName = Regex.Replace(ServiceName, @"RequestType( )*=( )*""", "");
                            ServiceName = Regex.Replace(ServiceName, @"""( )*?", "");

                            CompressType = esResult["hits"]["hits"][i]["_source"]["stored"]["CompressType"] == null ? "XML" : esResult["hits"]["hits"][i]["_source"]["stored"]["CompressType"].ToString().ToUpper();

                            if (CompressType == "XML")
                            {
                                try//返回报文不是正确的xml格式跳过
                                {
                                    XmlDocument doc = new XmlDocument();
                                    doc.LoadXml(ResponseData);
                                }
                                catch
                                {
                                    return;
                                }
                            }
                        }

                        else
                        {
                            ServiceName = string.Format("{0}.{1}", esResult["hits"]["hits"][i]["_source"]["stored"]["ServiceName"] == null ? string.Empty : esResult["hits"]["hits"][i]["_source"]["stored"]["ServiceName"].ToString(), esResult["hits"]["hits"][i]["_source"]["stored"]["SubService"] == null ? string.Empty : esResult["hits"]["hits"][i]["_source"]["stored"]["SubService"].ToString());

                            CompressType = esResult["hits"]["hits"][i]["_source"]["stored"]["CompressType"] == null ? "JSON" : esResult["hits"]["hits"][i]["_source"]["stored"]["CompressType"].ToString().Trim().ToUpper() == "BJJSON" ? "BJJSON" : "JSON";
                        }
                        //补偿找不到ServiceName的情况
                        if (ServiceName.Trim() == string.Empty)
                            ServiceName = esResult["hits"]["hits"][i]["_source"]["stored"]["SubService"] == null ? string.Empty : esResult["hits"]["hits"][i]["_source"]["stored"]["SubService"].ToString();
                        #endregion
                    }
                    else
                    {
                        if (esResult["hits"]["hits"][i]["_source"]["stored"]["SOAVersion"].ToString().ToUpper() == "2.0")
                        {
                            ServiceName = string.Join(".", esResult["hits"]["hits"][i]["_source"]["SOAServiceName"].ToString().Split('_'));
                            if (esResult["hits"]["hits"][i]["_source"]["stored"]["SOA20Format"].ToString().Trim().ToUpper() == "BJJSON")
                            {
                                Uri uri = new Uri(esResult["hits"]["hits"][i]["_source"]["stored"]["RequestServiceUrl"].ToString());
                                if (Regex.IsMatch(uri.Host, @"\d+.\d+.\d+.\d+"))
                                {
                                    CompressType = "BJJSON";
                                }
                                else
                                {
                                    CompressType = "JSON";
                                }
                            }
                            else
                            {
                                CompressType = "JSON";
                            }
                        }
                        else
                            throw new NotImplementedException();
                    }
                    
                    //存在相同接口的不支持，过滤
                    if (soaNameList.Contains(ServiceName))
                        return;
                    else
                        soaNameList.Add(ServiceName);

                    if (CompressType == "JSON" || CompressType == "BJJSON")
                    {
                        //Mock报文不是正确的json格式pass掉
                        try
                        {
                            // convert JSON to object
                            JObject.Parse(ResponseData);
                        }
                        catch (JsonReaderException)
                        {
                            return;
                        }
                    }

                    MockDataSql += string.Format("INSERT into FlightBookingAutomation..ESLogMockData VALUES ('{0}','{1}','{2}','{3}','{4}','{5}') ", batchNum, guid, ServiceName, RequestData.Replace("'", "''"), ResponseData.Replace("'", "''"), CompressType);
                }
            }
            if (counter >= int.Parse(targetCount))//落地前再次检查数量
                return;
            Interlocked.Increment(ref counter);
            SQLHelper.ExecuteSqlByNonQuery(ESLogMobileWSSql+MockDataSql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));

        }

        /// <summary>
        /// 查询后服务的请求
        /// </summary>
        /// <param name="searchFilter"></param>
        public string BuildQuery(string searchFilter, string count, string useMobileLog, long toTimeStamp = 0)
        {
            string filter = string.Empty;

            //时间当前时间一小时以前的日志
            if (toTimeStamp == 0)
            {
                toTimeStamp = Utility.ConvertDateTimeLong(DateTime.Now.AddHours(-2));
            }

            if (toTimeStamp != 0)
            {
                filter = string.Format(@"""filter"": {{
                ""bool"": {{
                    ""must"": [{{
                        ""range"": {{
                            ""@timestamp"": {{
                                ""to"": {0}
                            }}
                        }}
                    }}]
                }}
            }},", toTimeStamp - 1);//搜索的截止时间为上次搜索结果的最早时间戳-1tick
            }
            string requestStr = string.Empty;
            if (useMobileLog != null && useMobileLog.Trim().ToLower() == "true")
            {
                requestStr = string.Format(@"{{
    ""access_token"": ""eb9d4ff440ae45e234b70c9c81a0a96f"",
    ""request_body"": {{
        ""query"": {{
            ""bool"": {{
                ""should"": [{{
                    ""query_string"": {{
                        ""query"": ""{0}"",
                        ""lowercase_expanded_terms"": false
                    }}
                }}],""filter"": {{
                ""bool"": {{
                    ""must"": [{{
                        ""range"": {{
                            ""@timestamp"": {{
                                ""from"": 1490350560000,
                                ""to"": 1490351400000
                            }}
                        }}
                    }},
                    {{
                        ""query_string"": {{
                            ""query"": ""ip:(\""10.8.43.63\"")""
                        }}
                    }}]
                }}
            }},
                ""minimum_should_match"": 1
            }}
        }},
        ""size"": 0,
        ""aggs"": {{
            ""terms"": {{
                ""terms"": {{
                    ""field"": ""Guid"",
                    ""size"": {1},
                ""order"":[{{
			""@timestamp"":""desc""
		            }}]
                }},
                ""aggs"": {{
                    ""@timestamp"": {{
                        ""min"": {{
                            ""field"": ""@timestamp""
                        }}
                    }}
                }}
            }}
        }}
    }}
}}", searchFilter,  count);
                
            }
            else
            {
                requestStr = string.Format(@"{{
    ""access_token"": ""eb9d4ff440ae45e234b70c9c81a0a96f"",
    ""request_body"": {{
        ""query"": {{
            ""bool"": {{
                ""should"": [{{
                    ""query_string"": {{
                        ""query"": ""{0}"",
                        ""lowercase_expanded_terms"": false
                    }}
                }}],{1}
                ""minimum_should_match"": 1
            }}
        }},
        ""size"": 0,
        ""aggs"": {{
            ""terms"": {{
                ""terms"": {{
                    ""field"": ""LogToken"",
                    ""size"": {2},   
                ""order"":[{{
			""@timestamp"":""desc""
		            }}]
                }},
                ""aggs"": {{
                    ""@timestamp"": {{
                        ""min"": {{
                            ""field"": ""@timestamp""
                        }}
                    }}
                }}
            }}
        }}
    }}
}}", searchFilter, filter, count);
            }


            return requestStr;
        }

        /// <summary>
        /// 根据GUID查询后服务调用的外部接口的请求
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string BuildGUIDQuery(string guid, string useMobileLog)
        {
            string requestStr = string.Empty;
            if (useMobileLog != null && useMobileLog.Trim().ToLower() == "true")
            {
                requestStr = string.Format(@"{{
    ""access_token"":""eb9d4ff440ae45e234b70c9c81a0a96f"",
    ""request_body"":{{
    ""query"": {{
        ""bool"": {{
            ""should"": [{{
                ""query_string"": {{
                    ""query"": ""Guid:{0}"",
                    ""lowercase_expanded_terms"": false
                }}
            }}],
            ""minimum_should_match"": 1
        }}
    }},
    ""size"": 60,
    ""sort"": [{{
        ""@timestamp"": {{
            ""order"": ""desc""
        }}
    }},
    {{
        ""@timestamp"": {{
            ""order"": ""desc""
        }}
    }}]
}}}}", guid);
            }
            else
            {
                requestStr = string.Format(@"{{
    ""access_token"":""eb9d4ff440ae45e234b70c9c81a0a96f"",
    ""request_body"":{{
    ""query"": {{
        ""bool"": {{
            ""should"": [{{
                ""query_string"": {{
                    ""query"": ""LogToken:\""{0}\"""",
                    ""lowercase_expanded_terms"": false
                }}
            }}],
            ""minimum_should_match"": 1
        }}
    }},
    ""size"": 60,
    ""sort"": [{{
        ""@timestamp"": {{
            ""order"": ""desc""
        }}
    }},
    {{
        ""@timestamp"": {{
            ""order"": ""desc""
        }}
    }}]
}}}}", guid);
            }
            return requestStr;
        }

        /// <summary>
        /// http请求方法
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="postDataStr"></param>
        /// <returns></returns>
        public static string HttpPost(string Url, string postDataStr, string contentType = "")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";
            request.ContentType = "application/json";
            if (contentType != "")
                request.ContentType = contentType;
            //request.Credentials = new NetworkCredential(@"l.cui", "Cuilin19820514", "cn1");
            //request.ContentLength = Encoding.UTF8.GetByteCount(postDataStr);
            //request.CookieContainer = cookie;
            Stream myRequestStream = request.GetRequestStream();
            StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding("utf-8"));
            myStreamWriter.Write(postDataStr);
            myStreamWriter.Close();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //response.Cookies = cookie.GetCookies(response.ResponseUri);
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                string retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();

                return retString;

            }
            catch (WebException e)
            {
                Stream myResponseStream = e.Response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                string retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();

                return retString;
            }
        }
    }
}