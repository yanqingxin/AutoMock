﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Xml;
using System.Net;
using System.IO;
using System.Text;
using Ctrip.Flight.Common.SnappyDotnet;
using System.Data;
using System.Threading;
using System.Collections;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Ctrip.Automation.Framework.Lib;


namespace AutoMockTest.CommonLib
{
    public class Utility
    {
        static string uid = "13671850010";
        static string password = "841b7ed798d4959bdc10e22e113b27d62033a235ade0bfe05f3c486b5e031994dd312e96ad04da8d2a920cb0dd422b18cc1a69657233cb9b45b17de49e57f322a6cf146617c0f5ab297677e4c42d8e1c553f0272122140b49370ac7026990b3bb5c8a9c3f1f55e90ca673aaa5b7af16879a9553ab9fc4c9e681ba59b24cc5aa2";
        static string[] uidTable = new string[] { "13671850002", "13671850003", "13671850004", "13671850005", "13671850006", "13671850010" };
        static string[] caseidTable = new string[] { "4997", "4998", "4999", "5000", "5001", "1347" };
        static string[] clientidTable = new string[] { "12021025410000708798", "12001129810028412242", "12001072510021541271", "12001114710031780194", "32001156510029911736", "32001078510035788787" };
        static string GetMockStatusUrl = "http://mock.fws.qa.nt.ctripcorp.com/SuiteApi/json/reply/GetCaseApiInfo";
        static Dictionary<string, string> uidAuthDic = new Dictionary<string, string>();
        static DataTable apiidTable = new DataTable();
        static Dictionary<string, Hashtable> statusDic = new Dictionary<string, Hashtable>();
        static DataTable apiIDs = getApiIDs();
        static string CloseSOAMockUrl = "http://mock.fws.qa.nt.ctripcorp.com/SuiteApi/xml/reply/SetCaseApiMockStatus";
        
        /// <summary>  
        /// DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time"> DateTime时间格式</param>  
        /// <returns>Unix时间戳格式</returns>  
        internal static long ConvertDateTimeLong(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000;   //除10000调整为13位      
            return t;
        }
    

        #region 更新起飞时间
        public static string updateTakeOffDate(string xml, string date)
        {
            string origXml = xml;
            DateTime targetTime = DateTime.Parse(date);
            XmlDocument xmldoc = new XmlDocument();

            try
            {
                xmldoc.LoadXml(xml);

                xml = dateAdjust(xmldoc.InnerXml, targetTime);

            }
            catch
            {
                xml = dateAdjust(xml, targetTime);
            }

            return xml == string.Empty ? origXml : xml;
        }


        /// <summary>
        /// 根据输入的日期，更新所有出发到达日期，以第一个出发日期作为基准
        /// </summary>
        /// <param name="responseXML">报文json或者xml</param>
        /// <param name="targetTime">输入的目标日期</param>
        /// <returns></returns>
        private static string dateAdjust(string responseXML, DateTime targetTime)
        {
            TimeSpan timeSpan = new TimeSpan(0, 0, 0, 0, 0);
            DateTime dTime = new DateTime();
            bool flag = false;
            string responseXMLProcessed = responseXML;
            #region json格式 "DeptDateTime": "/Date(1479602400000-0000)/" "ArrvDateTime": "/Date(1479610800000-0000)/"
            MatchCollection responseGroups = Regex.Matches(responseXML, @"""DeptDateTime"":\W*""/Date\(\d+-\d+\)/""");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                dTime = ConverFromJsDate(departTime.Replace(@"""DeptDateTime"": ""/Date(", "").Replace(@"""DeptDateTime"":""/Date(", "").Replace(@")/""", ""));
                if (!flag)
                {
                    timeSpan = targetTime.Hour == 0 && targetTime.Minute == 0 && targetTime.Second == 0 ? targetTime.Subtract(dTime.Date) : targetTime.Subtract(dTime);
                    flag = true;
                }
                if (!flag)
                    return string.Empty;

                dTime = dTime.Add(timeSpan);
                string departTimeNew = string.Format(@"""DeptDateTime"": ""/Date({0})/""", CovertToJSDate(dTime));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"""ArrvDateTime"":\W*""/Date\(\d+-\d+\)/""");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = ConverFromJsDate(arriveTime.Replace(@"""ArrvDateTime"": ""/Date(", "").Replace(@"""ArrvDateTime"":""/Date(", "").Replace(@")/""", ""));
                aTime = aTime.Add(timeSpan);
                string arriveTimeNew = string.Format(@"""ArrvDateTime"": ""/Date({0})/""", CovertToJSDate(aTime));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }
            #endregion
            #region xml格式 <q1:DeptDateTime>2016-11-24T22:10:00</q1:DeptDateTime>  <q1:ArrvDateTime>2016-11-24T23:05:00</q1:ArrvDateTime>
            responseGroups = Regex.Matches(responseXML, @"<q1:DeptDateTime>\d+-\d+-\d+T\d+:\d+:\d+</q1:DeptDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                dTime = DateTime.Parse(departTime.Replace("<q1:DeptDateTime>", "").Replace("</q1:DeptDateTime>", ""));
                if (!flag)
                {
                    timeSpan = targetTime.Hour == 0 && targetTime.Minute == 0 && targetTime.Second == 0 ? targetTime.Subtract(dTime.Date) : targetTime.Subtract(dTime);
                    flag = true;
                }
                if (!flag)
                    return string.Empty;

                dTime = dTime.Add(timeSpan);
                string departTimeNew = string.Format("<q1:DeptDateTime>{0}</q1:DeptDateTime>", dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<q1:ArrvDateTime>\d+-\d+-\d+T\d+:\d+:\d+</q1:ArrvDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<q1:ArrvDateTime>", "").Replace("</q1:ArrvDateTime>", ""));
                aTime = aTime.Add(timeSpan);
                string arriveTimeNew = string.Format("<q1:ArrvDateTime>{0}</q1:ArrvDateTime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }
            #endregion

            #region xml格式 <DeptDateTime>2016-11-24T22:10:00</DeptDateTime>  <ArrvDateTime>2016-11-24T23:05:00</ArrvDateTime>
            responseGroups = Regex.Matches(responseXML, @"<DeptDateTime>\d+-\d+-\d+T\d+:\d+:\d+</DeptDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                dTime = DateTime.Parse(departTime.Replace("<DeptDateTime>", "").Replace("</DeptDateTime>", ""));
                if (!flag)
                {
                    timeSpan = targetTime.Hour == 0 && targetTime.Minute == 0 && targetTime.Second == 0 ? targetTime.Subtract(dTime.Date) : targetTime.Subtract(dTime);
                    flag = true;
                }
                if (!flag)
                    return string.Empty;

                dTime = dTime.Add(timeSpan);
                string departTimeNew = string.Format("<DeptDateTime>{0}</DeptDateTime>", dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<ArrvDateTime>\d+-\d+-\d+T\d+:\d+:\d+</ArrvDateTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<ArrvDateTime>", "").Replace("</ArrvDateTime>", ""));
                aTime = aTime.Add(timeSpan);
                string arriveTimeNew = string.Format("<ArrvDateTime>{0}</ArrvDateTime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }
            #endregion

            #region xml格式 <DepartTime>2016-11-26T06:45:00</DepartTime> <ArriveTime>2016-11-26T09:05:00</ArriveTime>
            responseGroups = Regex.Matches(responseXML, @"<DepartTime>\d+-\d+-\d+T\d+:\d+:\d+</DepartTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                dTime = DateTime.Parse(departTime.Replace("<DepartTime>", "").Replace("</DepartTime>", ""));
                if (!flag)
                {
                    timeSpan = targetTime.Hour == 0 && targetTime.Minute == 0 && targetTime.Second == 0 ? targetTime.Subtract(dTime.Date) : targetTime.Subtract(dTime);
                    flag = true;
                }
                if (!flag)
                    return string.Empty;
                dTime = dTime.Add(timeSpan);
                string departTimeNew = string.Format("<DepartTime>{0}</DepartTime>", dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<ArriveTime>\d+-\d+-\d+T\d+:\d+:\d+</ArriveTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<ArriveTime>", "").Replace("</ArriveTime>", ""));
                aTime = aTime.Add(timeSpan);
                string arriveTimeNew = string.Format("<ArriveTime>{0}</ArriveTime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }
            #endregion

            #region xml格式 <DeptDateTime>2016-11-26T06:45:00</DeptDateTime> <ArrvDateTime>2016-11-26T09:05:00</ArrvDateTime>
            responseGroups = Regex.Matches(responseXML, @"""DeptDateTime"":""\d+-\d+-\d+ \d+:\d+:\d+""");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                dTime = DateTime.Parse(departTime.Replace(@"""DeptDateTime"":""", "").Replace(@"""", ""));
                if (!flag)
                {
                    timeSpan = targetTime.Hour == 0 && targetTime.Minute == 0 && targetTime.Second == 0 ? targetTime.Subtract(dTime.Date) : targetTime.Subtract(dTime);
                    flag = true;
                }
                if (!flag)
                    return string.Empty;
                dTime = dTime.Add(timeSpan);
                string departTimeNew = string.Format(@"""DeptDateTime"":""{0}""", dTime.ToString("yyyy-MM-dd HH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"""ArrvDateTime"":""\d+-\d+-\d+ \d+:\d+:\d+""");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace(@"""ArrvDateTime"":""", "").Replace(@"""", ""));
                aTime = aTime.Add(timeSpan);
                string arriveTimeNew = string.Format(@"""ArrvDateTime"":""{0}""", aTime.ToString("yyyy-MM-dd HH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }
            #endregion

            #region xml格式 <q1:DTime>2016-11-26T06:45:00</q1:DTime> <q1:ATime>2016-11-26T09:05:00</q1:ATime>
            responseGroups = Regex.Matches(responseXML, @"<q1:DTime>\d+-\d+-\d+T\d+:\d+:\d+</q1:DTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                dTime = DateTime.Parse(departTime.Replace("<q1:DTime>", "").Replace("</q1:DTime>", ""));

                if (!flag)
                    return string.Empty;
                dTime = dTime.Add(timeSpan);
                string departTimeNew = string.Format("<q1:DTime>{0}</q1:DTime>", dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<q1:ATime>\d+-\d+-\d+T\d+:\d+:\d+</q1:ATime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<q1:ATime>", "").Replace("</q1:ATime>", ""));
                aTime = aTime.Add(timeSpan);
                string arriveTimeNew = string.Format("<q1:ATime>{0}</q1:ATime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }
            #endregion

            #region xml格式 <DTime>2016-11-26T06:45:00</DTime> <ATime>2016-11-26T09:05:00</ATime>
            responseGroups = Regex.Matches(responseXML, @"<DTime>\d+-\d+-\d+T\d+:\d+:\d+</DTime>");
            foreach (Match eachGroup in responseGroups)
            {
                string departTime = eachGroup.Value;
                dTime = DateTime.Parse(departTime.Replace("<DTime>", "").Replace("</DTime>", ""));

                if (!flag)
                    return string.Empty;
                dTime = dTime.Add(timeSpan);
                string departTimeNew = string.Format("<DTime>{0}</DTime>", dTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(departTime, departTimeNew);
                responseXMLProcessed = responseXML;
            }

            responseGroups = Regex.Matches(responseXML, @"<ATime>\d+-\d+-\d+T\d+:\d+:\d+</ATime>");
            foreach (Match eachGroup in responseGroups)
            {
                string arriveTime = eachGroup.Value;
                DateTime aTime = DateTime.Parse(arriveTime.Replace("<ATime>", "").Replace("</ATime>", ""));
                aTime = aTime.Add(timeSpan);
                string arriveTimeNew = string.Format("<ATime>{0}</ATime>", aTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                responseXML = responseXML.Replace(arriveTime, arriveTimeNew);
                responseXMLProcessed = responseXML;
            }
            #endregion

            return responseXMLProcessed;
        }


        private static string CovertToJSDate(DateTime dateTime)
        {
            string ticks = dateTime.Subtract(new DateTime(1970, 1, 1, 8, 0, 0)).Ticks.ToString();
            return ticks.Substring(0, ticks.Length - 4) + "-" + ticks.Substring(ticks.Length - 4, 4);

        }

        private static DateTime ConverFromJsDate(string ticks)
        {
            ticks = ticks.Replace("-", "");
            long realTime = 0;
            if (long.TryParse(ticks, out realTime))
            {
                TimeSpan span = new TimeSpan(realTime);
                DateTime StartDate = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
                DateTime date = StartDate.Add(span);
                return date;
            }
            else
            {
                return DateTime.Now;
            }
        }
        #endregion

        /// <summary>
        /// http请求方法
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="postDataStr"></param>
        /// <returns></returns>
        public static string HttpPost(string Url, string postDataStr, string ContentType = "", string encoding = "gb2312")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";
            request.ContentType = "application/xml";
            if (ContentType != "")
                request.ContentType = ContentType;

            request.UserAgent = "Fiddler";
            Stream myRequestStream = request.GetRequestStream();
            StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding(encoding));
            myStreamWriter.Write(postDataStr);
            myStreamWriter.Close();

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //response.Cookies = cookie.GetCookies(response.ResponseUri);
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding(encoding));
                string retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();

                return retString;

            }
            catch (WebException e)
            {
                return "false";
            }
        }

        /// <summary>
        /// http请求方法
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="postDataStr"></param>
        /// <returns></returns>
        public static string HttpGet(string Url, string encoding = "gb2312",int timeout = 5000)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            request.Timeout = timeout;

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                
                //response.Cookies = cookie.GetCookies(response.ResponseUri);
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding(encoding));
                string retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();

                return retString;

            }
            catch (WebException e)
            {
                return "false";
            }
        }

        public static string getAuth()
        {
            string Auth = string.Empty;
            string token = string.Empty;
            string signature = string.Empty;

            string sql = "SELECT TOP 1 [ControlID],[KeyName],[Value],[DataChange_LastTime] FROM [FlightBookingAutomation].[dbo].[ControlSwitch] where KeyName = 'Auth'";
            DataTable dt = SQLHelper.ExecuteSql(sql);
            DateTime lastChangeTime = DateTime.Parse(dt.Rows[0]["DataChange_LastTime"].ToString());
            if (DateTime.Compare(lastChangeTime, DateTime.Now.AddHours(-2)) < 0 || dt.Rows[0]["Value"].ToString().Trim() == "")
            {
                token = getToken();
                signature = getSignature();

                string postData = string.Format("username={0}&Password={1}&captchaId=&capcode=&onetimetoken={2}&IsRemember=true&cid=09031138310001579282&signature={3}", uid, password, token, signature);

                string url = "https://accounts.fat466.qa.nt.ctripcorp.com/H5Login/Handler/LoginAction.ashx";

                string result = HttpPost(url, postData, "application/x-www-form-urlencoded");

                Auth = Regex.Match(result, @"""Auth"":"".*?""").Value.Replace(@"""Auth"":""", "").Replace(@"""", "");
                sql = string.Format("update [FlightBookingAutomation].[dbo].[ControlSwitch] set Value = '{0}',DataChange_LastTime = getdate() where ControlID = '{1}'", Auth, dt.Rows[0]["ControlID"].ToString());
                SQLHelper.ExecuteSqlByNonQuery(sql);
            }
            else
            {
                Auth = dt.Rows[0]["Value"].ToString();
            }
            return Auth;
        }

        /// <summary>
        ///  得到对应的auth值
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static Dictionary<string, string> getAuthNew()
        {
            string Auth = string.Empty;

            string sql = string.Format("select [ControlID],[KeyName],[Value],[DataChange_LastTime] from [FlightBookingAutomation].[dbo].[ControlSwitch] where KeyName like 'Auth.%'");
            DataTable dt = SQLHelper.ExecuteSql(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string currentUid = dt.Rows[i]["KeyName"].ToString().Substring(5);
                DateTime lastChangeTime = DateTime.Parse(dt.Rows[i]["DataChange_LastTime"].ToString());
                string ControlID = dt.Rows[i]["ControlID"].ToString();
                string value = dt.Rows[i]["Value"].ToString().Trim();

                Auth = getEffectAuth(currentUid, lastChangeTime,value,ControlID);
                
                if (uidAuthDic.ContainsKey(currentUid))
                {
                    uidAuthDic[currentUid] = Auth;
                }
                else
                {
                    uidAuthDic.Add(currentUid, Auth);
                }
                
            }

            return uidAuthDic;
        }

        public static string getEffectAuth(string userid, DateTime ChangeTime, string value, string ControlID)
        {
            
            string sql = string.Empty;
            string Auth = string.Empty;
         

            DateTime lastChangeTime = ChangeTime;

            if (DateTime.Compare(lastChangeTime, DateTime.Now.AddHours(-2)) < 0 || value == "")
            {
                for (int i = 0; i < 3; i++)
                {
                    Auth = postGetAuth(userid);
                    if (!Auth.Equals(""))
                    {
                        break;
                    }
                    Thread.Sleep(2000);
                }

                if (!Auth.Equals(""))
                {
                    sql = string.Format("update [FlightBookingAutomation].[dbo].[ControlSwitch] set Value = '{0}',DataChange_LastTime = getdate() where ControlID = '{1}'", Auth, ControlID);
                    SQLHelper.ExecuteSqlByNonQuery(sql);
                }
                else
                {
                    Auth = value;
                }
                
            }
            else
            {
                Auth = value;
            }
            return Auth;
        }
        
        //请求post得到auth
        public static string postGetAuth(string userid)
        {
            
            string Auth = string.Empty;
            string token = getToken();
            string signature = getSignature();

            string postData = string.Format("username={0}&Password={1}&captchaId=&capcode=&onetimetoken={2}&IsRemember=true&cid=09031138310001579282&signature={3}", userid, password, token, signature);

            string url = "https://accounts.fat466.qa.nt.ctripcorp.com/H5Login/Handler/LoginAction.ashx";

            string result = HttpPost(url, postData, "application/x-www-form-urlencoded");

            Auth = Regex.Match(result, @"""Auth"":"".*?""").Value.Replace(@"""Auth"":""", "").Replace(@"""", "");

            return Auth;
        }

        //初始化，得到六套的配置
        public static List<MockModel> getMockModelList(){
            getAuthNew();//得到登录的auth
            List<MockModel> modelList = new List<MockModel>();
            for (int i = 0; i < uidTable.Length; i++)
            {
                MockModel model = new MockModel();
                string uid = uidTable[i];
                model.Uid = uid;
                model.Auth = uidAuthDic[uid];
                model.Caseid = caseidTable[i];
                model.Clientid = clientidTable[i];
                modelList.Add(model);
            }
            return modelList;
        }
        

        //得到六套的开关配置
        public static Dictionary<string,Hashtable> getStatus(){

            string sql = "select ApiID from FlightBookingAutomation..MockIDConfig order by id;";
            apiidTable = SQLHelper.ExecuteSql(sql);
            List<Task> taskList = new List<Task>();
            //配置的六套开关
            for (int i = 0; i < caseidTable.Length; i++)
            {
                string caseid = caseidTable[i];
                Hashtable apiHashtable = getHashTable();
                if (statusDic.ContainsKey(caseid)){
                    statusDic[caseid] = apiHashtable;
                }else{
                    statusDic.Add(caseid, apiHashtable);
                }
            }
            return statusDic;
        }


        //将每个apiid的开关都设置为false
        public static Hashtable getHashTable(){

            Hashtable tempTable = new Hashtable();
            for (int i = 0; i < apiidTable.Rows.Count; i++)
            {
              string apiId = apiidTable.Rows[i][0].ToString();
              tempTable[apiId] = false;   
            }
            return tempTable;
         }


        //得到所有appid
        public static DataTable getApiIDs()
        {
            string sql = "select ApiID from FlightBookingAutomation..MockIDConfig order by id;";
            DataTable table = SQLHelper.ExecuteSql(sql);
            return table;
        }

        //重置、取消所有的mock
        public static string  Reset()
        {
            List<string> list = new List<string>();

            List<Task> tasklist = new List<Task>();

            for (int i = 0; i < caseidTable.Length; i++)
            {
                Task eachTask = null;
                string caseid = caseidTable[i];
                eachTask = Task.Factory.StartNew(() =>
                {
                    closeProcess(caseid);
                });
                tasklist.Add(eachTask);
            }
            Task.WaitAll(tasklist.ToArray());
            return "success";
        }

        public static void closeProcess(string caseid)
        {

            for (int j = 0; j < apiIDs.Rows.Count; j++)
            {
                string apiid = apiIDs.Rows[j][0].ToString();

                closeMock(caseid, apiid);
            }
        }


        //关闭指定caseid的mock平台数据
        public static void closeMock(string caseid, string apiID)
        {
            string url = CloseSOAMockUrl;

            string request = string.Format("<SetCaseApiMockStatus xmlns:i='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://schemas.datacontract.org/2004/07/ServiceMock.RestApi.Suite'>  <ApiID>{0}</ApiID>  <ApiType>0</ApiType>  <CaseID>{1}</CaseID> <IsMock>false</IsMock>  <UserDomain>CN1</UserDomain>  <UserName>yanqx</UserName></SetCaseApiMockStatus>", apiID, caseid);

            string response = HttpPost(url, request);
        }

        private static string getToken()
        {
            string token = string.Empty;

            string url = "https://accounts.fat466.qa.nt.ctripcorp.com/H5Login/Handler/takeonetimetoken.ashx";
            string postData = "username=&cid=09031138310001579282";

            string result = HttpPost(url, postData, "application/x-www-form-urlencoded");

            token = Regex.Match(result, @"""token"":"".+?""").Value.Replace(@"""token"":""", "").Replace(@"""", "");
            return token;
        }

        private static string getSignature()
        {
            string signature = string.Empty;

            string url = "https://accounts.fat466.qa.nt.ctripcorp.com/H5Login/Handler/GetNewValidateCode.ashx";
            string postData = "username=&cid=09031138310001579282";

            string result = HttpPost(url, postData, "application/x-www-form-urlencoded");

            signature = Regex.Match(result, @"""signature"":"".+?""").Value.Replace(@"""signature"":""", "").Replace(@"""", "");
            return signature;
        }

        public static string UpdateUid(string response)
        {
            string processedStr = response;
            processedStr = Regex.Replace(processedStr, @"""UID""( )*:( )*""[^""]+?""", string.Format(@"""UID"": ""{0}""", uid),RegexOptions.IgnoreCase);
            processedStr = Regex.Replace(processedStr, @"""UserID""( )*:( )*"".+?""", string.Format(@"""UserID"": ""{0}""", uid), RegexOptions.IgnoreCase);
            processedStr = Regex.Replace(processedStr, @"<Uid>.+?</Uid>", string.Format(@"<Uid>{0}</Uid>", uid), RegexOptions.IgnoreCase);
            processedStr = Regex.Replace(processedStr, @"""loginName""( )*:( )*"".+?""", string.Format(@"""loginName"": ""{0}""", uid), RegexOptions.IgnoreCase);

            return processedStr;
        }

        //重载，指定uid替换
        public static string UpdateUid(string response,string crruentUid)
        {
            string processedStr = response;
            processedStr = Regex.Replace(processedStr, @"""UID""( )*:( )*""[^""]+?""", string.Format(@"""UID"": ""{0}""", crruentUid), RegexOptions.IgnoreCase);
            processedStr = Regex.Replace(processedStr, @"""UserID""( )*:( )*"".+?""", string.Format(@"""UserID"": ""{0}""", crruentUid), RegexOptions.IgnoreCase);
            processedStr = Regex.Replace(processedStr, @"<Uid>.+?</Uid>", string.Format(@"<Uid>{0}</Uid>", crruentUid), RegexOptions.IgnoreCase);
            processedStr = Regex.Replace(processedStr, @"""loginName""( )*:( )*"".+?""", string.Format(@"""loginName"": ""{0}""", crruentUid), RegexOptions.IgnoreCase);

            return processedStr;
        }

        internal static string UpdateOrderId(string eachResponse, string orderid)
        {
            string oldOrderid = Regex.Match(Regex.Match(eachResponse, @"""OrderID"":\d+",RegexOptions.IgnoreCase).Value, @"\d+").Value;
            if (oldOrderid != "")
                eachResponse = eachResponse.Replace(oldOrderid, orderid);
            return eachResponse;
        }

        /// <summary>
        /// 更新json报文中的日期为UNIX时间戳格式
        /// </summary>
        /// <param name="eachResponse"></param>
        /// <returns></returns>
        internal static string ConvertDate(string eachResponse)
        {            
            string originDataStr = eachResponse;
            if (!Regex.IsMatch(eachResponse, "{.+}"))//如果不是Json格式报文就不修改
                return originDataStr;
         
            MatchCollection mc = Regex.Matches(originDataStr, @"""\d+-\d+-\d+ \d+:\d+:\d+""");
            foreach (Match eachMc in mc)
            {
                DateTime dt = DateTime.Parse(Regex.Match(eachMc.Value, @"\d+-\d+-\d+ \d+:\d+:\d+").Value);
                originDataStr = originDataStr.Replace(eachMc.Value, string.Format(@"""\/Date({0}+0800)\/""", ConvertDateTimeLong(dt)));
            }
            return originDataStr;
        }

        public static string processConditions(string filters)
        {
            string processedFilter = filters;

            List<string> filterList = Regex.Split(filters, @"( )+\w+( )+").ToList();
            foreach (string filter in filterList)
            {
                string key = string.Empty;
                string value = string.Empty;
                if (Regex.IsMatch(filter, @"[^\\]:"))
                {
                    key = filter.Split(':')[0];
                    value = filter.Substring(filter.IndexOf(":")+1);
                }
                else
                {
                    value = filter;
                }

                value = processCondition(value);

                processedFilter = processedFilter.Replace(filter, key == string.Empty ? value : key + ":" + value);
            }

            return processedFilter;
        }

        public static string processCondition(string filter)
        {
            string processedFilter = filter;
            bool hasQuote = Regex.IsMatch(filter, @"^"".+""$");

            if (hasQuote)
            {
                processedFilter = processedFilter.TrimStart('"').TrimEnd('"');

                processedFilter = string.Format(@"\""{0}\""", processedFilter);
            }
            else
            {
                processedFilter = processedFilter.Replace(@"\", @"\\");
            }

            return processedFilter;
        }

        public static string DecompressData(string compressData)
        {
            try
            {
                byte[] compressDataByte = Convert.FromBase64String(compressData);
                compressDataByte = Snappy.Decompress(compressDataByte);

                string unCompressData = string.Empty;
                try
                {
                    using (MemoryStream ms = new MemoryStream(compressDataByte))
                    {
                        unCompressData = ProtoBuf.Serializer.Deserialize<string>(ms);
                        //body = XMLSerializer.Serialize(flightResponse);
                    }
                }
                catch
                {
                    unCompressData = Encoding.GetEncoding("UTF-8").GetString(compressDataByte);
                }

                return unCompressData;
            }
            catch
            {
                return compressData;
            }
        }

        public static string CompressData(string unCompressData)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ProtoBuf.Serializer.Serialize<string>(ms, unCompressData);
                    ms.Position = 0;
                    byte[] responsBodyByte = ms.ToArray();
                    responsBodyByte = Snappy.Compress(responsBodyByte);
                    unCompressData = Convert.ToBase64String(responsBodyByte);
                }
            }
            catch
            { }
            return unCompressData;
        }

        #region LevenshteinDistance 计算两个字符串相似度的算法

        /// <summary>
        /// 取最小的一位数
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <param name="third"></param>
        /// <returns></returns>
        private static int LowerOfThree(int first, int second, int third)
        {
            int min = Math.Min(first, second);
            return Math.Min(min, third);
        }

        private static int Levenshtein_Distance(string str1, string str2)
        {
            int[,] Matrix;
            int n = str1.Length;
            int m = str2.Length;

            int temp = 0;
            char ch1;
            char ch2;
            int i = 0;
            int j = 0;
            if (n == 0)
            {
                return m;
            }
            if (m == 0)
            {

                return n;
            }
            Matrix = new int[n + 1, m + 1];

            for (i = 0; i <= n; i++)
            {
                //初始化第一列
                Matrix[i, 0] = i;
            }

            for (j = 0; j <= m; j++)
            {
                //初始化第一行
                Matrix[0, j] = j;
            }

            for (i = 1; i <= n; i++)
            {
                ch1 = str1[i - 1];
                for (j = 1; j <= m; j++)
                {
                    ch2 = str2[j - 1];
                    if (ch1.Equals(ch2))
                    {
                        temp = 0;
                    }
                    else
                    {
                        temp = 1;
                    }
                    Matrix[i, j] = LowerOfThree(Matrix[i - 1, j] + 1, Matrix[i, j - 1] + 1, Matrix[i - 1, j - 1] + temp);
                }
            }

            return Matrix[n, m];
        }

        

        /// <summary>
        /// 计算字符串相似度
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns></returns>
        public static decimal LevenshteinDistancePercent(string str1, string str2)
        {
            if (str1 == "" && str2 == "")
                return 1;
            //int maxLenth = str1.Length > str2.Length ? str1.Length : str2.Length;
            int val = Levenshtein_Distance(str1, str2);
            return 1 - (decimal)val / Math.Max(str1.Length, str2.Length);
        }

        #endregion

        /// <summary>
        /// 根据xPath过滤对比结果
        /// </summary>
        /// <param name="OriginalData"></param>
        /// <param name="xmlPath"></param>
        /// <returns></returns>
        internal static string filerXpath(string OriginalData, string xmlPath)
        {

            string[] nodes = Regex.Split(OriginalData, @"\|\|\|");
            string processedData = string.Empty;

            foreach (string node in nodes)
            {
                if(node.Contains("[HightLight]"))
                {
                    string[] hightLightNode = Regex.Split(node, @"\|\|");
                    if (hightLightNode[0].Trim().ToLower() == xmlPath.Trim().ToLower() && xmlPath.Trim() != string.Empty)
                    {
                        processedData = processedData + "|||" + hightLightNode[1].Replace("[HightLight]", "[BackGround]");
                    }
                    else if (hightLightNode[0].Trim().ToLower().StartsWith("Root.ResponseStatus".ToLower()))//排除根节点的不同
                    {
                        processedData = processedData + "|||" + hightLightNode[1].Replace("[HightLight]", "");
                    }
                    else
                    {
                        processedData = processedData + "|||" + hightLightNode[1];
                    }
                }
                else
                {
                    processedData = processedData + "|||" + node;
                }
            }

            processedData = Regex.Replace(processedData,@"^\|\|\|","");
            return processedData;
        }

        public static void RecordLog(string message, string stackTrace = "")
        {
            string sql = string.Format("INSERT into FlightBookingAutomation..ESLogErrorLog VALUES('{0}','{1}',GETDATE())", message.Replace("'", "''"), stackTrace.Replace("'", "''"));
            SQLHelper.ExecuteSqlByNonQuery(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));
        }

        public static void RecordHermesMessage(string message,string AppId ,string Status)
        {
            string sql = string.Format("INSERT into FlightBookingAutomation..HermesMessage (HermesMessage,AppId,Status,DataCreateTime) VALUES('{0}','{1}','{2}',GETDATE())", message.Replace("'", "''"), AppId, Status);
            SQLHelper.ExecuteSqlByNonQuery(sql, SQLHelper.constr("FlightBookingAutomation_ConnStr", "FAT"));
        }

        public static string getControlSwitch(string KeyName)
        {
            string sql = string.Format("SELECT [Value] FROM [FlightBookingAutomation].[dbo].[ControlSwitch] where KeyName = '{0}'", KeyName);
            DataTable controlSwitch = SQLHelper.ExecuteSql(sql);
            if (controlSwitch.Rows.Count == 0)
                return null;
            return controlSwitch.Rows[0][0].ToString();
        }

    }
}