﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoMockTest.CommonLib
{
    /// <summary>
    /// 存储mock系统的信息
    /// </summary>
    public class MockModel
    {
        private string uid;
        private string auth;
        private string clientid;
        private string caseid;
        
        public string Uid
        {
            get { return uid; }
            set { uid = value; }
        }

        public string Auth
        {
            get { return auth; }
            set { auth = value; }
        }


        public string Clientid
        {
            get { return clientid; }
            set { clientid = value; }
        }


        public string Caseid
        {
            get { return caseid; }
            set { caseid = value; }
        }
    }
}