﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoMockTest.CommonLib
{
    public class LogObject
    {
        public string realLastTestToken { get; set; }
        public string realTestToken { get; set; }
        public Dictionary<string, List<LogPair>> logDict { get; set; }
        public LogObject()
        {
            logDict = new Dictionary<string, List<LogPair>>();
        }
    }
    public class LogPair
    {
        public string LastTestRequest { get; set; }
        public string TestRequest { get; set; }

        public LogPair()
        {
            LastTestRequest = "";
            TestRequest = "";
        }
    }

    public class CatLog
    {
        public string soaserviceName { get; set; }
        public string request { get; set; }
        public string timeStamp { get; set; }
    }
}
