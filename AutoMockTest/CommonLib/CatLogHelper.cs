﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Text.RegularExpressions;

namespace AutoMockTest.CommonLib
{
    public class CatLogHelper
    {
        static string catUrl = "http://cat.fws.qa.nt.ctripcorp.com/cat/r/tag?";

        /// <summary>
        /// 落库，异步处理
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeBatchNum"></param>
        /// <param name="guid"></param>
        /// <param name="logToken"></param>
        /// <param name="benchMarkIP">"379"</param>
        /// <param name="targetIP">"389"</param>
        /// <param name="timestamp">请求日志的时间戳</param>
        public void Process(string batchNum, int executeBatchNum, string guid, string lastTestToken, string testToken, string lastTestIP, string testIP, DateTime timestamp)
        {
            return;
            lastTestIP = Regex.Match(lastTestIP, @"\d+.\d+.\d+.\d+").Value;
            testIP = Regex.Match(testIP, @"\d+.\d+.\d+.\d+").Value;
            string sql = string.Format("DELETE FROM FlightBookingAutomation..ESLogTestLog WHERE BatchNum = '{0}' AND ExecuteBatchNum = '{1}' AND GUID = '{2}'; DELETE FROM FlightBookingAutomation..ESLogTestLogCompare WHERE BatchNum = '{0}' AND ExecuteBatchNum = '{1}' AND GUID = '{2}';",
                batchNum, executeBatchNum, guid);
            sql += string.Format("INSERT INTO FlightBookingAutomation..ESLogTestLog (BatchNum,ExecuteBatchNum,GUID,LastTestLogToken,TestLogToken,TimeStamp,LastTestIP,TestIP,Status,CreateTime)VALUES('{0}','{1}','{2}','{3}','{4}','{5:yyyy-MM-dd HH:mm:ss}','{6}','{7}','U',GETDATE())",
                batchNum, executeBatchNum, guid, lastTestToken, testToken, timestamp, lastTestIP, testIP);

            SQLHelper.ExecuteSqlByNonQuery(sql);//初始化数据，U状态
            LogObject lo = new LogObject();
            string _lastTestToken = lastTestToken;
            string _lastTestIP = lastTestIP;
            string _testToken = testToken;
            string _testIP = testIP;
            string _guid = guid;
            DateTime _timestamp = timestamp;

            List<Task> taskList = new List<Task>();
            Task lastTestTask = Task.Factory.StartNew(()=>{
                QueryLog(_lastTestToken, _lastTestIP, _timestamp, true, lo);
            });
            taskList.Add(lastTestTask);
            Task testTask = Task.Factory.StartNew(() =>
            {
                QueryLog(_testToken, _testIP, _timestamp, false, lo);
            });
            taskList.Add(testTask);


            Task.Factory.ContinueWhenAll(taskList.ToArray(), o =>
            {
                Compare(batchNum, executeBatchNum, _guid, lo);
            });
        }

        /// <summary>
        /// 处理直接的结果，进行逐一对比
        /// </summary>
        /// <param name="batchNum"></param>
        /// <param name="executeBatchNum"></param>
        /// <param name="guid"></param>
        /// <param name="logDict"></param>
        private void Compare(string batchNum, int executeBatchNum, string guid, LogObject lo)
        {
            string sql = string.Format("UPDATE FlightBookingAutomation..ESLogTestLog SET status = 'P',LastTestLogToken = '{0}',TestLogToken = '{1}' WHERE BatchNum = '{2}' AND ExecuteBatchNum = '{3}' AND GUID = '{4}'", lo.realLastTestToken, lo.realTestToken, batchNum, executeBatchNum, guid);
            SQLHelper.ExecuteSqlByNonQuery(sql);//更新状态为处理中

            sql = string.Empty;            
            CompareHelper ch = new CompareHelper();

            try
            {                
                foreach (string key in lo.logDict.Keys)
                {
                    foreach (LogPair logPair in lo.logDict[key])
                    {
                        if (logPair.LastTestRequest.Trim() != "" && logPair.TestRequest.Trim() != "")
                        {
                            List<string> compareResultA = new List<string>();
                            List<string> compareResultB = new List<string>();
                            List<string> diffList = new List<string>();


                            ch.compare(logPair.LastTestRequest, logPair.TestRequest, compareResultA, compareResultB, diffList, "Root");

                            sql += string.Format("INSERT INTO FlightBookingAutomation..ESLogTestLogCompare (BatchNum,ExecuteBatchNum,GUID,RequestType,LastTestRequest,TestRequest,LastTestCompare,TestCompare,DifferSet)VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}'); ",
                                batchNum,
                                executeBatchNum,
                                guid,
                                key,
                                logPair.LastTestRequest,
                                logPair.TestRequest,
                                (string.Join("|||", compareResultA)).Replace("'", "''"),
                                (string.Join("|||", compareResultB)).Replace("'", "''"),
                                (string.Join("|||", diffList)).Replace("'", "''"));
                        }
                        else
                        {
                            sql += string.Format("INSERT INTO FlightBookingAutomation..ESLogTestLogCompare (BatchNum,ExecuteBatchNum,GUID,RequestType,LastTestRequest,TestRequest,LastTestCompare,TestCompare,DifferSet)VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}'); ",
                            batchNum,
                            executeBatchNum,
                            guid,
                            key,
                            logPair.LastTestRequest,
                            logPair.TestRequest,
                            "",
                            "",
                            "");
                        }
                    }
                }

                sql += string.Format("UPDATE FlightBookingAutomation..ESLogTestLog SET status = 'S' WHERE BatchNum = '{0}' AND ExecuteBatchNum = '{1}' AND GUID = '{2}'; ", batchNum, executeBatchNum, guid);
            }
            catch (Exception e)
            {
                sql = string.Format("UPDATE FlightBookingAutomation..ESLogTestLog SET status = 'F' WHERE BatchNum = '{0}' AND ExecuteBatchNum = '{1}' AND GUID = '{2}'; ", batchNum, executeBatchNum, guid);
                
                throw e;
            }
            finally
            {
                SQLHelper.ExecuteSqlByNonQuery(sql);
            }
        }

        private void QueryLog(string logToken, string ip, DateTime timestamp, bool isLastTest, LogObject lo)
        {
            Thread.Sleep(90000);//延迟半分钟，等待日志写入

            string realLogToken = string.Empty;//真正日志里面的LogToken
            //日志的搜索时间段为前后2分钟
            long startTime = Utility.ConvertDateTimeLong(timestamp.AddMinutes(-1));
            long endTime = Utility.ConvertDateTimeLong(timestamp.AddSeconds(30));

            string param = string.Format("endTime={0}&forceDownload=json&index=shopping&mode=edit&op=query&query=ip:{1}&startTime={2}", endTime, ip, startTime);

            string jsonStr = Utility.HttpGet(catUrl+param,"utf-8",60000);
            JObject result = JObject.Parse(jsonStr);

            JToken hits = result["data"]["hits"]["hits"];
            if (hits != null && hits.Count() > 0)
            {
                for (int i = 0; i < hits.Count(); i++)
                {
                    JToken JRequest = hits[i]["_source"]["stored"]["ClientRequestBody"];//遍历主服务的请求报文，找到请求时候的埋点
                    string Request = string.Empty;
                    if (JRequest != null)
                        Request = Utility.DecompressData(JRequest.ToString());
                    if (Request != string.Empty && Regex.IsMatch(Request, string.Format(@"""name""( )*:( )*""pvid""( )*,( )*""value""( )*:( )*""{0}""", logToken)))
                    {
                        JToken JLogToken = hits[i]["_source"]["LogToken"];
                        if (JLogToken != null)
                        {
                            lock (lo)
                            {
                                realLogToken = JLogToken.ToString();
                            }
                            break;
                        }
                    }
                }
            }

            if (realLogToken == string.Empty)//没有找到LogToken退出
                return;



            param = string.Format("endTime={0}&forceDownload=json&index=shopping&mode=edit&op=query&query=LogToken:{1}+AND+ip:{2}&startTime={3}", endTime, realLogToken, ip, startTime);
            jsonStr = Utility.HttpGet(catUrl + param,"utf-8",60000);
            result = JObject.Parse(jsonStr);
            hits = result["data"]["hits"]["hits"];

            List<CatLog> cls = new List<CatLog>();

            if (hits != null && hits.Count() > 0)
            {
                for (int i = 0; i < hits.Count(); i++)
                {
                    JToken JSOAServiceName = hits[i]["_source"]["SOAServiceName"];
                    if (JSOAServiceName != null && (JSOAServiceName.ToString().StartsWith("GetSoa1MockInfoUrl") || JSOAServiceName.ToString().StartsWith("CheckWhiteListURL") || JSOAServiceName.ToString().StartsWith("GetSoa2MockUrl")))
                        continue;
                    else if (JSOAServiceName == null)
                    {
                        JSOAServiceName = hits[i]["_source"]["stored"]["RequestServiceUrl"];//Java调用Mock的情况下，没有SOAServiceName，从RequestServiceUrl直接取ActionName
                        if (JSOAServiceName == null || JSOAServiceName.ToString().ToLower().Contains("GetSoa1MockInfoUrl".ToLower()) || JSOAServiceName.ToString().ToLower().Contains("CheckWhiteList".ToLower()) || JSOAServiceName.ToString().ToLower().Contains("GetSoa2MockUrl".ToLower()) )
                            continue;
                    }

                    JToken JRequest = hits[i]["_source"]["stored"]["SOARequest20"];

                    if (JRequest == null)//对于Java服务，触发Mock的时候，记录的节点是RestfulRequest
                    {
                        JRequest = hits[i]["_source"]["stored"]["RestfulRequest"];                        
                    }

                    if (JRequest == null)
                        continue;

                    string SOAServiceName = JSOAServiceName.ToString();
                    SOAServiceName = SOAServiceName.Split('/')[SOAServiceName.Split('/').Length - 1];//处理http://mock.fws.qa.nt.ctripcorp.com/service/f2042564/json/openOrderDetailSearch
                    SOAServiceName = SOAServiceName.Replace("_", ".");//java服务里面格式为_连接字符
                    SOAServiceName = SOAServiceName.ToLower();

                    string ts = hits[i]["_source"]["@timestamp"].ToString();

                    CatLog cl = new CatLog();
                    cl.soaserviceName = SOAServiceName.Replace("client","");

                    if (cl.soaserviceName.Contains("certificateimageprocess"))//getCertificationInOrder接口调用的soa，java和.net名字不同
                        cl.soaserviceName = cl.soaserviceName.Replace("certificateimageprocess","flightwirelessrestservice");

                    cl.request = JRequest.ToString();
                    cl.timeStamp = ts;

                    cls.Add(cl);
                }

                cls = cls.OrderBy(e => e.timeStamp).ToList();//按照时间戳升序排序

                foreach (CatLog cl in cls)
                {
                    lock (lo)
                    {
                        if (isLastTest)
                            lo.realLastTestToken = realLogToken;
                        else
                            lo.realTestToken = realLogToken;

                        if (lo.logDict.ContainsKey(cl.soaserviceName))
                        {
                            LogPair lp = null;
                            if (isLastTest)
                            {
                                lp = lo.logDict[cl.soaserviceName].Find(e => e.LastTestRequest == "");
                                if(lp == null)
                                {
                                    LogPair _lp = new LogPair();
                                    _lp.LastTestRequest = cl.request;
                                    lo.logDict[cl.soaserviceName].Add(_lp);
                                }
                                else
                                {
                                    lp.LastTestRequest = cl.request;
                                }
                            }
                            else
                            {
                                lp = lo.logDict[cl.soaserviceName].Find(e => e.TestRequest == "");

                                if(lp == null)
                                {
                                    LogPair _lp = new LogPair();
                                    _lp.TestRequest = cl.request;
                                    lo.logDict[cl.soaserviceName].Add(_lp);
                                }
                                else
                                {
                                    lp.TestRequest = cl.request;
                                }
                            }
                        }
                        else
                        {
                            List<LogPair> lps = new List<LogPair>();
                            LogPair lp = new LogPair();
                            if (isLastTest)
                            {
                                lp.LastTestRequest = cl.request;
                            }
                            else
                            {
                                lp.TestRequest = cl.request;
                            }

                            lps.Add(lp);

                            lo.logDict.Add(cl.soaserviceName, lps);
                        }
                    }
                }
            }
        }
    }


}