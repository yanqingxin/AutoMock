﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoMockTest.CommonLib
{
    public class BaseResponse
    {
        public string ResultCode { get; set; }
        public string ErrorMsg { get; set; }
        public string StackTrace { get; set; }

    }

    public class Response : BaseResponse
    {
        public string batchNum { get; set; }
        public string executeBatchNum { get; set; } 
    }

    public class ProgressResponse : BaseResponse
    {
        public string Status { get; set; }
        public string Progress { get; set; }
    }

    public class SearchResponse  : BaseResponse
    {
        public List<BatchInfo> Batches { get; set; }

        public SearchResponse()
        {
            Batches = new List<BatchInfo>();
        }

        public void appendBatchInfo(BatchInfo batch)
        {
            Batches.Add(batch);
        }
    }

    public class BatchInfo
    {
        public string BatchNum { get; set; }
        public string CreateTime { get; set; }
        public string Status { get; set; }
        public string SearchCondition { get; set; }
        public string Count { get; set; }

        public List<ExecuteInfo> Executes { get; set; }

        public BatchInfo()
        {
            Executes = new List<ExecuteInfo>();
        }

        public void appendExecuteInfo(ExecuteInfo execute)
        {
            Executes.Add(execute);
        }
    }

    public class ExecuteInfo
    {
        public string ExecuteBatchNum { get; set; }
        public string CreateTime { get; set; }
        public string Status { get; set; }
    }

    public class CompareResultResponse : BaseResponse
    {
        public string totalCount { get; set; }
        public string Status { get; set; }
        public List<DifferInfo> differs { get; set; }
        public List<DifferDetail> differDetails { get; set; }
        public CompareResultResponse()
        {
            differs = new List<DifferInfo>();
            differDetails = new List<DifferDetail>();
        }
    }

    public class DifferInfo:IComparable<DifferInfo>
    {
        public string xmlPath { get; set; }
        public int count { get; set; }

        public int CompareTo(DifferInfo other)
        {
            if (other == null)
                return 1;
            if (other.count == this.count)
                return string.Compare(this.xmlPath,other.xmlPath);
            return other.count - this.count;
        }
    }
    public class DifferDetail
    {
        public string GUID { get; set; }
        public string ESCompare { get; set; }
        public string TestCompare { get; set; }
    }

}