﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Newtonsoft.Json.Linq;
using AutoMockTest.CommonLib;
using System.Threading;
using System.Data;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace AutoMockTest
{
    public partial class HermesListener : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //List<string> compareResultA = new List<string>();
            //List<string> compareResultB = new List<string>();
            //List<string> diffList = new List<string>();
            //string _ESJson = "{\"ResponseStatus\":{\"Timestamp\":\"/Date(1495705743381+0800)/\",\"Ack\":\"Success\",\"Errors\":[],\"Extension\":[]},\"rc\":0,\"rmsg\":\"\",\"url\":\"m.ctrip.fat531.qa.nt.ctripcorp.com/webapp/LivechatH5/chat?version=3.0&at=2&platform=1&orderid=3679086863&origin=37&groupcode=FltIntlBookLocal&case=37000001\"}";
            //string _TestJson = "{\"responseStatus\":{\"Timestamp\":\"/Date(1495705743371+0800)/\",\"Ack\":\"Success\",\"Errors\":[],\"Build\":null,\"Version\":null,\"Extension\":[]},\"rc\":0,\"rmsg\":null,\"url\":\"https://m.ctrip.fat531.qa.nt.ctripcorp.com/webapp/LivechatH5/chat?version=3.0&at=2&platform=1&orderid=3679086863&origin=37&groupcode=FltIntlBookLocal&case=37000001\",\"schema\":{\"fields\":[{\"pos\":0,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"fields\":[{\"pos\":0,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"datetime\",\"props\":null,\"type\":\"DATETIME\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Timestamp\",\"defaultValue\":null},{\"pos\":1,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"symbols\":[\"Success\",\"Failure\",\"Warning\",\"PartialFailure\"],\"doc\":null,\"schemaName\":{\"name\":\"AckCodeType\",\"space\":\"com.ctriposs.baiji.rpc.common.types\",\"encSpace\":\"com.ctriposs.baiji.rpc.common.types\",\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.AckCodeType\"},\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.AckCodeType\",\"name\":\"AckCodeType\",\"props\":null,\"type\":\"ENUM\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Ack\",\"defaultValue\":null},{\"pos\":2,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"itemSchema\":{\"fields\":[{\"pos\":0,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Message\",\"defaultValue\":null},{\"pos\":1,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"ErrorCode\",\"defaultValue\":null},{\"pos\":2,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"StackTrace\",\"defaultValue\":null},{\"pos\":3,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"symbols\":[\"Error\",\"Warning\"],\"doc\":null,\"schemaName\":{\"name\":\"SeverityCodeType\",\"space\":\"com.ctriposs.baiji.rpc.common.types\",\"encSpace\":\"com.ctriposs.baiji.rpc.common.types\",\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.SeverityCodeType\"},\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.SeverityCodeType\",\"name\":\"SeverityCodeType\",\"props\":null,\"type\":\"ENUM\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"SeverityCode\",\"defaultValue\":null},{\"pos\":4,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"itemSchema\":{\"fields\":[{\"pos\":0,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"FieldName\",\"defaultValue\":null},{\"pos\":1,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"ErrorCode\",\"defaultValue\":null},{\"pos\":2,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Message\",\"defaultValue\":null}],\"doc\":null,\"schemaName\":{\"name\":\"ErrorFieldType\",\"space\":\"com.ctriposs.baiji.rpc.common.types\",\"encSpace\":\"com.ctriposs.baiji.rpc.common.types\",\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ErrorFieldType\"},\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ErrorFieldType\",\"name\":\"ErrorFieldType\",\"props\":null,\"type\":\"RECORD\"},\"name\":\"array\",\"props\":null,\"type\":\"ARRAY\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"ErrorFields\",\"defaultValue\":null},{\"pos\":5,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"symbols\":[\"ServiceError\",\"ValidationError\",\"FrameworkError\",\"SLAError\",\"SecurityError\"],\"doc\":null,\"schemaName\":{\"name\":\"ErrorClassificationCodeType\",\"space\":\"com.ctriposs.baiji.rpc.common.types\",\"encSpace\":\"com.ctriposs.baiji.rpc.common.types\",\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ErrorClassificationCodeType\"},\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ErrorClassificationCodeType\",\"name\":\"ErrorClassificationCodeType\",\"props\":null,\"type\":\"ENUM\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"ErrorClassification\",\"defaultValue\":null}],\"doc\":null,\"schemaName\":{\"name\":\"ErrorDataType\",\"space\":\"com.ctriposs.baiji.rpc.common.types\",\"encSpace\":\"com.ctriposs.baiji.rpc.common.types\",\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ErrorDataType\"},\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ErrorDataType\",\"name\":\"ErrorDataType\",\"props\":null,\"type\":\"RECORD\"},\"name\":\"array\",\"props\":null,\"type\":\"ARRAY\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Errors\",\"defaultValue\":null},{\"pos\":3,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Build\",\"defaultValue\":null},{\"pos\":4,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Version\",\"defaultValue\":null},{\"pos\":5,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"itemSchema\":{\"fields\":[{\"pos\":0,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Id\",\"defaultValue\":null},{\"pos\":1,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Version\",\"defaultValue\":null},{\"pos\":2,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"ContentType\",\"defaultValue\":null},{\"pos\":3,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Value\",\"defaultValue\":null}],\"doc\":null,\"schemaName\":{\"name\":\"ExtensionType\",\"space\":\"com.ctriposs.baiji.rpc.common.types\",\"encSpace\":\"com.ctriposs.baiji.rpc.common.types\",\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ExtensionType\"},\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ExtensionType\",\"name\":\"ExtensionType\",\"props\":null,\"type\":\"RECORD\"},\"name\":\"array\",\"props\":null,\"type\":\"ARRAY\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"Extension\",\"defaultValue\":null}],\"doc\":null,\"schemaName\":{\"name\":\"ResponseStatusType\",\"space\":\"com.ctriposs.baiji.rpc.common.types\",\"encSpace\":\"com.ctrip.flight.afterservice.change.contract.getbuchaturl\",\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ResponseStatusType\"},\"namespace\":\"com.ctriposs.baiji.rpc.common.types\",\"fullName\":\"com.ctriposs.baiji.rpc.common.types.ResponseStatusType\",\"name\":\"ResponseStatusType\",\"props\":null,\"type\":\"RECORD\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"ResponseStatus\",\"defaultValue\":null},{\"pos\":1,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"int\",\"props\":null,\"type\":\"INT\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"rc\",\"defaultValue\":null},{\"pos\":2,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"rmsg\",\"defaultValue\":null},{\"pos\":3,\"doc\":null,\"ordering\":\"IGNORE\",\"schema\":{\"schemas\":[{\"name\":\"string\",\"props\":null,\"type\":\"STRING\"},{\"name\":\"null\",\"props\":null,\"type\":\"NULL\"}],\"name\":\"union\",\"props\":null,\"type\":\"UNION\"},\"aliases\":null,\"name\":\"url\",\"defaultValue\":null}],\"doc\":null,\"schemaName\":{\"name\":\"GetBuChatUrlResponseType\",\"space\":\"com.ctrip.flight.afterservice.change.contract.getbuchaturl\",\"encSpace\":null,\"namespace\":\"com.ctrip.flight.afterservice.change.contract.getbuchaturl\",\"fullName\":\"com.ctrip.flight.afterservice.change.contract.getbuchaturl.GetBuChatUrlResponseType\"},\"namespace\":\"com.ctrip.flight.afterservice.change.contract.getbuchaturl\",\"fullName\":\"com.ctrip.flight.afterservice.change.contract.getbuchaturl.GetBuChatUrlResponseType\",\"name\":\"GetBuChatUrlResponseType\",\"props\":null,\"type\":\"RECORD\"}}";
            ////string _ESJson = "{\"ResponseStatus\":{\"Timestamp\":\"/Date(1495790900490+0800)/\",\"Ack\":\"Success\",\"Errors\":[],\"Extension\":[]},\"rc\":0,\"errMsg\":\"\",\"oBscInfo\":{\"oid\":3986936732,\"statusId\":9,\"statusName\":\"已出票\",\"trpType\":1,\"fltType\":1,\"tag\":0,\"operates\":0,\"oTime\":\"2017-05-26 14:42\",\"srvFrom\":\"client/iphone\"},\"roinfo\":[],\"nextFlt\":{\"segNo\":1,\"sortByNo\":\"O_0\"},\"segs\":[{\"segNo\":1,\"sortBy\":\"O_0\",\"orgFltInfo\":{\"subId\":1,\"priceInfo\":{\"agtId\":\"137\",\"agtCode\":\"\",\"price\":1020,\"tax\":50,\"oil\":0,\"currency\":\"CNY\",\"subsidy\":0,\"ageType\":\"ADU\"},\"seq\":[{\"seqId\":1,\"flag\":0,\"basicInfo\":{\"fNo\":\"CZ6661\",\"airCode\":\"CZ\",\"airUrl\":\"https://pic.c-ctrip.com/flight_intl/airline_logo/32/cz.png?v=4\",\"airName\":\"南方航空\",\"airEName\":\"\",\"class\":\"经济舱\",\"subClass\":\"Z\",\"isShare\":false,\"classEName\":\"Economy class\"},\"dptInfo\":{\"dCityCode\":\"CGO\",\"dCityId\":\"559\",\"dCityName\":\"郑州\",\"dCityEName\":\"Zhengzhou                                         \",\"dPortCode\":\"CGO\",\"dPortName\":\"郑州新郑机场\",\"dPTEName\":\"\",\"dTerminal\":\"\",\"dBldShort\":\"T2\",\"dCityInChina\":true,\"aCityCode\":\"URC\",\"aCityId\":\"39\",\"aCityName\":\"乌鲁木齐\",\"aCityEName\":\"Wulumuqi                                          \",\"aPortCode\":\"URC\",\"aPortName\":\"乌鲁木齐地窝堡机场\",\"aPTEName\":\"\",\"aTerminal\":\"\",\"aBldShort\":\"T3\",\"aCityInChina\":true,\"dDate\":\"2017-06-01 11:30\",\"dEDate\":\"2017-06-01 11:30\",\"aDate\":\"2017-06-01 15:40\",\"aEDate\":\"2017-06-01 15:40\",\"fltDuration\":250,\"fltEnDuration\":\"250\",\"sCityName\":\"\",\"sCityEName\":\"\",\"stopTime\":\"\"},\"psgrList\":[{\"name\":\"苏培\",\"cardType\":\"身份证\",\"cardNo\":\"372923**********12\",\"tickno\":\"784-4954952106\",\"status\":1,\"submitId\":\"\",\"isIn\":true,\"ffp\":\"\",\"stsName\":\"\"}],\"idx\":1}]},\"rbkFltInfo\":[],\"rfdFltInfo\":[],\"chgList\":[],\"nwSortBy\":\"O_0\"}],\"odeliver\":[],\"oProofOperates\":1,\"avblProofName\":\"行程单\",\"contactinfo\":{\"mobile\":\"157****8988\",\"fMobile\":\"\",\"email\":\"\",\"operates\":0},\"repayList\":[],\"opriceinfo\":{\"foreignFee\":0,\"tMoneySrvFee\":0,\"amount\":1154},\"Paymentlst\":[{\"name\":\"信用卡\",\"amt\":1154}],\"avUpCertList\":[],\"notes\":[{\"noteType\":21,\"subType\":0,\"segNo\":1,\"prio\":0,\"noteTit\":\"退改签信息\",\"desc\":\"\",\"noteDetail\":[{\"secTit\":\"套餐退订费\",\"flag\":1,\"titType\":1,\"desc\":\"\",\"extend\":[{\"type\":2,\"exttitle\":\"起飞前2小时外\",\"extcontent\":\"￥510/人\"},{\"type\":2,\"exttitle\":\"起飞前2小时内\",\"extcontent\":\"￥1020/人\"}]},{\"secTit\":\"套餐同舱更改费\",\"flag\":1,\"titType\":2,\"desc\":\"\",\"extend\":[{\"type\":2,\"exttitle\":\"起飞前2小时外\",\"extcontent\":\"￥306/人\"},{\"type\":2,\"exttitle\":\"起飞前2小时内\",\"extcontent\":\"￥510/人\"}]},{\"secTit\":\"备注\",\"flag\":1,\"titType\":4,\"desc\":\"\",\"extend\":[{\"type\":3,\"exttitle\":\"起飞前2小时（含）以外，每次收取票面价50％的退票费；2小时以内及起飞后，不得退票。\",\"extcontent\":\"\"},{\"type\":3,\"exttitle\":\"起飞前2小时（含）以外，每次收取票面价30%的变更费；2小时以内及起飞后，每次收取票面价50%的变更费。改期费与升舱费同时发生时，则需同时收取改期费和升舱差额。同舱不同价需补齐差价。\",\"extcontent\":\"\"}]}]},{\"noteType\":23,\"subType\":0,\"segNo\":0,\"prio\":0,\"noteTit\":\"行李额说明\",\"desc\":\"\",\"noteDetail\":[{\"secTit\":\"免费托运行李额\",\"flag\":1,\"titType\":5,\"desc\":\"\",\"extend\":[{\"type\":0,\"exttitle\":\"\",\"extcontent\":\"20 KG免费托运行李额\"}]}]}],\"selfSrv\":[{\"type\":1002,\"title\":\"我要改签\",\"stsCode\":\"APPLY\",\"remark\":\"\",\"group\":1,\"icon\":\"change\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"\",\"prompt\":\"\"},\"pos\":1},{\"type\":1001,\"title\":\"我要退票\",\"stsCode\":\"APPLY\",\"remark\":\"\",\"group\":1,\"icon\":\"refund-full\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"\",\"prompt\":\"\"},\"pos\":1},{\"type\":1004,\"title\":\"我要报销\",\"stsCode\":\"\",\"remark\":\"\",\"group\":1,\"icon\":\"expense\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flightdelivery/delivery/reopen.html?oid=3986936732\",\"prompt\":\"\"},\"pos\":1},{\"type\":5002,\"title\":\"更多服务\",\"stsCode\":\"\",\"remark\":\"\",\"group\":0,\"icon\":\"\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"RN\",\"url\":\"/rn_flight_assistant/_crn_config?CRNModuleName=flight_assistant&CRNType=1&oid=3986936732&dpcode=CGO&apcode=URC&ddate=2017-06-01\",\"prompt\":\"\"},\"pos\":1},{\"type\":2001,\"title\":\"在线选座\",\"stsCode\":\"\",\"remark\":\"\",\"group\":2,\"icon\":\"checkin\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"\",\"url\":\"\",\"prompt\":\"\"},\"pos\":2},{\"type\":2002,\"title\":\"航班动态\",\"stsCode\":\"\",\"remark\":\"\",\"group\":2,\"icon\":\"dynamics\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"\",\"url\":\"\",\"prompt\":\"\"},\"pos\":2},{\"type\":3002,\"title\":\"微领队\",\"stsCode\":\"\",\"remark\":\"获取出行帮助\",\"group\":3,\"icon\":\"flag\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"\",\"prompt\":\"\"},\"pos\":2},{\"type\":2003,\"title\":\"常旅卡\",\"stsCode\":\"\",\"remark\":\"\",\"group\":2,\"icon\":\"\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"https://m.ctrip.fat4.qa.nt.ctripcorp.com/webapp/frequentflyer/card_manage.html\",\"prompt\":\"\"},\"pos\":2}],\"couponList\":[{\"cate\":1,\"sType\":\"\",\"name\":\"酒店优惠券\",\"code\":\"\",\"count\":1,\"price\":38,\"sts\":0,\"noteList\":[],\"url\":\"\",\"notes\":\"<div><strong>酒店优惠券使用说明：</strong><br />1.每位乘客包含1张100元酒店优惠券。<br />2.适用于单价大于300元的国内酒店（含港澳台）房型（旅行社、团购、小时房、特价房除外）。<br />3.使用优惠券成功预订并入住、在订单成交后3个工作日内，100元将返还至您的携程账户。<br />4.每张优惠券限用一次，每张订单限用一张优惠券，优惠券不得转赠他人。<br />5.优惠券自订单出票日起30天内有效，酒店入住日期不限。<br />6.优惠券在有效期内未使用可随机票一起退订，若优惠券已使用或超过使用有效期限，订单发生退订时须返还相应优惠金额。<br />7.对于以不正当方式使用优惠券的用户，包括但不限于恶意套现、恶意下单、利用程序漏洞等行为，携程旅行网有权在不事先通知的情况下作废其优惠券。</div>\",\"prmid\":0}],\"aValueList\":[],\"xWaitTime\":500,\"recmXProd\":[{\"TypeId\":4,\"Name\":\"接机\"},{\"TypeId\":5,\"Name\":\"送机\"},{\"TypeId\":10,\"Name\":\"保险\"},{\"TypeId\":2,\"Name\":\"安检\"},{\"TypeId\":1,\"Name\":\"休息室\"},{\"TypeId\":7,\"Name\":\"停车场\"},{\"TypeId\":11,\"Name\":\"行李寄送\"},{\"TypeId\":8,\"Name\":\"租车\"}],\"xFlag\":0,\"abt\":1763,\"xpdtlst\":[{\"segno\":1,\"pcgId\":0,\"prodOid\":3986942902,\"canRepay\":false,\"repayExpireTime\":\"/Date(-62135596800000-0000)/\",\"repayAmount\":6,\"extNo\":\"\",\"xpdttpe\":2,\"sType\":\"\",\"xpdtname\":\"快速安检通道\",\"xpdtEname\":\"\",\"xpdtdesc\":\"四楼出发层4号门正对面值机岛D东侧，国内出发2号南“捷易登机”验证核销通行码后，服务人员指引至安检通道\",\"xstatus\":1,\"xOrderStatus\":2,\"xStatusName\":\"已成交\",\"xpdtprice\":6,\"disAmount\":0,\"xpdtcnt\":1,\"xpdtext\":[{\"exttpe\":9,\"title\":\"\",\"content\":\"0\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":10,\"title\":\"\",\"content\":\"2017年6月1日-2017年6月16日内使用有效\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":11,\"title\":\"\",\"content\":\"\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":4,\"title\":\"\",\"content\":\"6|1\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":1,\"title\":\"\",\"content\":\"http://images4.c-ctrip.com/target/6006070000002mgk8FB25.png\",\"contentEn\":\"\",\"desc\":\"核销休息室迎宾\"},{\"exttpe\":1,\"title\":\"\",\"content\":\"http://images4.c-ctrip.com/target/600l070000002mgka6CAA.png\",\"contentEn\":\"\",\"desc\":\"核销休息室远景\"},{\"exttpe\":1,\"title\":\"\",\"content\":\"http://images4.c-ctrip.com/target/600d070000002mgkcDE7A.png\",\"contentEn\":\"\",\"desc\":\"核销休息室近景\"},{\"exttpe\":6,\"title\":\"\",\"content\":\"\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":12,\"title\":\"\",\"content\":\"四楼出发层4号门正对面值机岛D东侧，国内出发2号南“捷易登机”验证核销通行码后，服务人员指引至安检通道\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":16,\"title\":\"\",\"content\":\"07:00-22:00\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":15,\"title\":\"\",\"content\":\"航班起飞前免费退订，航班起飞后不可退订。不可更改。\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":20,\"title\":\"\",\"content\":\"\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":21,\"title\":\"\",\"content\":\"26\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":18,\"title\":\"\",\"content\":\"携程·空港易行\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":8,\"title\":\"\",\"content\":\"苏培\",\"contentEn\":\"\",\"desc\":\"{\\\"ProductID\\\":3986942905,\\\"IssudID\\\":78459494,\\\"TakeOffTime\\\":\\\"2017-06-01T11:30:00\\\",\\\"SegmentNo\\\":1,\\\"PassengerName\\\":\\\"苏培\\\",\\\"ProductType\\\":19}\"},{\"exttpe\":13,\"title\":\"\",\"content\":\"3986942905\",\"contentEn\":\"\",\"desc\":\"苏培\"},{\"exttpe\":2,\"title\":\"1\",\"content\":\"1001702275832141\",\"contentEn\":\"\",\"desc\":\"苏培\"},{\"exttpe\":7,\"title\":\"\",\"content\":\"\",\"contentEn\":\"\",\"desc\":\"\"}],\"histroylst\":[],\"single\":false,\"abt\":1,\"actSts\":\"S\"}],\"opRedLst\":{\"id\":658,\"dspName\":\"专属客服\",\"avatar\":\"https://dimg04.c-ctrip.com/images/Z0040f0000007mj9vDF1E_R_120_120.jpg\",\"newMsg\":false,\"url\":\"ctrip://wireless/hotel_netstar?bu=FLT&orderID=3986936732&ext={\\\"voyage\\\":\\\"【单程】郑州-乌鲁木齐\\\",\\\"Takeoff\\\":\\\"2017年06月01日\\\"}&hasOpRed=2&chatTitle=13671850010&orderTitle=【单程】CZ6661  郑州-乌鲁木齐&orderDesc=2017年06月01日 1人\"},\"rfdApplyInfo\":[],\"srvTime\":\"2017-05-26 17:28:20\",\"abver\":\"\",\"insLstV2\":[{\"cate\":106,\"insurName\":\"航空组合险\",\"price\":40,\"count\":1}],\"banLst\":[{\"cate\":1001,\"title\":\"保险\",\"detSec\":\"\",\"subSrc\":\"\",\"remark\":\"\",\"tag\":0,\"btnTxt\":\"查看详情\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"https://m.ctrip.com/webapp/xbook/insurance/detail?id=3986936732&sourcefrom=viewdetails\",\"prompt\":\"\"},\"subType\":0,\"pos\":0,\"icon\":\"\",\"tagName\":\"\"},{\"cate\":3001,\"title\":\"快速安检\",\"detSec\":\"\",\"subSrc\":\"\",\"remark\":\"\",\"tag\":0,\"btnTxt\":\"查看详情\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"https://m.ctrip.com/webapp/flight/xproduct/fltxpd/fltXNewSecurityChannel.html?oid=3986936732&reload=1&xpdttpe=2&sourcefrom=viewdetails\",\"prompt\":\"\"},\"subType\":2,\"pos\":0,\"icon\":\"\",\"tagName\":\"\"},{\"cate\":4001,\"title\":\"优惠券\",\"detSec\":\"\",\"subSrc\":\"\",\"remark\":\"\",\"tag\":0,\"btnTxt\":\"查看详情\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"RN\",\"url\":\"/rn_flight_xbook_luggage/_crn_config?CRNModuleName=flight_xbook_luggage&CRNType=1&pagetype=detailcoupon&oid=3986936732&sourcefrom=viewdetails\",\"prompt\":\"\"},\"subType\":0,\"pos\":0,\"icon\":\"\",\"tagName\":\"\"}],\"payDetail\":[{\"tit\":\"机票总价\",\"grp\":[{\"cate\":1,\"name\":\"机票价格\",\"uPrc\":1020,\"cnt\":1,\"uName\":\"人\"},{\"cate\":2,\"name\":\"机场建设费\",\"uPrc\":50,\"cnt\":1,\"uName\":\"人\"}]},{\"tit\":\"其他\",\"grp\":[{\"cate\":3,\"name\":\"航空组合险\",\"uPrc\":40,\"cnt\":1,\"uName\":\"份\"},{\"cate\":4,\"name\":\"快速安检通道\",\"uPrc\":6,\"cnt\":1,\"uName\":\"\"},{\"cate\":5,\"name\":\"酒店优惠券\",\"uPrc\":38,\"cnt\":1,\"uName\":\"份\"}]}]}";
            ////string _TestJson = "{\"ResponseStatus\":{\"Timestamp\":\"/Date(1495790899884+0800)/\",\"Ack\":\"Success\",\"Errors\":[],\"Extension\":[]},\"rc\":0,\"errMsg\":\"\",\"oBscInfo\":{\"oid\":3986936732,\"statusId\":9,\"statusName\":\"已出票\",\"trpType\":1,\"fltType\":1,\"tag\":0,\"operates\":0,\"oTime\":\"2017-05-26 14:42\",\"srvFrom\":\"client/iphone\"},\"roinfo\":[],\"nextFlt\":{\"segNo\":1,\"sortByNo\":\"O_0\"},\"segs\":[{\"segNo\":1,\"sortBy\":\"O_0\",\"orgFltInfo\":{\"subId\":1,\"priceInfo\":{\"agtId\":\"137\",\"agtCode\":\"\",\"price\":1020,\"tax\":50,\"oil\":0,\"currency\":\"CNY\",\"subsidy\":0,\"ageType\":\"ADU\"},\"seq\":[{\"seqId\":1,\"flag\":0,\"basicInfo\":{\"fNo\":\"CZ6661\",\"airCode\":\"CZ\",\"airUrl\":\"https://pic.c-ctrip.com/flight_intl/airline_logo/32/cz.png?v=4\",\"airName\":\"南方航空\",\"airEName\":\"\",\"class\":\"经济舱\",\"subClass\":\"Z\",\"isShare\":false,\"classEName\":\"Economy class\"},\"dptInfo\":{\"dCityCode\":\"CGO\",\"dCityId\":\"559\",\"dCityName\":\"郑州\",\"dCityEName\":\"Zhengzhou                                         \",\"dPortCode\":\"CGO\",\"dPortName\":\"郑州新郑机场\",\"dPTEName\":\"\",\"dTerminal\":\"\",\"dBldShort\":\"T2\",\"dCityInChina\":true,\"aCityCode\":\"URC\",\"aCityId\":\"39\",\"aCityName\":\"乌鲁木齐\",\"aCityEName\":\"Wulumuqi                                          \",\"aPortCode\":\"URC\",\"aPortName\":\"乌鲁木齐地窝堡机场\",\"aPTEName\":\"\",\"aTerminal\":\"\",\"aBldShort\":\"T3\",\"aCityInChina\":true,\"dDate\":\"2017-06-01 11:30\",\"dEDate\":\"2017-06-01 11:30\",\"aDate\":\"2017-06-01 15:40\",\"aEDate\":\"2017-06-01 15:40\",\"fltDuration\":250,\"fltEnDuration\":\"250\",\"sCityName\":\"\",\"sCityEName\":\"\",\"stopTime\":\"\"},\"psgrList\":[{\"name\":\"苏培\",\"cardType\":\"身份证\",\"cardNo\":\"372923**********12\",\"tickno\":\"784-4954952106\",\"status\":1,\"submitId\":\"\",\"isIn\":true,\"ffp\":\"\",\"stsName\":\"\"}],\"idx\":1}]},\"rbkFltInfo\":[],\"rfdFltInfo\":[],\"chgList\":[],\"nwSortBy\":\"O_0\"}],\"odeliver\":[],\"oProofOperates\":1,\"avblProofName\":\"行程单\",\"contactinfo\":{\"mobile\":\"157****8988\",\"fMobile\":\"\",\"email\":\"\",\"operates\":0},\"repayList\":[],\"opriceinfo\":{\"foreignFee\":0,\"tMoneySrvFee\":0,\"amount\":1154},\"Paymentlst\":[{\"name\":\"信用卡\",\"amt\":1154}],\"avUpCertList\":[],\"notes\":[{\"noteType\":21,\"subType\":0,\"segNo\":1,\"prio\":0,\"noteTit\":\"退改签信息\",\"desc\":\"\",\"noteDetail\":[{\"secTit\":\"套餐退订费\",\"flag\":1,\"titType\":1,\"desc\":\"\",\"extend\":[{\"type\":2,\"exttitle\":\"起飞前2小时外\",\"extcontent\":\"￥510/人\"},{\"type\":2,\"exttitle\":\"起飞前2小时内\",\"extcontent\":\"￥1020/人\"}]},{\"secTit\":\"套餐同舱更改费\",\"flag\":1,\"titType\":2,\"desc\":\"\",\"extend\":[{\"type\":2,\"exttitle\":\"起飞前2小时外\",\"extcontent\":\"￥306/人\"},{\"type\":2,\"exttitle\":\"起飞前2小时内\",\"extcontent\":\"￥510/人\"}]},{\"secTit\":\"备注\",\"flag\":1,\"titType\":4,\"desc\":\"\",\"extend\":[{\"type\":3,\"exttitle\":\"起飞前2小时（含）以外，每次收取票面价50％的退票费；2小时以内及起飞后，不得退票。\",\"extcontent\":\"\"},{\"type\":3,\"exttitle\":\"起飞前2小时（含）以外，每次收取票面价30%的变更费；2小时以内及起飞后，每次收取票面价50%的变更费。改期费与升舱费同时发生时，则需同时收取改期费和升舱差额。同舱不同价需补齐差价。\",\"extcontent\":\"\"}]}]},{\"noteType\":23,\"subType\":0,\"segNo\":0,\"prio\":0,\"noteTit\":\"行李额说明\",\"desc\":\"\",\"noteDetail\":[{\"secTit\":\"免费托运行李额\",\"flag\":1,\"titType\":5,\"desc\":\"\",\"extend\":[{\"type\":0,\"exttitle\":\"\",\"extcontent\":\"20 KG免费托运行李额\"}]}]}],\"selfSrv\":[{\"type\":1002,\"title\":\"我要改签\",\"stsCode\":\"APPLY\",\"remark\":\"\",\"group\":1,\"icon\":\"change\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"\",\"prompt\":\"\"},\"pos\":1},{\"type\":1001,\"title\":\"我要退票\",\"stsCode\":\"APPLY\",\"remark\":\"\",\"group\":1,\"icon\":\"refund-full\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"\",\"prompt\":\"\"},\"pos\":1},{\"type\":1004,\"title\":\"我要报销\",\"stsCode\":\"\",\"remark\":\"\",\"group\":1,\"icon\":\"expense\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"http://m.ctrip.fat395.qa.nt.ctripcorp.com/webapp/flightdelivery/delivery/reopen.html?oid=3986936732\",\"prompt\":\"\"},\"pos\":1},{\"type\":5002,\"title\":\"更多服务\",\"stsCode\":\"\",\"remark\":\"\",\"group\":0,\"icon\":\"\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"RN\",\"url\":\"/rn_flight_assistant/_crn_config?CRNModuleName=flight_assistant&CRNType=1&oid=3986936732&dpcode=CGO&apcode=URC&ddate=2017-06-01\",\"prompt\":\"\"},\"pos\":1},{\"type\":2001,\"title\":\"在线选座\",\"stsCode\":\"\",\"remark\":\"\",\"group\":2,\"icon\":\"checkin\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"\",\"url\":\"\",\"prompt\":\"\"},\"pos\":2},{\"type\":2002,\"title\":\"航班动态\",\"stsCode\":\"\",\"remark\":\"\",\"group\":2,\"icon\":\"dynamics\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"\",\"url\":\"http://m.ctrip.com/webapp/hybrid/schedule/detail.html?flightNo=CZ6661&queryDate=2017-06-01\",\"prompt\":\"\"},\"pos\":2},{\"type\":6003,\"title\":\"航班点评\",\"stsCode\":\"\",\"remark\":\"\",\"group\":2,\"icon\":\"6003icon\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"\",\"url\":\"http://m.ctrip.com/webapp/you/comment/list/CZ6661-flight.html\",\"prompt\":\"\"},\"pos\":2},{\"type\":3002,\"title\":\"微领队\",\"stsCode\":\"\",\"remark\":\"获取出行帮助\",\"group\":3,\"icon\":\"flag\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"\",\"prompt\":\"\"},\"pos\":2},{\"type\":2003,\"title\":\"常旅卡\",\"stsCode\":\"\",\"remark\":\"\",\"group\":2,\"icon\":\"\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"https://m.ctrip.fat4.qa.nt.ctripcorp.com/webapp/frequentflyer/card_manage.html\",\"prompt\":\"\"},\"pos\":2},{\"type\":6004,\"title\":\"分享行程\",\"stsCode\":\"\",\"remark\":\"\",\"group\":2,\"icon\":\"6004icon\",\"action\":{\"type\":\"C\",\"jumpTarget\":\"\",\"url\":\"\",\"prompt\":\"\"},\"pos\":2}],\"couponList\":[{\"cate\":1,\"sType\":\"\",\"name\":\"酒店优惠券\",\"code\":\"\",\"count\":1,\"price\":38,\"sts\":0,\"noteList\":[],\"url\":\"\",\"notes\":\"<div><strong>酒店优惠券使用说明：</strong><br />1.每位乘客包含1张100元酒店优惠券。<br />2.适用于单价大于300元的国内酒店（含港澳台）房型（旅行社、团购、小时房、特价房除外）。<br />3.使用优惠券成功预订并入住、在订单成交后3个工作日内，100元将返还至您的携程账户。<br />4.每张优惠券限用一次，每张订单限用一张优惠券，优惠券不得转赠他人。<br />5.优惠券自订单出票日起30天内有效，酒店入住日期不限。<br />6.优惠券在有效期内未使用可随机票一起退订，若优惠券已使用或超过使用有效期限，订单发生退订时须返还相应优惠金额。<br />7.对于以不正当方式使用优惠券的用户，包括但不限于恶意套现、恶意下单、利用程序漏洞等行为，携程旅行网有权在不事先通知的情况下作废其优惠券。</div>\",\"prmid\":0}],\"aValueList\":[],\"xWaitTime\":500,\"recmXProd\":[{\"TypeId\":4,\"Name\":\"接机\"},{\"TypeId\":5,\"Name\":\"送机\"},{\"TypeId\":10,\"Name\":\"保险\"},{\"TypeId\":2,\"Name\":\"安检\"},{\"TypeId\":1,\"Name\":\"休息室\"},{\"TypeId\":7,\"Name\":\"停车场\"},{\"TypeId\":11,\"Name\":\"行李寄送\"},{\"TypeId\":8,\"Name\":\"租车\"}],\"xFlag\":0,\"abt\":1763,\"xpdtlst\":[{\"segno\":1,\"pcgId\":0,\"prodOid\":3986942902,\"canRepay\":false,\"repayExpireTime\":\"/Date(-62135596800000-0000)/\",\"repayAmount\":6,\"extNo\":\"\",\"xpdttpe\":2,\"sType\":\"\",\"xpdtname\":\"快速安检通道\",\"xpdtEname\":\"\",\"xpdtdesc\":\"四楼出发层4号门正对面值机岛D东侧，国内出发2号南“捷易登机”验证核销通行码后，服务人员指引至安检通道\",\"xstatus\":1,\"xOrderStatus\":2,\"xStatusName\":\"已成交\",\"xpdtprice\":6,\"disAmount\":0,\"xpdtcnt\":1,\"xpdtext\":[{\"exttpe\":9,\"title\":\"\",\"content\":\"0\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":10,\"title\":\"\",\"content\":\"2017年6月1日-2017年6月16日内使用有效\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":11,\"title\":\"\",\"content\":\"\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":4,\"title\":\"\",\"content\":\"6|1\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":1,\"title\":\"\",\"content\":\"http://images4.c-ctrip.com/target/6006070000002mgk8FB25.png\",\"contentEn\":\"\",\"desc\":\"核销休息室迎宾\"},{\"exttpe\":1,\"title\":\"\",\"content\":\"http://images4.c-ctrip.com/target/600l070000002mgka6CAA.png\",\"contentEn\":\"\",\"desc\":\"核销休息室远景\"},{\"exttpe\":1,\"title\":\"\",\"content\":\"http://images4.c-ctrip.com/target/600d070000002mgkcDE7A.png\",\"contentEn\":\"\",\"desc\":\"核销休息室近景\"},{\"exttpe\":6,\"title\":\"\",\"content\":\"\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":12,\"title\":\"\",\"content\":\"四楼出发层4号门正对面值机岛D东侧，国内出发2号南“捷易登机”验证核销通行码后，服务人员指引至安检通道\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":16,\"title\":\"\",\"content\":\"07:00-22:00\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":15,\"title\":\"\",\"content\":\"航班起飞前免费退订，航班起飞后不可退订。不可更改。\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":20,\"title\":\"\",\"content\":\"\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":21,\"title\":\"\",\"content\":\"26\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":18,\"title\":\"\",\"content\":\"携程·空港易行\",\"contentEn\":\"\",\"desc\":\"\"},{\"exttpe\":8,\"title\":\"\",\"content\":\"苏培\",\"contentEn\":\"\",\"desc\":\"{\\\"ProductID\\\":3986942905,\\\"IssudID\\\":78459494,\\\"TakeOffTime\\\":\\\"2017-06-01T11:30:00\\\",\\\"SegmentNo\\\":1,\\\"PassengerName\\\":\\\"苏培\\\",\\\"ProductType\\\":19}\"},{\"exttpe\":13,\"title\":\"\",\"content\":\"3986942905\",\"contentEn\":\"\",\"desc\":\"苏培\"},{\"exttpe\":2,\"title\":\"1\",\"content\":\"1001702275832141\",\"contentEn\":\"\",\"desc\":\"苏培\"},{\"exttpe\":7,\"title\":\"\",\"content\":\"\",\"contentEn\":\"\",\"desc\":\"\"}],\"histroylst\":[],\"single\":false,\"abt\":1,\"actSts\":\"S\"}],\"opRedLst\":{\"id\":658,\"dspName\":\"专属客服\",\"avatar\":\"https://dimg04.c-ctrip.com/images/Z0040f0000007mj9vDF1E_R_120_120.jpg\",\"newMsg\":false,\"url\":\"ctrip://wireless/hotel_netstar?bu=FLT&orderID=3986936732&ext={\\\"voyage\\\":\\\"【单程】郑州-乌鲁木齐\\\",\\\"Takeoff\\\":\\\"2017年06月01日\\\"}&hasOpRed=2&chatTitle=13671850010&orderTitle=【单程】CZ6661  郑州-乌鲁木齐&orderDesc=2017年06月01日 1人\"},\"rfdApplyInfo\":[],\"srvTime\":\"2017-05-26 17:28:19\",\"abver\":\"\",\"insLstV2\":[{\"cate\":106,\"insurName\":\"航空组合险\",\"price\":40,\"count\":1}],\"banLst\":[{\"cate\":1001,\"title\":\"保险\",\"detSec\":\"\",\"subSrc\":\"\",\"remark\":\"\",\"tag\":0,\"btnTxt\":\"查看详情\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"https://m.ctrip.com/webapp/xbook/insurance/detail?id=3986936732&sourcefrom=viewdetails\",\"prompt\":\"\"},\"subType\":0,\"pos\":0,\"icon\":\"\",\"tagName\":\"\"},{\"cate\":3001,\"title\":\"快速安检\",\"detSec\":\"\",\"subSrc\":\"\",\"remark\":\"\",\"tag\":0,\"btnTxt\":\"查看详情\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"HY\",\"url\":\"https://m.ctrip.com/webapp/flight/xproduct/fltxpd/fltXNewSecurityChannel.html?oid=3986936732&reload=1&xpdttpe=2&sourcefrom=viewdetails\",\"prompt\":\"\"},\"subType\":2,\"pos\":0,\"icon\":\"\",\"tagName\":\"\"},{\"cate\":4001,\"title\":\"优惠券\",\"detSec\":\"\",\"subSrc\":\"\",\"remark\":\"\",\"tag\":0,\"btnTxt\":\"查看详情\",\"action\":{\"type\":\"J\",\"jumpTarget\":\"RN\",\"url\":\"/rn_flight_xbook_luggage/_crn_config?CRNModuleName=flight_xbook_luggage&CRNType=1&pagetype=detailcoupon&oid=3986936732&sourcefrom=viewdetails\",\"prompt\":\"\"},\"subType\":0,\"pos\":0,\"icon\":\"\",\"tagName\":\"\"}],\"payDetail\":[{\"tit\":\"机票总价\",\"grp\":[{\"cate\":1,\"name\":\"机票价格\",\"uPrc\":1020,\"cnt\":1,\"uName\":\"人\"},{\"cate\":2,\"name\":\"机场建设费\",\"uPrc\":50,\"cnt\":1,\"uName\":\"人\"}]},{\"tit\":\"其他\",\"grp\":[{\"cate\":3,\"name\":\"航空组合险\",\"uPrc\":40,\"cnt\":1,\"uName\":\"份\"},{\"cate\":4,\"name\":\"快速安检通道\",\"uPrc\":6,\"cnt\":1,\"uName\":\"\"},{\"cate\":5,\"name\":\"酒店优惠券\",\"uPrc\":38,\"cnt\":1,\"uName\":\"份\"}]}]}";
            //_ESJson = "{\"ResponseStatus\":{\"Timestamp\":\"/Date(1496226198287+0800)/\",\"Ack\":\"Success\",\"Errors\":[],\"Build\":null,\"Version\":null,\"Extension\":null},\"data\":{\"errCode\":\"EC_300002\",\"errMessage\":\"该订单不可退或已经退订\",\"refundTip\":\"\",\"bindType\":1,\"products\":[],\"refProducts\":null,\"passengers\":[],\"areas\":null}}";
            //_TestJson = "{\"ResponseStatus\":{\"Timestamp\":\"/Date(1496226198087+0800)/\",\"Ack\":\"Success\",\"Errors\":[],\"Build\":null,\"Version\":null,\"Extension\":null},\"data\":{\"errCode\":\"EC_300002\",\"errMessage\":\"该订单不可退或已经退订\",\"refundTip\":\"\",\"bindType\":1,\"refundMode\":0,\"products\":[],\"refProducts\":null,\"passengers\":[],\"areas\":null}}";
            //CompareHelper ch = new CompareHelper();
            //ch.compare(_ESJson, _TestJson, compareResultA, compareResultB, diffList, "Root");
            //string a = Utility.CompressData("testtesttest");

            string controlSwitch = Utility.getControlSwitch("Hermes.Appid");
            List<string> appidList = controlSwitch == null ? null : controlSwitch.Split(',').ToList();
            string POSTStr = PostInput();            

            if (POSTStr != "")
            {
                string AppId = "";
                string Status = "";
                string server = "";
                try
                {
                    JObject json = JObject.Parse(Regex.Match(POSTStr,@"{(.|\W)+}").Value.ToString());
                    AppId = json["application"] == null ? "" : json["application"].ToString();
                    Status = json["status"] == null ? "" : json["status"].ToString();

                    server = json["group"] == null?"":json["group"]["name"] == null ?"": json["group"]["name"].ToString().Split('_')[0];
                    Response.Write(POSTStr);
                }
                catch
                { }

                if (appidList != null && appidList.Contains(AppId) && server.ToUpper() == "FAT389" && Status == "SUCCESS")
                {
                    string hostName = Dns.GetHostName();   //获取本机名
                    IPHostEntry localhost = Dns.GetHostByName(hostName);    //方法已过期，可以获取IPv4的地址
                    //IPHostEntry localhost = Dns.GetHostEntry(hostName);   //获取IPv6地址
                    IPAddress localaddr = localhost.AddressList[0];

                    string ip = localaddr.ToString();
                    string postStr = POSTStr;
                    Task task = null;
                    if (ip == "10.2.21.107")
                    {
                        task = Task.Factory.StartNew(() =>
                        {
                            Utility.HttpPost("http://10.2.72.112/AutoMock.WS/HermesListener.aspx", postStr, "application/json;charset=UTF-8", "utf-8");
                        });
                        
                        return;
                    }

                    Utility.HttpGet("http://10.2.75.32/orderdetail/bizconfig/update?host=*");
                    Utility.HttpGet("http://10.2.75.32/orderchange/bizconfig/update?host=*");
                    Utility.HttpGet("http://10.2.74.57/orderdetail/bizconfig/update?host=*");
                    Utility.HttpGet("http://10.2.74.57/orderchange/bizconfig/update?host=*");
                    Utility.RecordHermesMessage(POSTStr, AppId, Status);
                    List<Task> taskList = new List<Task>();
                    Task eachTask = null;
                    eachTask = Task.Factory.StartNew(() =>
                    {
                        StartJob(AppId);
                    });
                    taskList.Add(eachTask);  
                    
                }
                else
                    return;

            }            
        }

        private void StartJob(string AppId)
        {
            string sql = string.Format("SELECT * from FlightBookingAutomation..ESLogJobConfig where AppId = '{0}'", AppId);
            DataTable jobCfgTable = SQLHelper.ExecuteSql(sql);
            if (jobCfgTable.Rows.Count == 0)
                return;
            List<Task> taskList = new List<Task>();
            string receiverList = "";
            List<string> emailContentList = new List<string>();

            for (int i = 0; i < jobCfgTable.Rows.Count; i++)
            {
                string searchCondition = jobCfgTable.Rows[i]["SearchCondition"].ToString();
                string count = jobCfgTable.Rows[i]["Count"].ToString();
                receiverList = jobCfgTable.Rows[i]["ReceiverList"].ToString();
                string JobName = jobCfgTable.Rows[i]["JobName"].ToString();

                Process(JobName, searchCondition, count, emailContentList);
            }

            Email mail = new Email();
            string emailContent = "<div style='font-size:16px;font-family: 微软雅黑;'><table class='table table-bordered' style='margin-top: 10px;margin-top: 10px;border: 1px solid #000000;font-family: 微软雅黑;border-collapse: collapse;' border='1'><tbody>";
            emailContent += "<tr><td>JobName</td><td>测试环境对比结果（线上版本与当前版本）</td><td>生产环境与测试环境对比结果（线上版本与当前版本）</td><td>对比结果地址</td></tr>";
            foreach (string eachContent in emailContentList)
            {
                emailContent += eachContent.Replace("\"","'");
            }
            emailContent += "</tbody></table></div>";
            mail.SendMail(receiverList, string.Format("AppID:{0}流量回放结束", AppId), emailContentList.Count == 0 ? "全部回放无差异" : emailContent, "");
        }

        // 获取POST返回来的数据
        private string PostInput()
        {
            try
            {
                System.IO.Stream s = Request.InputStream;
                int count = 0;
                byte[] buffer = new byte[1024];
                StringBuilder builder = new StringBuilder();
                while ((count = s.Read(buffer, 0, 1024)) > 0)
                {
                    builder.Append(Encoding.UTF8.GetString(buffer, 0, count));
                }
                s.Flush();
                s.Close();
                s.Dispose();
                return builder.ToString();
            }
            catch (Exception ex)
            { throw ex; }
        }

        private void Process(string JobName, string searchCondition, string count,  List<string> emailContentList)
        {
            //Thread.Sleep(120000);//延迟2分钟
            if (count.Trim() == "0")
                return;
            AutoMockService ams = new AutoMockService();
            Response rsp = ams.PullLog(searchCondition, count, "false");
            if (rsp.ResultCode != "PASS")
                return;
            string BatchNum = rsp.batchNum;
            string ExeBatchNum = string.Empty;

            int counter = 0;
            while (counter < 600)
            {
                ProgressResponse pr = ams.QueryProgress(BatchNum, "");
                if (pr.Status.Trim() == "S")
                {
                    break;
                }
                else if (pr.Status.Trim() == "F")
                {
                    return;
                }
                Thread.Sleep(1000);
                counter++;
            }
            Response exeRsp = ams.Excute(BatchNum);
            if (exeRsp.ResultCode != "PASS")
                return;
            ExeBatchNum = exeRsp.executeBatchNum;
            counter = 0;
            while (counter < 3600)
            {
                ProgressResponse pr = ams.QueryProgress(BatchNum, ExeBatchNum);
                if (pr.Status.Trim() == "S")
                {
                    break;
                }
                else if (pr.Status.Trim() == "F")//执行标记失败 但是可能触发现有结果的对比
                {
                    break;
                }
                Thread.Sleep(1000);
                counter++;
            }

            counter = 0;
            string emailContent_TT = "";
            string emailContent_PT = "";
            CompareResultResponse cpr = null;
            while (counter < 3600)
            {
                cpr = ams.queryCompareResult(BatchNum, ExeBatchNum, "1");
                if (cpr.Status.Trim() == "S")
                {
                    if (cpr.differs.Count == 0)
                        emailContent_PT = "不存在差异";
                    else
                        emailContent_PT = "存在差异";
                    break;
                }
                else if (cpr.Status.Trim() == "F")
                {
                    return;
                }
                else if (cpr.Status.Trim() == "U" && counter > 120)//2分钟内没有开始比较 认为执行流程失败
                {
                    return;
                }
                Thread.Sleep(1000);
                counter++;
            }
            cpr = ams.queryCompareResult(BatchNum, ExeBatchNum, "2");
            if (cpr.differs.Count == 0)
                emailContent_TT = "不存在差异";
            else
                emailContent_TT = "存在差异";

            string ReportUrl = string.Format("http://10.2.21.107/FlightTestSite/TrafficReplay?BatchNo={0}&ExeBatchNo={1}", BatchNum, ExeBatchNum);
            //ScreenCapture sc = new ScreenCapture();
            //string imageurl = sc.Capture("http://10.2.21.107/FlightTestSite/TrafficReplay?BatchNo=343&ExeBatchNo=1");

            string emailContentEntry = "";
            emailContentEntry = string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",JobName, emailContent_TT, emailContent_PT, ReportUrl);

            if (emailContent_PT=="存在差异" && emailContent_TT=="存在差异")
            {
                emailContentList.Insert(0, emailContentEntry);
            }
            else if (emailContent_PT == "不存在差异" && emailContent_TT == "不存在差异")
            {
                emailContentList.Add(emailContentEntry);
            }
            else
            {
                int index = emailContentList.FindIndex(e => e.Contains("<td>不存在差异</td><td>不存在差异</td>"));
                if (index == -1)
                    emailContentList.Add(emailContentEntry);
                else
                    emailContentList.Insert(index, emailContentEntry);
            }
        }
    }
}